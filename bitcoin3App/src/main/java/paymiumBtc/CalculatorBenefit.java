package paymiumBtc;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculatorBenefit {
	/**
	 * Calculates benefits in any platform with 8 decimals of precision
	 *
	 * @param amountBtcBuyOrder
	 * @param priceToBuy
	 * @param priceToSell
	 * @param costFeeBuy
	 * @param paypalFee
	 * @return estimated benefit
	 * @throws ServiceException
	 */
	protected double calculateBenefitAnyPlatform(final double amountBtcBuyOrder, final double priceToBuy,
			final double priceToSell, final double costFeeBuy, double costFeeSell) throws ServiceException {

		final double remainedBtc = amountBtcBuyOrder - Util.getBtcFee(amountBtcBuyOrder, costFeeBuy);
		final double tradingBuy = amountBtcBuyOrder * priceToBuy;
		final double tradingSell = remainedBtc * priceToSell
				- Util.getCurrencyFee(remainedBtc, priceToSell, costFeeSell);
		final double totalFee = Util.truncateDoubleDecimal(
				(priceToSell - priceToBuy) * amountBtcBuyOrder - (tradingSell - tradingBuy),
				Constants.SHORT_CUT_DECIMALS);

		final double retBenefit = Util.truncateDoubleDecimal(tradingSell - tradingBuy, Constants.SHORT_CUT_DECIMALS);

		log.info("Total BENEFIT [{}], Total Fee [{}]. Sell [{}], Buy [{}], Amount [{}]. ",
				new Object[] { retBenefit, totalFee, priceToSell, priceToBuy, amountBtcBuyOrder });

		// Comment
		log.trace(
				"Total Benefit [{}], Total Fee [{}]. Sell [{}], Buy [{}], Amount [{}], Remained Btc [{}] after buying. ",
				new Object[] { retBenefit, totalFee, priceToSell, priceToBuy, amountBtcBuyOrder, remainedBtc });

		return retBenefit;
	}

}
