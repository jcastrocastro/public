package paymiumBtc;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CalculateBenefitRun {

	private static final int MAX_ITERATION = 40;
	private static final int PRECITION_DECIMAL = 5;

	public static void main(final String[] args) {
		CalculatorBenefit calculatorBenefit = new CalculatorBenefit();

		// Input by console
		double priceToBuy = Util.readInputConsole("Enter Price Buy: ");
		double amountEurBuyOrder = Util.readInputConsole("Enter Amount of Euro: ");
		double steps = Util.readInputConsole("Step: ");

		// double priceToBuy = 10.5;
		// double amountEurBuyOrder = 1000;
		// double steps = 0.1;

		// Calculating the number of btc to buy
		double amountCoinBuyOrder = amountEurBuyOrder / priceToBuy;

		log.info("Calculating benefits buying [{}] coin", amountCoinBuyOrder);
		double priceToSell = priceToBuy;
		for (int cnt = 0; cnt < MAX_ITERATION; cnt = cnt + 1) {
			try {
				calculatorBenefit.calculateBenefitAnyPlatform(amountCoinBuyOrder, priceToBuy,
						Util.roundUp(priceToSell, PRECITION_DECIMAL), Constants.KRAKEN_BUY_FEE_16,
						Constants.KRAKEN_SELL_FEE_26);
			} catch (final ServiceException e) {
				log.error("Error calculating benefits", e);
			}

			priceToSell = priceToSell + steps;
		}
	}

}
