package paymiumBtc;

public class ServiceException extends Exception {

	/**
	 * UID
	 */
	private static final long serialVersionUID = -5464345417956212261L;

	public ServiceException(String msg) {
		super(msg);
	}

	public ServiceException(Exception exc, String msg) {
		super(msg, exc);
	}

	public ServiceException(Exception e) {
		super(e);
	}
}
