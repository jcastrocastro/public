package paymiumBtc;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class Util {

	private final static Logger LOGGER = LoggerFactory.getLogger(Util.class.getName());

	private static final StringBuilder stringBuilder = new StringBuilder(Constants.PARAM_QUARTZ_KEY);

	public static boolean isEmpty(final Collection<?> list) {
		return list == null || list.isEmpty();
	}

	public static boolean isEmpty(final String string) {
		return string == null || string.isEmpty();
	}

	public static boolean isEmpty(final Object obj) {
		return obj == null;
	}

	@SafeVarargs
	public static <E> boolean isEmpty(final E... array) {
		return array == null || Util.isEmpty(Lists.newArrayList(array));
	}

	/**
	 * Converts Decimal notation. Default 8 decimals
	 * 
	 * @param doubleScientificNotation
	 *            Double scientific notation
	 * @return String with the scientific notation
	 */
	public static String convertDecimalNotation(final double doubleScientificNotation) {
		return Util.convertDecimalNotation(doubleScientificNotation, Constants.STANDART_CUT_DECIMALS);
	}

	/**
	 * Converts Decimal notation.
	 * 
	 * @param doubleScientificNotation
	 *            Double scientific notation
	 * @param numberDecimal
	 *            Number of decimal
	 * @return String with the scientific notation
	 */
	public static String convertDecimalNotation(final double doubleScientificNotation, final int numberDecimal) {
		return new DecimalFormat("#." + Util.repeatCharacter("#", numberDecimal)).format(doubleScientificNotation);
	}

	/**
	 * Get a number of character
	 * 
	 * @param character
	 *            Character to repeat
	 * @param numberChar
	 *            Number of character
	 * @return
	 */
	public static String repeatCharacter(final String character, final int numberChar) {
		return Strings.repeat(character, numberChar);
	}

	/**
	 * Truncates value with the given number of decimals
	 * 
	 * @param value
	 *            Value
	 * @param decimalNumber
	 *            Number of decimals
	 * @return
	 * @throws ServiceException
	 *             Wrong number of decimals to truncate
	 */
	public static double truncateDoubleDecimal(final double value, final int decimalNumber) throws ServiceException {
		double calculateExp = 1e1;
		switch (decimalNumber) {
		case 1:
			calculateExp = 1e1;
			break;
		case 2:
			calculateExp = 1e2;
			break;
		case 3:
			calculateExp = 1e3;
			break;
		case 4:
			calculateExp = 1e4;
			break;
		case 5:
			calculateExp = 1e5;
			break;
		case 6:
			calculateExp = 1e6;
			break;
		case 7:
			calculateExp = 1e7;
			break;
		case 8:
			calculateExp = 1e8;
			break;
		default:
			LOGGER.error("Non supported decimal cut number [{}]. Should be between 1 and 8 inclusives", decimalNumber);
			throw new ServiceException("Non supported decimal cut number");
		}

		return Math.floor(value * calculateExp) / calculateExp;
	}

	public static <E> E ifNotNull(final E instance) {
		return instance == null ? null : instance;
	}

	public static boolean isEqual(final long long1, final long long2) {
		return long1 == long2;
	}

	public static String adaptPrimaryKeyToMongoDb(final String jsonObjectString, final String currentPk) {
		return jsonObjectString.replace(currentPk, Constants.STANDART_PK_MONGODB);
	}

	/**
	 * Reads the reader and put the content in only one string
	 * 
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public static String getString(final BufferedReader reader) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();

		String line = null;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
		}

		return stringBuilder.toString();
	}

	/**
	 * Gets btc fee truncating to 8 decimals
	 *
	 * @param amount
	 * @param feePlatform
	 * @return
	 * @throws ServiceException
	 *             Wrong number of decimals to truncate
	 */
	public static double getBtcFee(final Double amount, final double feePlatform) throws ServiceException {
		return Util.truncateDoubleDecimal(amount * feePlatform, 8);
	}

	/**
	 * Gets euro fee truncating to 8 decimals
	 *
	 * @param amount
	 * @param price
	 * @param feePlatform
	 * @return
	 * @throws ServiceException
	 *             Wrong number of decimals to truncate
	 */
	public static double getCurrencyFee(final Double amount, final Double price, final double feePlatform)
			throws ServiceException {
		return Util.truncateDoubleDecimal(amount * price * feePlatform, 8);
	}

	/**
	 * d1 == d2
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isEquals(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) == Double.doubleToLongBits(d2);
	}

	/**
	 * d1 > d2
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isGreater(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) > Double.doubleToLongBits(d2);
	}

	/**
	 * d1 > d2
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isGreaterOrEqual(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) >= Double.doubleToLongBits(d2);
	}

	/**
	 * d1 < d2
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isLower(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) < Double.doubleToLongBits(d2);
	}

	/**
	 * d1 <= d2
	 * 
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isLowerOrEqual(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) <= Double.doubleToLongBits(d2);
	}

	public static String getParamsName(final int numParam) {
		String retString = null;
		retString = stringBuilder.append(numParam).toString();

		// remove last added
		final int sizeNum = numParam > 9 ? 2 : 1;
		final int lastIdx = stringBuilder.lastIndexOf(String.valueOf(numParam));
		stringBuilder.replace(lastIdx, lastIdx + sizeNum, Constants.EMPTY_STRING);

		return retString;
	}

	/**
	 * Round up max two decimals
	 * 
	 * @param val
	 * @return
	 */
	public static double roundUp(final double val) {
		return new BigDecimal(val).setScale(Constants.SHORT_CUT_DECIMALS, RoundingMode.HALF_UP).doubleValue();
	}

	/**
	 * Round up max two decimals
	 * 
	 * @param val
	 * @return
	 */
	public static double roundUp(final double val, int precition) {
		return new BigDecimal(val).setScale(precition, RoundingMode.HALF_UP).doubleValue();
	}

	public static double readInputConsole(String inputString) {
		System.out.print(inputString);
		return Double.valueOf(System.console().readLine());
	}
}
