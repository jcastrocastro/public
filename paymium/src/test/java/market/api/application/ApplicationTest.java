package market.api.application;

import market.api.common.HelperTest;
import market.api.exception.ServiceException;

import org.junit.Ignore;
import org.junit.Test;

public class ApplicationTest {

	@Ignore("Will stop the JVM")
	@Test
	public void stopNotClushTest() {
		Application.stopAndSendEmail();
	}

	@Ignore("Will execute the application")
	@Test
	public void startTest() throws ServiceException{
		Application.setApplicationDb(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST);

		Application.start();
	}
}
