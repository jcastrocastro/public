package market.api.application;

import org.junit.Assert;
import org.junit.Test;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.common.Util;
import market.api.exception.ServiceException;

public class ApplicationConfTest {

	@Test
	public void applicationConfInitApplMainTest() throws ServiceException {
		ApplicationConf.setApplicationConf(Constants.PATH_CONFIGURATION_FILE_DB);
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getInitApplicationjobPlans());
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1());
		Assert.assertTrue(!Util.isEmpty(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1()));
	}
	
	@Test
	public void applicationConfInitApplTestingTest() throws ServiceException {
		ApplicationConf.setApplicationConf(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST );
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getInitApplicationjobPlans());
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1());
		Assert.assertTrue(!Util.isEmpty(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1()));
	}
	
	@Test
	public void applicationConfEngineMainTest() throws ServiceException {
		ApplicationConf.setApplicationConf(Constants.PATH_CONFIGURATION_FILE_DB);
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getEngine());
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getEngine().getJobPlan());
		Assert.assertTrue(!Util.isEmpty(ApplicationConf.getApplicationConf().getEngine().getJobPlan().get(0).getJobPlanClass()));
		Assert.assertTrue(!Util.isEmpty(ApplicationConf.getApplicationConf().getEngine().getJobPlan()));
	}
	
	@Test
	public void applicationConfEngineTestingTest() throws ServiceException {
		ApplicationConf.setApplicationConf(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST );
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getEngine());
		Assert.assertNotNull(ApplicationConf.getApplicationConf().getEngine().getJobPlan());
		Assert.assertTrue(!Util.isEmpty(ApplicationConf.getApplicationConf().getEngine().getJobPlan()));
	}

}
