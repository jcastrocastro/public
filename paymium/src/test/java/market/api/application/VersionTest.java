package market.api.application;

import org.junit.Assert;
import org.junit.Test;

public class VersionTest {

	@Test
	public void versionTest() {
		Assert.assertEquals("0.5", VersionRun.whichVersion());

	}

	@Test
	public void versionPlatform() {
		Assert.assertEquals("Paymium platform", VersionRun.whichPlatform());
	}

}
