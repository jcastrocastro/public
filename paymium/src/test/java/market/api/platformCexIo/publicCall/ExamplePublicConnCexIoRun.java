package market.api.platformCexIo.publicCall;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ExamplePublicConnCexIoRun {

	private final String USER_AGENT = "Mozilla/5.0";

	private void sendGet() throws Exception {

		final String url = "https://cex.io/api/ticker/BTC/EUR";

		final URL obj = new URL(url);
		final HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", this.USER_AGENT);

		final int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		final BufferedReader in = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String inputLine;
		final StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

//	// HTTP POST request
//	private void sendPost() throws Exception {
//
//		final String url = "https://selfsolve.apple.com/wcResults.do";
//		final URL obj = new URL(url);
//		final HttpsURLConnection con = (HttpsURLConnection) obj
//				.openConnection();
//
//		// add reuqest header
//		con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", this.USER_AGENT);
//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//
//		final String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
//
//		// Send post request
//		con.setDoOutput(true);
//		final DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//		wr.writeBytes(urlParameters);
//		wr.flush();
//		wr.close();
//
//		final int responseCode = con.getResponseCode();
//		System.out.println("\nSending 'POST' request to URL : " + url);
//		System.out.println("Post parameters : " + urlParameters);
//		System.out.println("Response Code : " + responseCode);
//
//		final BufferedReader in = new BufferedReader(new InputStreamReader(
//				con.getInputStream()));
//		String inputLine;
//		final StringBuffer response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//
//		// print result
//		System.out.println(response.toString());
//
//	}

	public static void main(final String[] args) {
		final ExamplePublicConnCexIoRun http = new ExamplePublicConnCexIoRun();

		System.out.println("Testing 1 - Send Http GET request");
		try {
			http.sendGet();
		} catch (final Exception e) {
			e.printStackTrace();
		}

//		System.out.println("\nTesting 2 - Send Http POST request");
//		try {
//			http.sendPost();
//		} catch (final Exception e) {
//			e.printStackTrace();
//		}

	}

}
