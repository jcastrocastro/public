package market.api.platformCexIo.publicCall;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.common.HelperTest;
import market.api.exception.ServiceException;

public class AskTickerRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTickerRun.class.getName());

	public static void main(final String[] args) throws ServiceException {
		HelperTest.beforeClass();

		final String tickerCexIo = PublicCexIoApi.getInstance().getTicker();
		Assert.assertNotNull(tickerCexIo);
		LOGGER.info("Ticker answer [{}]", tickerCexIo);
	}

}
