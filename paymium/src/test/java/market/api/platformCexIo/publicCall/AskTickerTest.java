package market.api.platformCexIo.publicCall;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.exception.ServiceException;

public class AskTickerTest {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTickerTest.class.getName());

	@Test
	public void test() throws ServiceException {
		final String tickerCexIo = PublicCexIoApi.getInstance().getTicker();
		Assert.assertNotNull(tickerCexIo);
		LOGGER.info("Ticker answer [{}]", tickerCexIo);
	}

}
