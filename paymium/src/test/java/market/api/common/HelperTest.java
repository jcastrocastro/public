package market.api.common;

import market.api.application.ApplicationConf;
import market.api.connection.db.ConnectionDbApi;
import market.api.exception.ServiceException;

import org.junit.BeforeClass;

public class HelperTest {

//	protected final MongoCollection<Document> testCollection;

	@BeforeClass
	public static void beforeClass () throws ServiceException {
		ApplicationConf.setApplicationConf(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST);
		ConnectionDbApi.setConnectionProp(ApplicationConf.getApplicationConf());
		ConnectionDbApi.setConnection(ConnectionDbApi.getConnectionMongoDb());
	}

	public static final String PATH_CONFIGURATION_FILE_DB_TEST = "/configuration.xml";

//	public HelperTest() {
//		this.testCollection = ConnectionDbApi.getCollection("test1");
//	}
//
//	@After
//	public void cleanUp(){
//		this.testCollection.drop();
//	}

//	@AfterClass
//	public static void afterClass() {
		// Close DB connection
//		ConnectionDbApi.close();
//	}
}
