package market.api.common;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import market.api.exception.ServiceException;

@SuppressWarnings("deprecation")
public class UtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void roundDownTest() throws ServiceException {
		Assert.assertEquals(2.5, Util.truncateDoubleDecimal(2.568, 1));
	}

	@Test
	public void roundDownTest2() throws ServiceException {
		Assert.assertEquals(2.56, Util.truncateDoubleDecimal(2.568, 2));
	}

	@Test
	public void roundDownTest3() throws ServiceException {
		Assert.assertEquals(2.561, Util.truncateDoubleDecimal(2.561234, 3));
	}

	@Test(expected = ServiceException.class)
	public void roundDownExceptionTest() throws ServiceException{
		Assert.assertEquals(2.561, Util.truncateDoubleDecimal(2.561234, 0));
	}

	@Test(expected = ServiceException.class)
	public void roundDownException2Test() throws ServiceException{
		Assert.assertEquals(2.561, Util.truncateDoubleDecimal(2.561234, 9));
	}

}
