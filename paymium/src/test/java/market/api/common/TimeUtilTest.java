package market.api.common;

import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import market.api.exception.ServiceException;

public class TimeUtilTest {

	private String dateUtcString;
	private Date dateUtcDate;

	@Before
	public void setUp() throws Exception {
		this.dateUtcString = "2016-02-14T04:43:58Z";

		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.MONTH, Calendar.FEBRUARY);
		cal.set(Calendar.DAY_OF_MONTH, 14);
		cal.set(Calendar.HOUR_OF_DAY, 05);
		cal.set(Calendar.MINUTE, 43);
		cal.set(Calendar.SECOND, 58);
		cal.set(Calendar.MILLISECOND,0);

		this.dateUtcDate = cal.getTime();
	}

	@Test
	public void getDateTest() throws ServiceException {
		final Date date1 = TimeUtil.getDateUtc(this.dateUtcString);
		Assert.assertEquals(TimeUtil.convertToLocalDateTime(this.dateUtcDate), TimeUtil.convertToLocalDateTime(date1));
	}

	@Ignore
	@Test
	public void getCheckGranularity1msTest() throws InterruptedException {
		final long long1 = TimeUtil.getTimeNow();

		Thread.sleep(1);

		final long long2 = TimeUtil.getTimeNow();

		Assert.assertFalse(Util.isEqual(long1, long2));
	}

}
