package market.api.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestingJob implements Job{

	private final static Logger LOGGER = LoggerFactory.getLogger(TestingJob.class.getName());
	
	@Override
	public void execute(final JobExecutionContext context)
			throws JobExecutionException {
		LOGGER.info ("Start job {}", TestingJob.class.getSimpleName());
		
		LOGGER.info ("Finish job {}", TestingJob.class.getSimpleName());
		
	}

}
