package market.api.scheduler.job;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

import market.api.common.HelperTest;
import market.api.platformPaymium.IPrivatePaymiumApi;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;
import market.api.scheduler.job.intraday.UpdateAliveTradesJob;

public class UpdateAliveTradesJobTest extends HelperTest{

	private UpdateAliveTradesJob updateAliveTradesJob;
	private JobExecutionContextTestImpl jobExecutionContext;

	@Before
	public void setUp(){
		this.jobExecutionContext = new JobExecutionContextTestImpl();
		this.updateAliveTradesJob = new UpdateAliveTradesJob();
	}
	
	@Test
	public void privateApiTest() {
		final IPrivatePaymiumApi privateApi = PrivatePaymiumApi.getInstance();
		Assert.assertNotNull(privateApi);
	}
	
	@Test
	public void updateAliveTradesJobTest() throws JobExecutionException {
		this.updateAliveTradesJob.execute(this.jobExecutionContext);
	}

}
