package market.api.scheduler.job.eod;

import market.api.common.HelperTest;

import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

public class EodJobTest extends HelperTest{

	private PLJob eodJob;

	@Before
	public void setUp() throws Exception {
		this.eodJob = new PLJob();
	}

	@Test
	public void executeEODTest() throws JobExecutionException {
		this.eodJob.execute(null);
	}

}
