package market.api.scheduler.job.intraday;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.exception.ServiceException;
import market.api.scheduler.job.JobExecutionContextTestImpl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CancelOldBuyJobRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(CancelOldBuyJobRun.class.getName());

	public static void main(final String[] args) {
		final CancelOldBuyJob cancelOldBuyJob = new CancelOldBuyJob();

		final JobExecutionContext jobExecutionContext = new JobExecutionContextTestImpl();

		jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 0, "720");
		jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 1, "326");

		try {
			HelperTest.beforeClass();

			cancelOldBuyJob.execute(jobExecutionContext);
		} catch (final JobExecutionException e) {
			LOGGER.error("", e);
		} catch (final ServiceException exc) {
			LOGGER.error("", exc);
		}
	}
}
