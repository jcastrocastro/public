package market.api.scheduler.job.eod;

import market.api.common.HelperTest;
import market.api.exception.ServiceException;

import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PLCexIoJobRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(PLCexIoJobRun.class.getName());

	PLCexIoJob plCexIoJob;

	public static void main(final String[] args) {
		try {
			HelperTest.beforeClass();

			final PLCexIoJob plCexIoJob = new PLCexIoJob();
			plCexIoJob.execute(null);
		} catch (final JobExecutionException e) {
			LOGGER.error("Error", e);
		} catch (final ServiceException exc) {
			LOGGER.error("Database Error", exc);
		}
	}

}
