package market.api.scheduler.job;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.Calendar;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.JobDetailImpl;

public class JobExecutionContextTestImpl implements JobExecutionContext {
	
	private final Map <Object, Object>hashmap = new HashMap<Object, Object>();
	private JobDetail jobDetail;

	@Override
	public Scheduler getScheduler() {
		return null;
	}

	@Override
	public Trigger getTrigger() {
		return null;
	}

	@Override
	public Calendar getCalendar() {
		return null;
	}

	@Override
	public boolean isRecovering() {
		return false;
	}

	@Override
	public TriggerKey getRecoveringTriggerKey() throws IllegalStateException {
		return null;
	}

	@Override
	public int getRefireCount() {
		return 0;
	}

	@Override
	public JobDataMap getMergedJobDataMap() {
		return null;
	}

	@Override
	public JobDetail getJobDetail() {
		if (this.jobDetail == null) {
			this.jobDetail = new JobDetailImpl();
		}
		
		return this.jobDetail;
	}

	@Override
	public Job getJobInstance() {
		return null;
	}

	@Override
	public Date getFireTime() {
		return null;
	}

	@Override
	public Date getScheduledFireTime() {
		return null;
	}

	@Override
	public Date getPreviousFireTime() {
		return null;
	}

	@Override
	public Date getNextFireTime() {
		return null;
	}

	@Override
	public String getFireInstanceId() {
		return null;
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	public void setResult(final Object result) {

	}

	@Override
	public long getJobRunTime() {
		return 0;
	}

	@Override
	public void put(final Object key, final Object value) {
		this.hashmap.put(key, value);
	}

	@Override
	public Object get(final Object key) {
		return this.hashmap.get(key);		
	}

}
