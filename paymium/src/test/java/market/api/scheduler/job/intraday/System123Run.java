package market.api.scheduler.job.intraday;

import market.api.common.HelperTest;
import market.api.exception.ServiceException;

import org.quartz.JobExecutionException;

public class System123Run {

	public static void main(final String[] args) {


		final System123 system123 = new System123();
		try {
			HelperTest.beforeClass();
			system123.execute(null);
		} catch (final JobExecutionException e) {
			e.printStackTrace();
		} catch (final ServiceException exc) {
			exc.printStackTrace();
		}
	}
}
