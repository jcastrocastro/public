package market.api.scheduler.job;

import market.api.common.HelperTest;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPrivatePaymiumApi;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetAllActiveTradesRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(GetAllActiveTradesRun.class.getName());

	public static void main(final String[] args) {
		try {
			HelperTest.beforeClass();
		} catch (final ServiceException e1) {
			LOGGER.error("Error Database ", e1);
			throw new RuntimeException(e1);
		}

		final IPrivatePaymiumApi privateApi = PrivatePaymiumApi.getInstance();

		try {
			final JSONArray jsonAliveArray = privateApi.getAllActiveOrders2();

			LOGGER.info("Number of alive trades [{}]", jsonAliveArray.length());
		} catch (final ServiceException e) {
			LOGGER.error("Error GetAllActiveTradesRun class ", e);
		}
	}

}
