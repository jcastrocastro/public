package market.api.scheduler.job;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.scheduler.job.eod.UpdateExecutedTradeMarketJob;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateOrderMakeJobTest extends HelperTest{

	private UpdateExecutedTradeMarketJob updateExecutedTradesJob;
	private JobExecutionContext jobExecutionContext;

	@Before
	public void setUp () {
		this.updateExecutedTradesJob = new UpdateExecutedTradeMarketJob();
		this.jobExecutionContext = new JobExecutionContextTestImpl();
	}

	@Ignore("Take aroung 30 secs to complete")
	@Test
	public void updateExecutedTradesJobTest() throws JobExecutionException {
		this.jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 0, 20);
		this.updateExecutedTradesJob.execute(this.jobExecutionContext);
	}

}
