package market.api.scheduler.job;

import market.api.common.HelperTest;
import market.api.scheduler.job.intraday.UpdateUserInfoJob;

import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

public class UpdateUserInfoJobTest extends HelperTest{

	private UpdateUserInfoJob updateUserInfoJob;

	@Before
	public void setUp() throws Exception {
		this.updateUserInfoJob = new UpdateUserInfoJob();

	}

	@Test
	public void executeTest() throws JobExecutionException {
		this.updateUserInfoJob.execute(null);
	}

}
