package market.api.scheduler;

import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.HelperTest;
import market.api.common.Util;
import market.api.exception.ServiceException;

import org.junit.Test;

public class SchedulerJobApiTest {

	@Test
	public void schedulerJobApiTest() throws ServiceException {
		Application.setApplicationDb(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST);
		
		Application.getSchedulerJobApi().startSchedulerPlanner();
		Application.getSchedulerJobApi().configureAllJobsInSeconds(Util.conversor(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1()));
	}

}
