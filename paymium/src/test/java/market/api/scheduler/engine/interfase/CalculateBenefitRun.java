package market.api.scheduler.engine.interfase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.exception.ServiceException;
import market.api.scheduler.engine.SimpleSellEngineJob;

public class CalculateBenefitRun {

	private static SimpleSellEngineJob iEngine;
	private static double amountBtcBuyOrder = 1.0;
	private static double priceToBuy = 1047.0;
	private static double priceToSell = 1060.0;

	private static final double PAYMIUM_FEE = 0.0059;

	private final static Logger LOGGER = LoggerFactory.getLogger(CalculateBenefitRun.class.getName());

	public static void main(final String[] args) {
		iEngine = new SimpleSellEngineJob();
		double benefit = 0.0;

		try {
			benefit = iEngine.calculateBenefitAnyPlatform(amountBtcBuyOrder, priceToBuy, priceToSell, PAYMIUM_FEE);
		} catch (final ServiceException e) {
			LOGGER.error("Error calculating benefits", e);
		}

		LOGGER.info("Benefit is [{}] for amount [{}] btc, priceToBuy [{}], priceSell [{}], fee [{}]", benefit, amountBtcBuyOrder, priceToBuy, priceToSell, PAYMIUM_FEE);
	}

}
