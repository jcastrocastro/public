package market.api.scheduler.engine.interfase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import market.api.application.ApplicationConf;
import market.api.common.HelperTest;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.SimpleSellEngineJob;

@SuppressWarnings("deprecation")
public class AEngineTest {

	AEngine iEngine;

	@Before
	public void setUp() throws Exception {
		this.iEngine = new SimpleSellEngineJob();
		ApplicationConf.setApplicationConf(HelperTest.PATH_CONFIGURATION_FILE_DB_TEST);
	}

	@Test
	public void estimatedBenefitTest() throws ServiceException{
		Assert.assertEquals(-0.58, this.iEngine.calculateBenefitAnyPlatform(0.5, 349.0, 352.0, ApplicationConf.getPlatformCostFee()));
	}

	@Test
	public void estimatedBenefitTest2() throws ServiceException{
		Assert.assertEquals(1.84, this.iEngine.calculateBenefitAnyPlatform(0.5, 349.222, 357.111, ApplicationConf.getPlatformCostFee()));
	}

	@Test
	public void estimatedBenefitTest3() throws ServiceException{
		Assert.assertEquals(-3.56, this.iEngine.calculateBenefitAnyPlatform(0.5, 352.0, 349.0, ApplicationConf.getPlatformCostFee()));
	}

	@Test
	public void estimatedBenefitTest4() throws ServiceException{
		Assert.assertEquals(0.09, this.iEngine.calculateBenefitAnyPlatform(0.3, 358.4, 363.0, ApplicationConf.getPlatformCostFee()));
	}

	@Test
	public void estimatedBenefitTest5() throws ServiceException{
		Assert.assertEquals(0.35, this.iEngine.calculateBenefitAnyPlatform(0.3, 360, 365.5, ApplicationConf.getPlatformCostFee()));
	}

	@Test
	public void estimatedBenefitTest6() throws ServiceException{
		Assert.assertEquals(0.01, this.iEngine.calculateBenefitAnyPlatform(0.3, 356, 360.3, ApplicationConf.getPlatformCostFee()));
	}
}
