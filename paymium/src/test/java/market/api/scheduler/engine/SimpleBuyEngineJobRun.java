package market.api.scheduler.engine;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.exception.ServiceException;
import market.api.scheduler.job.JobExecutionContextTestImpl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleBuyEngineJobRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleBuyEngineJobRun.class.getName());

	public static void main(final String[] args) {
		try {

			HelperTest.beforeClass();

			final SimpleBuyEngineJob simpleBuyEngineJob = new SimpleBuyEngineJob();

			final JobExecutionContext jobExecutionContext = new JobExecutionContextTestImpl();

			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 0, "500"); //  <!-- MaxAsks -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 1, "500"); //  <!-- MaxBids -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 2, "5");   //  <!-- N_TH  lowest bid-->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 3, "10");  //  <!-- Max time Update user. Minutes-->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 4, "1");  //  <!-- Decimals to consider to make a buy order -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 5, "0.1");  //  <!-- Price - Spread  -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 6, "223.97");  //  <!-- Limit buy  -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 7, "2");  //  <!-- Max Number Orders Buy -->

			simpleBuyEngineJob.execute(jobExecutionContext);
		} catch (final JobExecutionException | ServiceException exc) {
			LOGGER.error("Error running SimpleBuyEngineJobRun", exc);
		}
	}

}
