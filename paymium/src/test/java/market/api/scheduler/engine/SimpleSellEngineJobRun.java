package market.api.scheduler.engine;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.exception.ServiceException;
import market.api.scheduler.job.JobExecutionContextTestImpl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSellEngineJobRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleSellEngineJobRun.class.getName());

	public static void main(final String[] args) {
		try {
			HelperTest.beforeClass();

			final SimpleSellEngineJob simpleSellEngineJob = new SimpleSellEngineJob();

			final JobExecutionContext jobExecutionContext = new JobExecutionContextTestImpl();

			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 0, "500");   // <!-- MaxAsks -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 1, "500");   // <!-- MaxBids -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 2, "10");     // <!-- N_TH  highest ask -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 3, "1");     // <!-- Decimals to consider to make a sell order-->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 4, "0.1");   //	<!-- Price - Spread  -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 5, "195.0"); // <!-- Limit sell -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 6, "2");  	 // <!-- Max Number Orders Sell -->
			jobExecutionContext.getJobDetail().getJobDataMap().put(Constants.PARAM_QUARTZ_KEY + 7, "0.01");  // <!-- Min benefit in eur to make a sell order -->
			simpleSellEngineJob.execute(jobExecutionContext);

		} catch (final JobExecutionException | ServiceException exc) {
			LOGGER.error("Error running SimpleSellEngineJobRun", exc);
		}
	}

}
