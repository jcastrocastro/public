package market.api.connection.db.dao.impl;


import market.api.common.HelperTest;
import market.api.enumtype.Direction;
import market.api.enumtype.State;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BuySellDaoImplTest extends HelperTest {

	private BuySellDaoImpl buySellDaoImpl;

	@Before
	public void setUp() throws Exception {
		this.buySellDaoImpl = new BuySellDaoImpl();
	}

	@Test
	public void existActiverOrderTest() {
		Assert.assertTrue(this.buySellDaoImpl.existActiveOrder(Direction.BUY, 340, State.PENDING_EXECUTION));
	}

	@Test
	public void existActiverOrderAllNullTest() {
		Assert.assertEquals(4, this.buySellDaoImpl.getCollection().count());
		Assert.assertTrue(this.buySellDaoImpl.existActiveOrder(null, 340, null));
	}

	@Test
	public void existActiverOrderStateNullTest3() {
		Assert.assertTrue(this.buySellDaoImpl.existActiveOrder(Direction.BUY, 340, null));
	}

	@Test
	public void existActiverOrderDirectionNullTest3() {
		Assert.assertTrue(this.buySellDaoImpl.existActiveOrder(null, 340, State.PENDING_EXECUTION));
	}

}
