package market.api.connection.db.dao.impl;

import market.api.common.HelperTest;
import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.publicall.PublicPaymiumApi;

import org.junit.Test;

public class TickerDaoImplTest extends HelperTest {

	@Test
	public void tickerTest() throws ServiceException{
		final String json = PublicPaymiumApi.getInstance().getTicker();

		ConnectionDao.getNewTickerDao().save(json);
	}
}
