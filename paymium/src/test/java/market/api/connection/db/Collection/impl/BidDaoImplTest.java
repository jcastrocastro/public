package market.api.connection.db.Collection.impl;

import market.api.common.HelperTest;
import market.api.connection.db.dao.BidDao;
import market.api.connection.db.dao.impl.BidMarketDaoImpl;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BidDaoImplTest extends HelperTest{

	private final static Logger LOGGER = LoggerFactory.getLogger(BidDaoImplTest.class.getName());

	private BidDao bidDao;

	private int numLastBid;

	@Before
	public void setUp() {
		this.bidDao = new BidMarketDaoImpl();

		this.numLastBid = 5;
	}

	@Test
	public void getLowestBidTest() {
		final Document documentLastNth = this.bidDao.getLowestBid(this.numLastBid);
		LOGGER.info("Get {}th last bid from DB. Document {}", this.numLastBid, documentLastNth);
	}

}
