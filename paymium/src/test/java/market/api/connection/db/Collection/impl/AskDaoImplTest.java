package market.api.connection.db.Collection.impl;

import market.api.common.HelperTest;
import market.api.connection.db.dao.AskDao;
import market.api.connection.db.dao.impl.AskMarketDaoImpl;

import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AskDaoImplTest extends HelperTest{

	private final static Logger LOGGER = LoggerFactory.getLogger(AskDaoImplTest.class.getName());

	private AskDao askDao;

	private int numLastAsk;

	@Before
	public void setUp() {
		this.askDao = new AskMarketDaoImpl();

		this.numLastAsk = 5;
	}

	@Test
	public void getLastAskTest() {
		final Document documentLastNth = this.askDao.getHighestAsk(this.numLastAsk);
		LOGGER.info("Get {}th last ask from DB. Document {}", this.numLastAsk, documentLastNth);
	}

}
