package market.api.connection.db.dao.impl;

import market.api.common.HelperTest;
import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPrivatePaymiumApi;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AliveTradesHistDaoImplTest extends HelperTest{

	private final static Logger LOGGER = LoggerFactory.getLogger(AliveTradesHistDaoImplTest.class.getName());

	JSONArray jsonRecentTradeArray;

	String jsonRecentTradesString;
	String jsonActiveTradesString;

	private JSONArray jsonActiveTradeArray;

	@Before
	public void setUp () throws JobExecutionException {
		// Get alive trades from platform
		final IPrivatePaymiumApi privateApi = PrivatePaymiumApi.getInstance();

		try {
			this.jsonRecentTradesString = privateApi.getAllRecentOrders();
		} catch (final ServiceException e) {
			throw new JobExecutionException(e);
		}

		try {
			this.jsonActiveTradesString = privateApi.getAllActiveOrders2().toString();
		} catch (final ServiceException e) {
			throw new JobExecutionException(e);
		}

	}

	@Ignore
	@Test
	public void aliveTradesHistDaosTest() {
		ConnectionDao.getNewAliveTradesHistDao().insertManyWithVersion(this.jsonRecentTradeArray.toString());
	}

	@Test
	public void printTradesTest() {
		// Print active orders
		this.jsonActiveTradeArray = new JSONArray(this.jsonActiveTradesString);
		LOGGER.info("Active orders");
		for (int cnt=0; cnt< this.jsonActiveTradeArray.length(); cnt++){
			LOGGER.info("[{}]", this.jsonActiveTradeArray.get(cnt));
		}

		// Print recent orders
		this.jsonRecentTradeArray = new JSONArray(this.jsonRecentTradesString);
		LOGGER.info("Recent orders");
		for (int cnt=0; cnt< this.jsonRecentTradeArray.length(); cnt++){
			LOGGER.info("[{}]", this.jsonRecentTradeArray.get(cnt));
		}
	}

}
