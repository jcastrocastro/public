package market.api.platformPaymium.privateall;

import java.util.List;

import market.api.entity.Order;
import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserOrdersInfoRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(UserOrdersInfoRun.class.getName());

	public static void main(final String[] args) {

		// example body: www.herokuapp.com/echo?fname=Mark&lname=Bates
		//		String bodyMessage = "?offset=20";

		final String bodyMessage = "?active=true"; // only actives
//		final String bodyMessage = ""; // everything

		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(bodyMessage);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

		LOGGER.info("{}", userActivityList);

//		Util.sleepForever();
	}
}
