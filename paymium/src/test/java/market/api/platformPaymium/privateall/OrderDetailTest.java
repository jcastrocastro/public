package market.api.platformPaymium.privateall;



import java.util.List;

import org.junit.Assert;
import market.api.common.HelperTest;
import market.api.entity.Order;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.OrderDetail;
import market.api.platformPaymium.privateall.UserOrdersInfo;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class OrderDetailTest extends HelperTest {

	private OrderDetail orderDetail;
	private UserOrdersInfo userOrdersInfo;

	@Before
	public void setUp (){
		this.orderDetail = new OrderDetail();
		this.userOrdersInfo = new UserOrdersInfo();
	}
	
	@Ignore("Test more than 5 secs to be completed")
	@Test
	public void checkOrderAllTest() throws ServiceException {
		final List<Order> orderList = this.userOrdersInfo.get();
		
		for (final Order order: orderList) {					
			Assert.assertEquals(this.orderDetail.get(order.getUuid()), order);
		}
	}

	public void checkOrderOneTest() throws ServiceException {
		final List<Order> orderList = this.userOrdersInfo.get();
								
		Assert.assertEquals(this.orderDetail.get(orderList.get(0).getUuid()), orderList.get(0));		
	}
}
