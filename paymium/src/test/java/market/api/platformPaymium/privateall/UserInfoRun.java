package market.api.platformPaymium.privateall;

import market.api.common.TimeUtil;
import market.api.entity.User;
import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class UserInfoRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(UserInfoRun.class.getName());

	public static void main(final String[] args) {

		final UserInfo userInfo = new UserInfo();

		final long startTime = TimeUtil.startCountTime();

		User user = null;
		try {
			user = userInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

		TimeUtil.finishCountTime(startTime, "Updating user info.");

		LOGGER.info("User info [{}]", user);
	}
}
