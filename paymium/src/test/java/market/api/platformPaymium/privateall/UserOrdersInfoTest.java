package market.api.platformPaymium.privateall;

import java.util.List;

import market.api.common.Constants;
import market.api.common.HelperTest;
import market.api.entity.Order;
import market.api.exception.ServiceException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserOrdersInfoTest extends HelperTest{

	private final static Logger LOGGER = LoggerFactory.getLogger(UserOrdersInfoTest.class.getName());

	private String bodyMessageActive;

	private String bodyMessageCanceled;

	private String bodyMessageFilled;

	private String bodyMessagePending;

	private String bodyMessageAuthorized;

	private String bodyMessageAll;

	@Before
	public void setUp() throws Exception {
		this.bodyMessageActive = "?active=true"; // only active orders

		this.bodyMessageCanceled = "?canceled=true"; // only active orders

		this.bodyMessageFilled = "?filled=true";

		this.bodyMessagePending = "?pending=true";

		this.bodyMessageAuthorized = "?authorized=true";

		this.bodyMessageAll = Constants.EMPTY_STRING;
	}

	@Test
	public void checkAllOrderTest() {

		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessageAll);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

//		for (final Order order : userActivityList) {
//			Assert.assertEquals(State.ACTIVE.getName(), order.getState().getName());
//		}
		this.printAll(userActivityList);
	}


	@Test
	public void checkPendingOrderTest() {

		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessagePending);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

//		for (final Order order : userActivityList) {
//			Assert.assertEquals(State.ACTIVE.getName(), order.getState().getName());
//		}
		this.printAll(userActivityList);

//		LOGGER.info("{}", userActivityList);
	}

	@Test
	public void checkAuthorizedOrderTest() {

		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessageAuthorized);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

//		for (final Order order : userActivityList) {
//			Assert.assertEquals(State.ACTIVE.getName(), order.getState().getName());
//		}
		this.printAll(userActivityList);

//		LOGGER.info("{}", userActivityList);
	}

	@Test
	public void checkActiveOrderTest() {

		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessageActive);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

//		for (final Order order : userActivityList) {
//			Assert.assertEquals(State.ACTIVE.getName(), order.getState().getName());
//		}
		this.printAll(userActivityList);

//		LOGGER.info("{}", userActivityList);
	}

	@Test
	public void checkCancelOrderTest() {
		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessageCanceled);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

		this.printAll(userActivityList);

//		LOGGER.info("{}", userActivityList);
	}

	@Test
	public void checkFilledOrderTest() {
		final UserOrdersInfo userActivityInfo = new UserOrdersInfo(this.bodyMessageFilled);

		List<Order> userActivityList = null;
		try {
			userActivityList = userActivityInfo.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

//		for (final Order order : userActivityList) {
//			if (order.getState()==State.FILLED) {
//				LOGGER.info("{}",order);
//			}
//		}
//
//		LOGGER.info("{}", "NON FILLED -------------------------------");
//
//		for (final Order order : userActivityList) {
//			if (order.getState()!=State.FILLED) {
//				LOGGER.info("{}",order);
//			}
//		}
		this.printAll(userActivityList);

//		LOGGER.info("{}", userActivityList);
	}

	private void printAll(final List<Order> userActivityList) {
		for (final Order order : userActivityList) {
			LOGGER.info("{}",order);
		}
	}

}
