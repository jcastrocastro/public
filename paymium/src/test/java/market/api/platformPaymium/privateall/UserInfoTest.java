package market.api.platformPaymium.privateall;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import market.api.common.HelperTest;
import market.api.entity.User;
import market.api.exception.ServiceException;

public class UserInfoTest extends HelperTest{

	private User user;

	@Before
	public void setUp() throws ServiceException {
		final UserInfo userInfo = new UserInfo();
		
		this.user = userInfo.get();
	}
	
	@Test
	public void connectionTest() {
		Assert.assertNotNull(this.user);		
	}

}
