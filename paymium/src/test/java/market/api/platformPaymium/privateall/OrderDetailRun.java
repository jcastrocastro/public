package market.api.platformPaymium.privateall;


import market.api.entity.Order;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.OrderDetail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderDetailRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(OrderDetailRun.class.getName());
	
	public static void main(final String[] args) {

		final OrderDetail orderDetail = new OrderDetail();
		
		final String uuid ="d76d269e-1098-44dc-a3ae-b6c8e172d1c8";
		
		Order order = null;
		try {
			order = orderDetail.get(uuid);
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}

		LOGGER.info("Order detail: {}", order);
		
	}

}
