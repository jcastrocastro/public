package market.api.platformPaymium.privateall;

import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CancelOrderRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(CancelOrderRun.class.getName());
	
	public static void main(final String[] args) {		

		// instance of only the initial web page
		final CancelOrder cancelOrder = new CancelOrder();
		
		final String uuid ="76295b12-ff01-40d1-b8ef-1c639b8a2870";
		
		try {
			cancelOrder.delete(uuid);
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}		

	}

}
