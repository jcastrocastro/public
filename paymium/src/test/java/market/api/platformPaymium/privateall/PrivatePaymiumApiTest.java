package market.api.platformPaymium.privateall;


import market.api.common.HelperTest;
import market.api.exception.ServiceException;

import org.junit.Test;

public class PrivatePaymiumApiTest extends HelperTest{

	@Test
	public void testMakeLimitedSellOrder() throws ServiceException {
		PrivatePaymiumApi.getInstance().makeLimitedSellOrder(500.0, 0.01);
	}

}
