package market.api.platformPaymium.privateall;

import java.util.HashMap;
import java.util.Map;

import market.api.common.HelperTest;
import market.api.entity.TradeOrder;
import market.api.exception.ServiceException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TradingOrderRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(TradingOrderRun.class.getName());

	public static void main(final String[] args) {

		//		String bodyMessage = "?offset=20";

//		-d "type=LimitOrder"
//	     -d "currency=EUR"
//	     -d "direction=buy"
//	     -d "price=300.0"
//	     -d "amount=1.0"

//		String bodyMessage = "type=LimitOrder&currency=EUR&direction=buy&price=100.0&amount=0.01"; // body

//		String data = "product[title]=" + URLEncoder.encode(title) +
//                "&product[content]=" + URLEncoder.encode(content) +
//                "&product[price]=" + URLEncoder.encode(price.toString()) +
//                "&tags=" + tags;

//		TradeOrder tradeOrder = new TradeOrder(Constants.USER_ORDER_URL,
//											   bodyMessage);

		try {
			HelperTest.beforeClass();
		} catch (final ServiceException e1) {
			LOGGER.error("e1");
			throw new RuntimeException("Error DB");
		}

		final Map<String, String> valueByKeyTradeOrder = new HashMap<>();
		valueByKeyTradeOrder.put("type", "LimitOrder");
		valueByKeyTradeOrder.put("currency", "EUR");
		valueByKeyTradeOrder.put("direction", "buy");
		valueByKeyTradeOrder.put("price", "100");
		valueByKeyTradeOrder.put("amount", "0.01");
		final JSONObject jsonObject = new JSONObject(JSONObject.valueToString(valueByKeyTradeOrder));

		final TradingOrder tradeOrder = new TradingOrder();

		TradeOrder order = null;
		try {
			order = tradeOrder.post(jsonObject.toString());
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
 			LOGGER.error("Unexpected Error", exc);
		}

		LOGGER.info("User Order info [{}]", order);

//		Util.sleepForever();
	}
}
