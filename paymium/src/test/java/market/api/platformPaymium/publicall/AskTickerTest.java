package market.api.platformPaymium.publicall;

import market.api.common.HelperTest;
import market.api.common.TimeUtil;
import market.api.entity.Ticker;
import market.api.exception.ServiceException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AskTickerTest extends HelperTest {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTickerTest.class.getName());

	AskTicker askTicker;


	@Before
	public void setUp() throws ServiceException {
		this.askTicker = new AskTicker();

	}

	@Ignore ("Takes too long to complete")
	@Test
	public void timeChangeTickerTest() throws ServiceException, InterruptedException {
		final long startTime = TimeUtil.startCountTime();
		final Ticker tickerA = this.askTicker.get();
		LOGGER.info("First Ticker price [{}}", tickerA.getPrice());
		for (int cnt=0; cnt<10000; cnt++){
			Thread.sleep(1000);
			final Ticker tickerB = this.askTicker.get();
			LOGGER.info("Next Ticker price [{}}", tickerA.getPrice());
			if (!tickerA.equals(tickerB)){
				LOGGER.info("A price [{}} is different to B price [{}]", tickerA.getPrice(), tickerB.getPrice());
				TimeUtil.finishCountTime(startTime, "Change ticker.");
				LOGGER.info("Exit in cnt [{}]",cnt);
				return;
			}
		}

		LOGGER.info("A and B are always the same");
	}

	@Test
	public void tickerTest() throws ServiceException{
		final Ticker ticker = this.askTicker.get();

		LOGGER.info("{}", ticker);
	}

//	@Test
//	public void getSavingTickerUsingJsonTest() throws ServiceException {
//
//		final String jsonString = this.askTicker.getJson();
//
//		LOGGER.info("JsonString: [{}]", jsonString);
//
//		final Document doc = Document.parse(jsonString);
//
//		final long beforeCount = this.testCollection.count();
//
//		this.testCollection.insertOne(doc);
//
//		final long afterCount = this.testCollection.count();
//
//		Assert.assertEquals(afterCount, beforeCount+1);
//	}

}
