package market.api.platformPaymium.publicall;

import java.util.List;

import market.api.common.Util;
import market.api.entity.Trade;
import market.api.exception.ServiceException;
import market.api.platformPaymium.publicall.LatestOrderMade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LatestTradesRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(LatestTradesRun.class.getName());
	
	public static void main(final String[] args) {
		final LatestOrderMade testAsk = new LatestOrderMade();
		
		List<Trade> tradeList = null;
		try {
			tradeList = testAsk.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}
		
		if (!Util.isEmpty(tradeList)){
			LOGGER.info("{}", tradeList);
		}
	}

}
