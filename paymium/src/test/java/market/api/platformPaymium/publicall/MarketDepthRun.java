package market.api.platformPaymium.publicall;

import market.api.entity.AskBidDepth;
import market.api.exception.ServiceException;
import market.api.platformPaymium.publicall.MarketDepth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MarketDepthRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(MarketDepthRun.class.getName());

	public static void main(final String[] args) {
		final MarketDepth marketDepth = new MarketDepth();
		
		AskBidDepth askBidDepth = null;
		try {
			askBidDepth = marketDepth.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error service exception");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}
		
		if (askBidDepth != null){
			LOGGER.info("Bids: {}", askBidDepth.getBids());
			LOGGER.info("Asks: {}", askBidDepth.getAsks());
		}
	}

}
