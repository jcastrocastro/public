package market.api.platformPaymium.publicall;

import market.api.entity.Ticker;
import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AskTickerRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTickerRun.class.getName());
	
	public static void main(final String[] args) {
		
		final AskTicker testAsk = new AskTicker();
		
		Ticker ticker = null;
		try {
			ticker = testAsk.get();
		} catch (final ServiceException e) {
			LOGGER.error("Error IOException");
		} catch (final Exception exc) {
			LOGGER.error("Unexpected Error", exc);
		}
		
		if (ticker != null){
			LOGGER.info("{}", ticker);
		}
			
	}
}
