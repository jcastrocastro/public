package market.api.platformPaymium.publicall;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import market.api.exception.ServiceException;

public class PublicApiTest {

	private String json;
	
	private String bidString;

	private String askString;

	private String lastBidString;

	private String lastAskString;

	@Before
	public void setUp() throws FileNotFoundException {	
		final Scanner scanner = new Scanner(new File("src/test/resources/exampleMarketDepth"));
		this.json = scanner.useDelimiter("\\Z").next();
		scanner.close();
		
		this.bidString = "{\"timestamp\":1443192457,\"amount\":0.01,\"price\":208.5,\"currency\":\"EUR\"}";
		this.askString = "{\"timestamp\":1443196809,\"amount\":0.70358314,\"price\":214.089,\"currency\":\"EUR\"}";
		
		this.lastBidString = "[{\"timestamp\":1443192457,\"amount\":0.01,\"price\":208.5,\"currency\":\"EUR\"},{\"timestamp\":1443178057,\"amount\":0.49726611,\"price\":208.50004,\"currency\":\"EUR\"},{\"timestamp\":1443199756,\"amount\":0.31267942,\"price\":209.0,\"currency\":\"EUR\"},{\"timestamp\":1443204445,\"amount\":1.46503574,\"price\":209.00002,\"currency\":\"EUR\"},{\"timestamp\":1443204468,\"amount\":2.60807145,\"price\":209.00002001,\"currency\":\"EUR\"}]"; 
		
		this.lastAskString = "[{\"timestamp\":1443204368,\"amount\":1.28520477,\"price\":212.99998999,\"currency\":\"EUR\"},{\"timestamp\":1443202017,\"amount\":0.3,\"price\":212.99999,\"currency\":\"EUR\"},{\"timestamp\":1443204231,\"amount\":1.50652079,\"price\":213.45999,\"currency\":\"EUR\"},{\"timestamp\":1442147475,\"amount\":0.21330544,\"price\":213.46,\"currency\":\"EUR\"},{\"timestamp\":1443196809,\"amount\":0.70358314,\"price\":214.089,\"currency\":\"EUR\"}]";
	}


	@Ignore
	@Test
	public void marketDepthJsonTest() throws ServiceException{
		Assert.assertNotNull(PublicPaymiumApi.getInstance().getMarketDepthJson(5, 5));
	}	

	@Test
	public void getBids(){		
		Assert.assertEquals(this.bidString, PublicPaymiumApi.getInstance().getBid(this.json, 5));		
	}
	
	@Test
	public void getAsks(){
		Assert.assertEquals(this.askString, PublicPaymiumApi.getInstance().getAsk(this.json, 5));		
	}

	@Test
	public void getLastBids(){		
		Assert.assertEquals(this.lastBidString, PublicPaymiumApi.getInstance().getLastBid(this.json, 5));		
	}
	
	@Test
	public void getLastAsks(){
		Assert.assertEquals(this.lastAskString, PublicPaymiumApi.getInstance().getLastAsk(this.json, 5));		
	}

	
}
