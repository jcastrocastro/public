package market.api.platformPaymium.publicall;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import market.api.exception.ServiceException;

public class MarketDepthTest {	
	
	private MarketDepth marketDepth;
	
	@Before
	public void setUp () throws FileNotFoundException {
		this.marketDepth = new MarketDepth();
	}	
		
	@Test
	public void getStringTest() throws ServiceException {
		final String retJson = this.marketDepth.getJson();
		Assert.assertNotNull(retJson);
	}

}
