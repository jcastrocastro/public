package market.email;

import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SenderEmailRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(SenderEmailRun.class.getName());

	private final static SenderEmail senderEmail = new SenderEmail();

	public static void main(final String[] args) {
		try {
			final String response = senderEmail.sendSimpleMessage("Error Paymium Application", "This is only for testing. HA HA HA HA");
			LOGGER.info("Response [{}]", response);
		} catch (final ServiceException sexc) {
			LOGGER.error("");
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
		}
	}

}
