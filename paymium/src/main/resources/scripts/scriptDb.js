// Run server: > mongod
// Run javascript interpreter: > mongo

// Change prompt
prompt = function() {
             return db+"$ ";
         }

// Create database if it doesn't exist
//use paymium
db = connect("localhost:27017/paymium");

// tokens
// db.token.remove({})
// db.token.insert( {"_id":"1", "Hello":"xxxx", "Api-Key":"yyyy"} )

// Drop Database
// db.dropDatabase(

// See buySell trades
db.buySell.find({},{"state":1, "direction":1, "price":1})

// Limits to execute
db.orderCounter.insert( {"_id":"1", "executed": 0, "remained":86400, "maximum":86400,  "lastModified": new Date() } )
// db.maxOrdersByDay.update(
   // { _id: 1 },
   // {
     // $currentDate: {
        // "lastModified": true,
        // "lastModification.date": { $type: "timestamp" }
     // },
     // $set: {
        // "executed": 0,
        // "remained": 86400,
		// "maximum": 86400,
     // }
   // }
// )

// Most used queries in MongoDb -------------------------------------------------------------------------------------------------

// State mark_appl_sell_state
db.buySell.find({"state" : "mark_appl_sell_state"})

// Last benefit
db.benefit.find().sort({"updated_at":-1})

// alive trades direction, price, created_at
 db.aliveTrades.find({},{"price":1, "direction":1, "created_at":1}).sort({"created_at":-1})
 
 // Count alive trades
 db.aliveTrades.count()
 

