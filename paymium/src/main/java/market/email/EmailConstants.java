package market.email;

public class EmailConstants {

	private EmailConstants(){}

	static final String USER_NAME = "api";

	// Email addresses
	static final String FROM_EMAIL = "Mailgun Sandbox <postmaster@sandboxbe22828269984d88a52485fa8633bb21.mailgun.org>";
	static final String TO_EMAIL = "Jose Raul <raul1327@yahoo.es>";
	static final String URI_RESOURCE = "https://api.mailgun.net/v3/sandboxbe22828269984d88a52485fa8633bb21.mailgun.org/messages";

	// Email Headers
	static final String FROM = "from";
	static final String TO = "to";
	static final String SUBJECT = "subject";
	static final String TEXT = "text";
}
