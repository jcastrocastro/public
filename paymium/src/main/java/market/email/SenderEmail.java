package market.email;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.ws.rs.core.MediaType;

import market.api.exception.ServiceException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import web.StartController;

public class SenderEmail {

	private final static Logger LOGGER = LoggerFactory.getLogger(SenderEmail.class.getName());

	private static final String SENT_MESSAGE = "Already sent the stop message";

	private final WebResource webResource;
	private final AtomicBoolean isOn;


	public WebResource getWebResource() {
		return this.webResource;
	}

	public SenderEmail(){
		this.isOn = new AtomicBoolean(false);
		final Client client = Client.create();
		client.addFilter(new HTTPBasicAuthFilter(EmailConstants.USER_NAME, "key-a56f743a1f1b20d548d0aa3ac60127a5"));
	    this.webResource = client.resource(EmailConstants.URI_RESOURCE);
	}

	/**
	 * Send a simple message with the given text msg as a text
	 * @param subject
	 * @param textMsg
	 * @return
	 * @throws ServiceException Error non getting 200 Ok status
	 */
	public String sendSimpleMessage(final String subject, final String textMsg)
													throws ServiceException {

		String retResponse = SENT_MESSAGE;

		if (this.isOn.compareAndSet(false, true)) {
			final MultivaluedMapImpl formData = new MultivaluedMapImpl();
			formData.add(EmailConstants.FROM, EmailConstants.FROM_EMAIL);
			formData.add(EmailConstants.TO, EmailConstants.TO_EMAIL);
			formData.add(EmailConstants.SUBJECT, subject);
			formData.add(EmailConstants.TEXT, textMsg);
			final ClientResponse clientResponse = this.getWebResource().type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);

			if (clientResponse.getStatus() != 200) {
				LOGGER.error("Error sending email");
				throw new ServiceException(String.format("Failed : HTTP error code : %s", clientResponse.getStatus()));
			}

			retResponse = clientResponse.getEntity(String.class);
		}

		return retResponse;
	}

}
