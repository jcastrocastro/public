package market.email;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;

/**
 * I created this class to Receive email: IMAP o POP3. But finally I haven´t used in the project. I keep it here in
 * case I can reuse the code in the near future.
  */

public class ReadingEmail {
    public static void main(final String[] args) {
        final Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        props.put("mail.imap.ssl.enable", "true"); // required for Gmail
        props.put("mail.imap.sasl.enable", "true");
        props.put("mail.imap.auth.login.disable", "true");
        props.put("mail.imap.auth.plain.disable", "true");

        try {
            final Session session = Session.getInstance(props);
            final Store store = session.getStore();
            store.connect("imap.gmail.com", "raul1327a@gmail.com", "ysemii1327");
            final Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);
            final Message msg = inbox.getMessage(inbox.getMessageCount());
            final Address[] in = msg.getFrom();
            for (final Address address : in) {
                System.out.println("FROM:" + address.toString());
            }
            final Multipart mp = (Multipart) msg.getContent();
            final BodyPart bp = mp.getBodyPart(0);
            System.out.println("SENT DATE:" + msg.getSentDate());
            System.out.println("SUBJECT:" + msg.getSubject());
            System.out.println("CONTENT:" + bp.getContent());
        } catch (final Exception mex) {
            mex.printStackTrace();
        }
    }
}
