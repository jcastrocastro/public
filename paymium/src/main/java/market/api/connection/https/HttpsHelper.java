package market.api.connection.https;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.Util;
import market.api.enumtype.HttpMethod;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpsHelper {

	private final static Logger LOGGER = LoggerFactory.getLogger(HttpsHelper.class.getName());

	/**
	 * To avoid being instantiated
	 */
	private HttpsHelper() {}

	/**
	 * Public data
	 * @param urlWeb
	 * @param format
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static BufferedReader getPublicReader(final String urlWeb, final String format) throws UnsupportedEncodingException, IOException {
		final URL url = new URL(urlWeb);

		LOGGER.trace("Making a public transaction for url [{}]", urlWeb);
		return new BufferedReader(new InputStreamReader(url.openStream(), format));
	}

	/**
	 * Get for private data
	 * @param urlWeb
	 * @param format
	 * @param prop
	 * @param httpMethod
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static BufferedReader getPrivateReader(final String urlWeb, final String format, final Properties prop, final HttpMethod httpMethod)
																						throws UnsupportedEncodingException, IOException {
		final BufferedReader retBufferedReader = null;

		if (Util.isEmpty(prop)){
			LOGGER.error("Prop is empty");
			return retBufferedReader;
		}

		LOGGER.trace("URL web [{}]", urlWeb);

		return getPrivateBufferedReader(HttpsHelper.getHttpUrlConnection(urlWeb, prop, httpMethod), format);
	}

	/**
	 * Post for private data
	 * @param urlWeb
	 * @param format
	 * @param prop
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static BufferedReader postPrivateReader(final String urlWeb, final String format, final Properties prop) throws UnsupportedEncodingException, IOException {
		final BufferedReader retBufferedReader = null;

		if (Util.isEmpty(prop)){
			return retBufferedReader;
		}

		final HttpURLConnection httpUrlConn = getHttpUrlConnection(urlWeb, prop, HttpMethod.POST);

		// Send post request
		LOGGER.trace("Post body: {}", prop.getProperty(Constants.BODY_HTTP_MSG));
		writePostBody(httpUrlConn, prop.getProperty(Constants.BODY_HTTP_MSG));

		return getPrivateBufferedReader(httpUrlConn, format);
	}

	private static HttpURLConnection getHttpUrlConnection(final String urlWeb, final Properties prop, final HttpMethod httpMethod) throws IOException {
		final URL url = new URL(urlWeb);

		final HttpURLConnection httpUrlConn = (HttpURLConnection)url.openConnection();

		httpUrlConn.setRequestMethod(httpMethod.getName());

		//add three request headers
		httpUrlConn.setRequestProperty(Constants.API_KEY, prop.getProperty(Constants.API_KEY));
		LOGGER.trace("API_KEY [{}]", prop.getProperty(Constants.API_KEY));
		httpUrlConn.setRequestProperty(Constants.API_SIGNATURE, prop.getProperty(Constants.API_SIGNATURE));
		LOGGER.trace("API_SIGNATURE [{}]", prop.getProperty(Constants.API_SIGNATURE));
		httpUrlConn.setRequestProperty(Constants.API_NONCE, prop.getProperty(Constants.API_NONCE));
		LOGGER.trace("API_NONCE [{}]", prop.getProperty(Constants.API_NONCE));

		return httpUrlConn;
	}

	/**
	 * Writes post body in JSON format
	 * @param httpUrlConn
	 * @param bodyJson Body in JSON format
	 * @throws IOException
	 */
	private static void writePostBody(final HttpURLConnection httpUrlConn, final String bodyJson)
																							throws IOException {
		// Json message

		httpUrlConn.setRequestProperty(Constants.CONTENT_TYPE, new StringBuilder("application/json; charset=").append(Constants.UTF_8).toString());
		LOGGER.trace("Post Content-Type [{}], [{}]", Constants.CONTENT_TYPE, httpUrlConn.getRequestProperty(Constants.CONTENT_TYPE));

		httpUrlConn.setDoInput(true);
		httpUrlConn.setDoOutput(true);
		final OutputStream wr = httpUrlConn.getOutputStream();
		wr.write(bodyJson.getBytes(Constants.UTF_8));
		wr.flush();
		wr.close();
	}

	private static synchronized BufferedReader getPrivateBufferedReader(final HttpURLConnection httpUrlConn, final String format)
																													throws UnsupportedEncodingException, IOException {
		LOGGER.trace("Making a private transaction for url [{}]", httpUrlConn.getURL());
		return new BufferedReader(new InputStreamReader(httpUrlConn.getInputStream()));
	}

	public static BufferedReader getPublicReader2(final String urlWeb, final String format) throws IOException {
		final URL url = new URL(urlWeb);

		LOGGER.trace("Making a public transaction for url [{}]", urlWeb);

		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		return new BufferedReader(new InputStreamReader(conn.getInputStream()));
	}

	public static BufferedReader getPublicReaderCexIo(final String urlWeb) throws IOException {
		final URL url = new URL(urlWeb);

		final HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		// add request header
		conn.setRequestProperty("User-Agent", Constants.WEB_USER_AGENT);

		return new BufferedReader(new InputStreamReader(conn.getInputStream()));
	}

}
