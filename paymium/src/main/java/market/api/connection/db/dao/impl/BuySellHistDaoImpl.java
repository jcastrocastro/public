package market.api.connection.db.dao.impl;

import market.api.common.Constants;
import market.api.connection.db.BuySellHistDao;
import market.api.connection.db.ConnectionDbApi;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;

public class BuySellHistDaoImpl implements BuySellHistDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(BuySellHistDaoImpl.class.getName());

	private static final String NAME_BUYSELL_HIST_NAME = "buySellHist";

	private final String currentPk = Constants.ORDER_PK;

	private final MongoCollection<Document> collection;

	/**
	 * Index PRICE
	 */
	public BuySellHistDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_BUYSELL_HIST_NAME);
	}

	@Override
	public String getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}

	@Override
	public String getCurrentPk() {
		return this.currentPk;
	}

	private MongoCollection<Document> getCollection() {
		return this.collection;
	}


	@Override
	public void insertOrUpdate(final String jsonObjectString) {
		LOGGER.trace("Insert or update one json [{}] in collection name [{}]", jsonObjectString, this.getCollection().getNamespace());
		UtilDb.saveOrUpdateOneBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());

	}

}
