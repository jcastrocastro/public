package market.api.connection.db.dao;

import java.util.List;

import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;

import org.bson.Document;

public interface BuySellDao {

	/**
	 * Inserts the trade, calculates fees and saves them
	 *
	 * @param jsonObjectString
	 * @throws ServiceException Error rounding decimals
	 */
	void insertOneTradeAddingFeeInfo(String jsonObjectString) throws ServiceException;

	/**
	 * Number of orders with the given direction
	 *
	 * @param direction Direction to look for
	 * @return Number of orders
	 */
	long numberOrders(Direction direction);

	/**
	 * Number of orders with the given direction
	 *
	 * @param direction Direction to look for
	 * @param state State of the trade
	 *
	 * @return Number of orders
	 */
	long numberOrders(Direction direction, State state);

	/**
	 * Gets cost of all trades with the given direction, marked.
	 *
	 * @param direction BUY or SELL
	 * @return Total cost of all trades
	 * @throws ServiceException Wrong number of decimals to truncate or the platform fee is undefined
	 */
	double getCostMarkedTrades(Direction direction) throws ServiceException;

	/**
	 * Inserts or updates the trade without calculate the fees
	 * @param jsonObjectString
	 */
	void insertOrUpdate(String jsonObjectString);

	/**
	 * Gets a list of documents which have the given state and direction from the collection BuySell and they don't have the mark
	 * <p>
	 * The list is sorted by the given filter
	 *
	 * @param state State to look for
	 * @param direction Direction to look for
	 * @param sortBy
	 * @param sortFilter
	 * @return List of documents
	 */
	List<Document> getSortDocumentList(final State state,
		  	   									final Direction direction,
		  	   									final Document sortFilter);

	/**
	 * Gets a list of documents which have the direction from the collection BuySell and they don't have the mark
	 *
	 * @param state State to look for
	 * @param direction Direction to look for
	 * @return List of documents
	 */
	List<Document> getDocumentListMarked(Direction direction);

	/**
	 * Gets name of the collection
	 * @return
	 */
	String getNameCollection();

	/**
	 * Gets current primary key
	 * @return
	 */
	String getCurrentPk();

	/**
	 * Exists buy order with the given direction, price To buy order and state.
	 *
	 * @param directionValue Direction
	 * @param priceToBuy Price to buy
	 * @param state
	 * @return True if exists the order, otherwise false
	 */
	boolean existActiveOrder(Direction directionValue, double priceToBuy, State state);

	/**
	 * Exists buy order with the given direction and price value order
	 *
	 * @param directionValue Direction
	 * @param priceValue Price value
	 * @param state
	 * @return True if exists the order, otherwise false
	 */
	boolean existActiveOrder(Direction buy, double priceValue);

	/**
	 * Remove document
	 * @param doc
	 */
	void delete(Document doc);

	/**
	 * Changes state sell mark
	 *
	 * @param doc document to look for
	 * @param stateKey state key
	 * @param newStateValue new state value
	 * @return Changed state document or null if the given parameter doc is null
	 */
	Document changeStateSellMarkAndSave(Document doc, String stateKey, State newStateValue);

	/**
	 * Gets document from Collection
	 * @param doc
	 * @return The document or null if not found
	 */
	Document getDocument(Document doc);

	/**
	 *
	 * @param key0
	 * @param value0
	 * @param state
	 * @return
	 */
	Document getDocument(String key0, double value0, State state);


	/**
	 * Get one document which has the given key and value
	 * @param stringKey
	 * @param stringValue
	 * @return
	 */
	Document getDocument(String stringKey, String stringValue);




}
