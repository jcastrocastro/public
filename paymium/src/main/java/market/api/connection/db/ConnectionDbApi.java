package market.api.connection.db;

import market.api.application.configuration.Configuration;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


public class ConnectionDbApi {

	private final static Logger LOGGER = LoggerFactory.getLogger(ConnectionDbApi.class.getName());

	private static IConnection connection;

	private static Configuration configuration;

	private static ConnectionMongoDb connectionMongoDb;

	private static int port;

	private static String host;

	private static String databaseName;

	private ConnectionDbApi () {}

	public static int getPort() {
		return port;
	}

	public static String getHost() {
		return host;
	}

	public static String getDatabaseName() {
		return databaseName;
	}

	public static void setConnectionProp(final Configuration configurationProperties) {
		configuration = configurationProperties;
		databaseName = configuration.getDatabasename();
		host = configuration.getHost();
		port = configuration.getPort();
	}

	public static ConnectionMongoDb getConnectionMongoDb() {
		if (connectionMongoDb==null){
			connectionMongoDb = new ConnectionMongoDb(getConfigurationdb());
		}
		return connectionMongoDb;
	}

	static Configuration getConfigurationdb() {
		return configuration;
	}

	/**
	 *
	 * Set connection
	 * @param conn
	 */
	public static void setConnection(final IConnection conn) {
		connection = conn;
	}

	/**
	 * Get the Database or null if the database is not supported
	 * @return
	 */
	private static MongoDatabase getDatabase() {
		MongoDatabase retDatabase = null;
		if (connection instanceof ConnectionMongoDb) {
			retDatabase = ((ConnectionMongoDb)connection).getDatabase();
		} else {
			LOGGER.error("Database non supported. Can't get it");
		}

		return retDatabase;
	}

	/**
	 * Closes connection with the DB
	 */
	public static void close () {
		if (connection !=null){
			connection.close();
		}
	}

	/**
	 * Gets a collection of the DB.
	 * <p>
	 * If the collection doesn't exist in Db, then it will be created
	 * @param collectionName Collection name
	 * @return
	 */
	public static MongoCollection<Document> getCollection (final String collectionName) {
		LOGGER.trace("Getting collection [{}] from DB [{}]", collectionName, getDatabase().getName());
		return getDatabase().getCollection(collectionName);
	}


}
