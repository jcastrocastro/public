package market.api.connection.db;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class DatabaseUtil {

	/**
	 * Gets all documents from one table
	 * @param tableName
	 * @return
	 */
	public static List<Document> getCollection (final String tableName) {
		final List<Document> retDocs = new ArrayList<>();
		
		final MongoCollection<Document> collection = ConnectionDbApi.getCollection(tableName);
		
		final MongoCursor<Document> cursor = collection.find().iterator();
		try {
		    while (cursor.hasNext()) {
		        retDocs.add(cursor.next());
		    }
		} finally {
		    cursor.close();
		}
		
		return retDocs;
	}

}
