package market.api.connection.db.dao;

import org.bson.Document;

public interface BidDao {

	void saveOrUpdateMany(String json);

	void saveOrUpdateOne(final Document document);

	void deleteMany();

	void insertMany(String jsonObjectString);

	/**
	 * Gets the lowest th bid saved in DB
	 * <pre>
	 * Test query:
	 * db.bid.find({"_id":{$gte:1443720052}}).sort({"price":-1})
	 * </pre>
	 * @param numberTh The first lowest starts in 1 and next ones so on.
	 * @return
	 */
	Document getLowestBid(int numberTh);
}
