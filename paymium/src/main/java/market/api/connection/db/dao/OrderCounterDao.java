package market.api.connection.db.dao;

import org.bson.Document;

public interface OrderCounterDao {

	Document getCurrentMaxOrder();

	void incOrder();

	void reset();


}
