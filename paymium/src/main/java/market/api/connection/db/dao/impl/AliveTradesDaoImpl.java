package market.api.connection.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.AliveTradesDao;
import market.api.entity.TradeOrder;
import market.api.enumtype.Direction;
import market.api.enumtype.State;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class AliveTradesDaoImpl implements AliveTradesDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(AliveTradesDaoImpl.class.getName());

	/**
	 * Primary key of the collection
	 */
	private final String currentPk = Constants.ORDER_PK;

	private static final String NAME_TABLE_ALIVE_TRADES = "aliveTrades";

	private final MongoCollection<Document> collection;

	/**
	 * Index: DIRECTION and PRICE
	 */
	public AliveTradesDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_ALIVE_TRADES);

		// Creates index for alive trades collection
		Util.createIndex(TradeOrder.DIRECTION, 1, TradeOrder.PRICE, 1, this.collection);
	}

	@Override
	public String getNameTableAliveTrades() {
		return this.getCollection().getNamespace().toString();
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	/**
	 * Get current primary key
	 * @return
	 */
	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public void deleteMany() {
		LOGGER.trace("Delete all documents from collection [{}]", this.getCollection().getNamespace());
		this.getCollection().deleteMany(new Document());
	}

	@Override
	public void insertOrUpdateMany(final String jsonObjectString) {
		UtilDb.saveOrUpdateManyBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public boolean existActiveOrder(final String directionLabel, final String directionValue,
									final String priceLabel, final double priceValue) {

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists alive trade in collection [{}] with key0 [{}] and value0 [{}], key1 [{}] and value1 [{}]",
																		new Object[]{this.getNameTableAliveTrades(),
																					 directionLabel, directionValue,
																					 priceLabel, priceValue});

		boolean retValue = false;
		FindIterable<Document> iterable = null;
		synchronized (this) {
			iterable = this.getCollection().find(new Document(directionLabel, directionValue).
					  							 append(priceLabel, priceValue).
					  							 append(TradeOrder.STATE, State.ACTIVE.getName())).
					  							 limit(1);
		}

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				retValue = true;
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value. Next key0 [%s] and value0 [%s], Next key1 [%s] and value1 [%s]",
																								new Object[]{directionLabel, directionValue, priceLabel, priceValue}));

		return retValue;
	}

	@Override
	public boolean existActiveOrder(final String directionLabel, final Direction directionValue,
									final String priceLabel, final double priceValue,
									final String stateLabel, final State stateValue) {

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists alive trade in collection [{}] with key0 [{}] and value0 [{}], key1 [{}] and value1 [{}], key2 [{}] and value2 [{}]",
																									new Object[]{this.getNameTableAliveTrades(),
																												 directionLabel, directionValue.getName(),
																					 							 priceLabel, priceValue,
																					 							 stateLabel, stateValue.getName()});

		boolean retValue = false;
		FindIterable<Document> iterable = null;
		synchronized (this) {
			iterable = this.getCollection().find(new Document(directionLabel, directionValue.getName()).
																				  append(priceLabel, priceValue).
																				  append(stateLabel, stateValue.getName())).
																				  limit(1);
		}

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				retValue = true;
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value. Next key0 [%s] and value0 [%s], Next key1 [%s] and value1 [%s], Next key2 [%s] and value2 [%s]",
																									new Object[]{directionLabel, directionValue.getName(),
				 																								 priceLabel, priceValue,
				 																								 stateLabel, stateValue.getName()}));
		return retValue;
	}

	@Override
	public void insertMany(final String jsonObjectString) {
		UtilDb.insertMany(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public long count() {
		final String nameCollection = this.getNameTableAliveTrades();
		LOGGER.trace("Counting collection [{}]", nameCollection);
		final long startTime = TimeUtil.startCountTime();
		final long retLong = this.getCollection().count();
		TimeUtil.finishCountTime(startTime, String.format("Number of documents [%d] in database [%s]", retLong, nameCollection));
		return retLong;
	}

	@Override
	public long count(final Direction direction) {
		final String nameCollection = this.getNameTableAliveTrades();
		LOGGER.trace("Counting collection [{}] with direction [{}]", nameCollection, direction);
		final long startTime = TimeUtil.startCountTime();
		final long retLong = this.getCollection().count(new Document(TradeOrder.DIRECTION, direction.getName()));
		TimeUtil.finishCountTime(startTime, String.format("Number of documents [%d] in database [%s] with direction [%s]", retLong, nameCollection, direction.getName()));
		return retLong;
	}

	@Override
	public long count(final Direction direction, final double price, final State state) {
		final String nameCollection = this.getNameTableAliveTrades();
		LOGGER.trace("Counting collection [{}] with direction [{}]", nameCollection, direction);
		final long startTime = TimeUtil.startCountTime();
		final long retLong = this.getCollection().count(new Document(TradeOrder.DIRECTION, direction.getName()).
																					append(TradeOrder.PRICE, price).
																					append(TradeOrder.STATE, state.getName()));

		TimeUtil.finishCountTime(startTime, String.format("Count number doc [%d] in database [%s] with price [%.2f] and direction [%s]",
																										retLong,
																										nameCollection,
																										price,
																										direction.getName()));
		return retLong;
	}

	@Override
	public List<Document> getAliveTrades(final String directionLabel, final Direction directionValue) {
		final List<Document> retList = new ArrayList<>();

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists alive trade in collection [{}] with key0 [{}] and value0 [{}]",
																					new Object[]{this.getNameTableAliveTrades(),
																					 			 directionLabel,
																					 			 directionValue.getName()});



		final FindIterable<Document> iterable = this.getCollection().find(new Document(directionLabel, directionValue.getName()));

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			while (cursor.hasNext()) {
		    	retList.add(cursor.next());
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value. Next key0 [%s] and value0 [%s]",
																									new Object[]{directionLabel,
																												 directionValue.getName()}));

		return retList;
	}

	@Override
	public List<Document> getAliveTrades(final String directionLabel, final Direction directionValue,
										 final String priceLabel, final int priceValue) {

		final List<Document> retList = new ArrayList<>();

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists alive trade in collection [{}] with key0 [{}], value0 [{}] and greater key1 [{}] and value1 [{}]",
																					new Object[]{this.getNameTableAliveTrades(),
																					 			 directionLabel, directionValue.getName(),
																					 			 priceLabel, priceValue});

		final FindIterable<Document> iterable = this.getCollection().find(new Document(directionLabel, directionValue.getName()).
																				append(priceLabel, new Document(Constants.GREATER_THAN, priceValue)));
		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			while (cursor.hasNext()) {
		    	retList.add(cursor.next());
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value. Next key0 [%s] and value0 [%s] and greater key1 [{}] and value1 [{}]",
																									new Object[]{directionLabel,
																												 directionValue.getName(),
																												 priceLabel, priceValue}));

		return retList;
	}

	@Override
	public List<Document> getAliveTradesSort(final String directionLabel, final Direction directionValue,
			  								 final String priceLabel, final int priceValue,
			  								 final String stateLabel, final State stateValue,
			  								 final Document sortDocument){

		final List<Document> retList = new ArrayList<>();

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists alive trade in collection [{}] with key0 [{}], value0 [{}] and greater key1 [{}] and value1 [{}]. Sort [{}]",
																					new Object[]{this.getNameTableAliveTrades(),
																					 			 directionLabel, directionValue.getName(),
																					 			 priceLabel, priceValue,
																					 			 sortDocument});

		final FindIterable<Document> iterable = this.getCollection().find(new Document(directionLabel, directionValue.getName()).
																			    append(stateLabel, stateValue.getName()).
																				append(priceLabel, new Document(Constants.GREATER_THAN, priceValue))).
																				sort(sortDocument);
		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			while (cursor.hasNext()) {
		    	retList.add(cursor.next());
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value. Next key0 [%s] and value0 [%s] and greater key1 [%s] and value1 [%s]",
																									new Object[]{directionLabel,
																												 directionValue.getName(),
																												 priceLabel, priceValue}));

		return retList;
	}
}
