package market.api.connection.db.dao;

import java.time.LocalDateTime;

import org.bson.Document;

import com.mongodb.client.MongoCollection;


public interface TickerDao {

	void save(String json);

	String getNameCollection();

	MongoCollection<Document> getCollection();

	String getCurrentPk();

	/**
	 * Get the last ticker for the given date if exists, otherwise null
	 * @param localDateTime
	 * @return
	 */
	Document getLastTicker(LocalDateTime localDateTime);

}
