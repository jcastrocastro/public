package market.api.connection.db;

import market.api.application.configuration.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class ConnectionMongoDb implements IConnection {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ConnectionMongoDb.class.getName());
	
	private final MongoClient mongoClient;

	private final MongoDatabase mongoDatabase;

	private final Configuration configuration;
	
	public ConnectionMongoDb(final Configuration configuration) {
		this.configuration = configuration;		
		this.mongoClient = new MongoClient(configuration.getHost(), configuration.getPort());
		LOGGER.info("Connected to DB. Host [{}], Port [{}]", configuration.getHost(), configuration.getPort());		
		this.mongoDatabase = this.mongoClient.getDatabase(this.configuration.getDatabasename());
		LOGGER.info("Connected to Database [{}]", configuration.getDatabasename());
	}	

	@Override
	public MongoClient getConnectionClient() {		
		return this.mongoClient;		
	}
	
	@Override
	public MongoDatabase getDatabase() {
		return this.mongoDatabase;
	}
	
	@Override
	public void close(){
		LOGGER.info("Closing mongoDB client host [{}], port [{}]", this.configuration.getHost(), this.configuration.getPort());
		
		if (this.mongoClient!=null) { 		
			this.mongoClient.close();
		}		
	}

}
