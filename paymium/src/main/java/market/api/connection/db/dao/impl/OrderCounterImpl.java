package market.api.connection.db.dao.impl;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.OrderCounterDao;

import org.bson.BsonInt32;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class OrderCounterImpl implements OrderCounterDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(OrderCounterImpl.class.getName());

	private static final String ORDERS_BY_DAY_TABLE_NAME = "orderCounter";
	private static final String EXECUTED = "executed";
	private static final String REMAINED = "remained";
	private static final String MAXIMUM = "maximum";
	private static final String LAST_MODIFIED = "lastModified";
	private static final String PK_VALUE = "1";

	private static final int MAX_ORDERS_BY_DAY = 86400;

	private final String currentPk = Constants.STANDART_PK_MONGODB;

	private final MongoCollection<Document> collection;

	public OrderCounterImpl(){
		this.collection = ConnectionDbApi.getCollection(ORDERS_BY_DAY_TABLE_NAME);
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	public static String getNameTableBuyOrder() {
		return ORDERS_BY_DAY_TABLE_NAME;
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public Document getCurrentMaxOrder() {
		Document doc = null;

		final FindIterable<Document> iterable = this.getCollection().find();

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				doc = cursor.next();
			}
		} finally {
			cursor.close();
		}

		return doc;
	}

	@Override
	public void incOrder() {
		if (this.existOrder()) {
			LOGGER.trace("Increment order counter.");
			final Document updateDocInc = new Document("$inc", new Document(EXECUTED, 1).append(REMAINED, -1)).
														append("$currentDate", new BasicDBObject(LAST_MODIFIED,true));
			this.getCollection().updateOne(new Document(this.getCurrentPk(), PK_VALUE), updateDocInc);
		} else {
			this.reset();
		}
	}

	@Override
	public void reset() {
		LOGGER.info("Reset order counter");
		this.getCollection().deleteMany(new Document()); // Delete all documents
		final Document doc = new Document(this.getCurrentPk(), PK_VALUE).
									append(EXECUTED, new BsonInt32(0)).
									append(REMAINED, new BsonInt32(MAX_ORDERS_BY_DAY)).
									append(MAXIMUM, new BsonInt32(MAX_ORDERS_BY_DAY)).
									append(LAST_MODIFIED,TimeUtil.getNow());

		this.getCollection().insertOne(doc);
	}

	private boolean existOrder() {
		return this.getCollection().count()>0;
	}

}
