package market.api.connection.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.PaymiumUtil;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.BuySellDao;
import market.api.entity.Order;
import market.api.entity.TradeOrder;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;

public class BuySellDaoImpl implements BuySellDao {

	private final static Logger LOGGER = LoggerFactory.getLogger(BuySellDaoImpl.class.getName());

	private static final String NAME_BUYSELL_TRADE_NAME = "buySell";

	private final String currentPk = Constants.ORDER_PK;

	private final MongoCollection<Document> collection;

	/**
	 * Index PRICE
	 */
	public BuySellDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_BUYSELL_TRADE_NAME);

		// Create indexes in the state
		Util.createIndex(TradeOrder.STATE, -1, TradeOrder.DIRECTION, -1, this.collection);
	}

	@Override
	public String getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}

	@Override
	public String getCurrentPk() {
		return this.currentPk;
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public void insertOneTradeAddingFeeInfo(final String jsonObjectString)
																	throws ServiceException {
		LOGGER.trace("Insert one trade json [{}]", jsonObjectString);
		final Document document = Document.parse(jsonObjectString);
		document.put(Order.BTC_FEE, PaymiumUtil.getBtcFeeOrderPaymium(document));
		document.put(Order.CURRENCY_FEE, PaymiumUtil.getCurrencyFeeOrderPaymium(document));
		UtilDb.insertOne(document.toJson(), this.getCurrentPk(), this.getCollection());
	}

	@Override
	public void insertOrUpdate(final String jsonObjectString) {
		LOGGER.trace("Insert or update one json [{}] in collection name [{}]", jsonObjectString, this.getCollection().getNamespace());
		UtilDb.saveOrUpdateOneBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public long numberOrders(final Direction direction) {
		LOGGER.trace("Counting collection [{}] with trade orders direction [{}]", this.getCollection().getNamespace(), direction);
		final long startTime = TimeUtil.startCountTime();
		final long retLong = this.getCollection().count(Filters.eq(TradeOrder.DIRECTION, direction.getName()));
		TimeUtil.finishCountTime(startTime, String.format("Number of orders [%d] with direction [%s] in database [%s]", retLong, direction, this.getCollection().getNamespace()));
		return retLong;
	}

	@Override
	public long numberOrders(final Direction direction, final State state) {
		LOGGER.trace("Counting trade with direction [{}] and state [{}] in collection [{}]", this.getCollection().getNamespace(), direction, state);
		final long startTime = TimeUtil.startCountTime();
		final long retLong = this.getCollection().count(Filters.and(
															Filters.eq(TradeOrder.DIRECTION, direction.getName()),
															Filters.eq(TradeOrder.STATE, state.getName())));
		TimeUtil.finishCountTime(startTime, String.format("Number of orders [%d] with direction [%s] and state [%s] in database [%s]", retLong, direction, state, this.getCollection().getNamespace()));
		return retLong;
	}

	@Override
	public double getCostMarkedTrades(final Direction direction)
															throws ServiceException {
		double retValue = 0.0;

		final MongoCursor<Document> cursor = this.getCollection().find(Filters.and(
																					Filters.eq(TradeOrder.DIRECTION, direction.getName()),
																					Filters.eq(TradeOrder.MARK, Boolean.TRUE.booleanValue()))).iterator();
		double sense = 1;
		if (direction == Direction.SELL) {
			sense = -1;
		}

		try {
		    while (cursor.hasNext()) {
		    	final Document doc = cursor.next();
		    	final Double amountCurrency = Util.getDouble(doc.toJson(), TradeOrder.AMOUNT);
				final Double price = Util.getDouble(doc.toJson(), TradeOrder.PRICE);

				final double fee = Util.getBtcFee(amountCurrency, ApplicationConf.getPlatformCostFee());
				retValue = retValue + amountCurrency * price + fee * sense;
		    }
		} finally {
		    cursor.close();
		}

		return retValue;
	}

	@Override
	public List<Document> getSortDocumentList(final State state,
											  final Direction direction,
											  final Document sortFilter) {

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Get document list with a given state [{}], direction [{}] and sort filter [{}] in collection [{}]",
																									new Object[]{state.getName(),
																												 direction.getName(),
																												 sortFilter,
																												 this.getNameCollection()});
		final List<Document> retList = new ArrayList<Document>();

		// Filter by direction, state and mark
		final MongoCursor<Document> cursor = this.getCollection().find(Filters.and(
																				Filters.eq(TradeOrder.STATE, state.getName()),
																				Filters.eq(TradeOrder.DIRECTION, direction.getName()))).
																				sort(sortFilter).iterator();
		try {
		    while (cursor.hasNext()) {
		    	final Document doc = cursor.next();
		    	retList.add(doc);
		    }
		} finally {
		    cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("getSortDocumentListNonMarked for selling [%s]", this.getNameCollection()));

		return retList;
	}

	@Override
	public List<Document> getDocumentListMarked(final Direction direction) {

		final List<Document> retList = new ArrayList<Document>();

		// Filter by direction, state and mark
		final MongoCursor<Document> cursor = this.getCollection().find(Filters.and(Filters.eq(TradeOrder.DIRECTION, direction.getName()),
																				   Filters.eq(TradeOrder.MARK, Boolean.TRUE.booleanValue()))).iterator();

		try {
		    while (cursor.hasNext()) {
		    	final Document doc = cursor.next();
		    	retList.add(doc);
		    }
		} finally {
		    cursor.close();
		}

		return retList;
	}

	@Override
	public boolean existActiveOrder(final Direction directionValue, final double priceValue, final State stateValue) {
		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Checks if exists trade with direction [{}], price to buy [{}], state [{}] in collection [{}]",
																	new Object[]{directionValue,
																				 priceValue,
																				 stateValue,
																				 this.getCollection().getNamespace()});
		boolean retValue = false;

		final Document docLookFor = new Document(TradeOrder.PRICE, priceValue);

		if (directionValue!=null){
			docLookFor.append(TradeOrder.DIRECTION, directionValue.getName());
		}
		if (stateValue!=null){
			docLookFor.append(TradeOrder.STATE, stateValue.getName());
		}

		final FindIterable<Document> iterable = this.getCollection().find(docLookFor).limit(1);

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				retValue = true;
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value with direction [%s], price to buy [%s], state [%s]",
																								new Object[]{directionValue, priceValue, stateValue}));
		return retValue;
	}

	@Override
	public boolean existActiveOrder(final Direction direction, final double priceValue) {
		return this.existActiveOrder(direction, priceValue, null);
	}

	@Override
	public void delete(final Document doc) {
		if (doc!=null){
			LOGGER.trace("Removing one doc with PK [{}] and value [{}] from collection [{}]", Constants.STANDART_PK_MONGODB, doc.getString(Constants.STANDART_PK_MONGODB), this.getNameCollection());
			this.getCollection().deleteOne(new Document(Constants.STANDART_PK_MONGODB, doc.get(Constants.STANDART_PK_MONGODB)));
		}
	}

	@Override
	public Document changeStateSellMarkAndSave(final Document doc, final String stateKey, final State stateValue) {
		if (doc==null) {
			LOGGER.error("The given document is null. Please review it");
			return null;
		}

		LOGGER.trace("Change doc old state [{}] to the new state [{}] in collection [{}]",
																				doc.getString(TradeOrder.STATE),
																				stateValue,
																				this.getNameCollection());

		// Change state
		doc.put(stateKey, stateValue.getName());
		// Save the new state in DB
		this.insertOrUpdate(doc.toJson());

		return doc;
	}

	@Override
	public Document getDocument(final Document doc) {
		final String idDoc = doc.getString(Constants.STANDART_PK_MONGODB);

		LOGGER.trace("Get doc with id [{}] from collection [{}]", idDoc, this.getNameCollection());

		final long startTime = TimeUtil.startCountTime();

		Document retDocument = null;

		// Filter by _id
		final MongoCursor<Document> cursor = this.getCollection().find(Filters.eq(TradeOrder.ID_BUY, idDoc)).iterator();
		try {
		    if (cursor.hasNext()) {
		    	retDocument = cursor.next();
		    }
		} finally {
		    cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("getSortDocumentListNonMarked for selling [%s]", this.getNameCollection()));

		return retDocument;
	}

	@Override
	public Document getDocument(final String key0, final double value0, final State markApplSellState) {
		LOGGER.trace("Get key0 [{}] value0[{}] from collection [{}]", key0, value0, this.getNameCollection());

		final long startTime = TimeUtil.startCountTime();

		Document retDocument = null;

		// Filter by _id
		final MongoCursor<Document> cursor = this.getCollection().find(Filters.and(Filters.eq(key0, value0),
																				   Filters.eq(TradeOrder.STATE, markApplSellState.getName()))).limit(1).iterator();
		try {
		    if (cursor.hasNext()) {
		    	retDocument = cursor.next();
		    }
		} finally {
		    cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("getDocument mark [%s] in collection [%s]", markApplSellState, this.getNameCollection()));

		return retDocument;
	}

	@Override
	public Document getDocument(final String stringKey, final String stringValue) {
		Document retDoc = null;

		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Get document list with the given key [{}] in value [{}] in collection [{}]", new Object[]{stringKey,
																									  		    stringValue,
																												this.getNameCollection()});

		final MongoCursor<Document> cursor = this.getCollection().find(new Document(stringKey, stringValue)).limit(1).
																													iterator();
		try {
			if (cursor.hasNext()) {
				retDoc = cursor.next();
			}
		} finally {
			cursor.close();
		}

		TimeUtil.finishCountTime(startTime, String.format("Time to check exist key value with key [%s] and value [%s]", stringKey, stringValue));

		return retDoc;
	}



}