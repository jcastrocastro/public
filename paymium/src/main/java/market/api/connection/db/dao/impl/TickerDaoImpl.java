package market.api.connection.db.dao.impl;

import java.time.LocalDateTime;

import market.api.common.Util;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.TickerDao;
import market.api.entity.Ticker;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;


public class TickerDaoImpl implements TickerDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(TickerDaoImpl.class.getName());

	private final String currentPk = "tradeId";

	private static final String NAME_TABLE_USER_INFO = "ticker";

	private final MongoCollection<Document> collection;

	public TickerDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_USER_INFO);

		// Creates index for alive trades collection
		Util.createIndex(Ticker.AT, -1, this.collection);
	}

	@Override
	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public String getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}

	@Override
	public void save(final String jsonObjectString) {
		LOGGER.trace("Insert or update one in collection [{}], json [{}]", this.getCollection().getNamespace(), jsonObjectString);
		UtilDb.saveOrUpdateOneBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public Document getLastTicker(final LocalDateTime localDateTime) {

//		XXX
		final FindIterable<Document> iterable = this.getCollection().find(new Document(this.getCurrentPk(), new Document("$exists", true))).limit(1);

		Document document = null;
		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				document = cursor.next();
			}
		} finally {
			cursor.close();
		}

		return document;
	}

}
