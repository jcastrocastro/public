package market.api.connection.db.dao;

import org.bson.Document;


public interface UserInfoDao {

	public static final String LAST_MODIFIED = "last_modified";

	void saveOrUpdate(String json);

	/**
	 * Gets user info or null if it doesn't exist in the collection
	 * <pre>
	 * Example:
	 *
	 * {_id=PM-U62729767,
	 * locale=en,
	 * channel_id=2102ff4d745a5e67180f8fbafe358ef1915bac0dde33a22e7387fa31edfb937e,
	 * balance_btc=0.0,
	 * locked_btc=1.00944889,
	 * balance_eur=792.21,
	 * locked_eur=522.60171336}}
	 * </pre>
	 *
	 * @return
	 */
	Document getUserInfo();

	String getNameCollection();


}
