package market.api.connection.db.dao.impl;

import market.api.common.Constants;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.ExecutedTradesMarketDao;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

public class ExecutedTradesMarketDaoImpl implements ExecutedTradesMarketDao{

	private static final String NAME_TABLE_EXECUTED_TRADES = "executedTradesMarket";

	private final String currentPk = Constants.ORDER_PK;

	private final MongoCollection<Document> collection;

	public ExecutedTradesMarketDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_EXECUTED_TRADES);
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public String getNameTableExecutedTrades() {
		return this.getCollection().getNamespace().toString();
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public void insertOrUpdateMany(final String jsonObjectString) {
		UtilDb.saveOrUpdateManyBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());

	}

}
