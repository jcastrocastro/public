package market.api.connection.db;


public interface BuySellHistDao {

	/**
	 * Gets name of the collection
	 * @return
	 */
	String getNameCollection();

	/**
	 * Gets current primary key
	 * @return
	 */
	String getCurrentPk();

	/**
	 * Inserts or updates one document in the collection
	 * @param jsonObjectString
	 */
	void insertOrUpdate(String jsonObjectString);

}
