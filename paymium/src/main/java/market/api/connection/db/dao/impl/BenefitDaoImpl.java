package market.api.connection.db.dao.impl;

import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.BenefitDao;
import market.api.entity.TradeOrder;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

public class BenefitDaoImpl implements BenefitDao {

	private static final String NAME_TABLE_BENEFIT = "benefit";

	private static final String ESTIMATED_BENEFIT = "estimatedBenefit";

	private final String currentPk = "timestamp";

	private final MongoCollection<Document> collection;

	public BenefitDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_BENEFIT);
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public Object getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}


	@Override
	public void save(final double totalBenefit) {
		final String jsonObjectString = new Document(this.getCurrentPk(), TimeUtil.getTimeNow()).
													append(TradeOrder.UPDATED_AT, TimeUtil.getNow()).
													append(ESTIMATED_BENEFIT, totalBenefit).toJson();

		UtilDb.insertOne(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

}
