package market.api.connection.db.dao.impl;

import java.time.LocalDateTime;
import java.util.List;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.BidDao;
import market.api.entity.PriceDepth;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public class BidMarketDaoImpl implements BidDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(AskMarketDaoImpl.class.getName());

	private static final String NAME_TABLE_ASK = "bid";

	private static final int NUM_HOURS_RANGE_BID = 15;

	private final String currentPk = Constants.TIMESTAMP_PK;

	private final MongoCollection<Document> collection = ConnectionDbApi.getCollection(NAME_TABLE_ASK);

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public void saveOrUpdateMany(final String jsonObjectString) {
		UtilDb.saveOrUpdateManyBsonInt32(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public void saveOrUpdateOne(final Document document) {
		UtilDb.saveOrUpdateOneBsonInt32(this.getCollection(), document);
	}

	@Override
	public void insertMany(final String jsonObjectString) {
		UtilDb.insertMany(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public void deleteMany() {
		LOGGER.debug("DeleteMany all collection [{}]", this.getCollection().getNamespace());
		this.getCollection().deleteMany(new Document());
	}

	@Override
	public Document getLowestBid(int numberTh) {
		final LocalDateTime nowLocalDateTime = TimeUtil.getNowLocalDateTime();

		final long hoursLessTimeStamp = TimeUtil.getTime(TimeUtil.addHours(nowLocalDateTime, -NUM_HOURS_RANGE_BID));

		LOGGER.trace("Timestamp hours less [{}], hour less [{}], now local datetime [{}]", new Object[]{hoursLessTimeStamp, -NUM_HOURS_RANGE_BID, nowLocalDateTime});

		final List<Document> documentList = UtilDb.getNthDocumentsSorted(this.getCollection(),
																				Filters.gte(Constants.STANDART_PK_MONGODB, hoursLessTimeStamp),
																				new Document(PriceDepth.PRICE, -1),
																				numberTh);
//		this.printTraceDocuments(documentList);

		if (documentList.size()<numberTh) {
			numberTh = documentList.size();
			LOGGER.trace("Maximum size lowest bid. Reset numberTh to [{}]", numberTh);
		}

		return documentList.get(numberTh-1);
	}

	void printTraceDocuments(final List<Document> documentList) {
		for (final Document document : documentList) {
			LOGGER.trace("Document: {}", document.toJson());
		}
	}

}
