package market.api.connection.db.dao.impl;

import java.util.List;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.AskDao;
import market.api.entity.PriceDepth;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public class AskMarketDaoImpl implements AskDao {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskMarketDaoImpl.class.getName());

	private static final String NAME_TABLE_ASK = "ask";

	private static final int NUM_HOURS_RANGE_ASK = 15;

	private final String currentPk = Constants.TIMESTAMP_PK;

	private final MongoCollection<Document> collection;

	public AskMarketDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_ASK);
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public void saveOrUpdateOne(final Document document) {
		UtilDb.saveOrUpdateOneBsonInt32(this.getCollection(), document);
	}

	@Override
	public void saveOrUpdateMany(final String jsonObjectString) {
		UtilDb.saveOrUpdateManyBsonInt32(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public Document getHighestAsk(int numberTh) {
		final long hoursLess = TimeUtil.getTime(TimeUtil.addHours(TimeUtil.getNowLocalDateTime(), - NUM_HOURS_RANGE_ASK));
		final List<Document> documentList = UtilDb.getNthDocumentsSorted(this.getCollection(),
																				Filters.gte(Constants.STANDART_PK_MONGODB, hoursLess),
																				new Document(PriceDepth.PRICE, -1),
																				numberTh);
		LOGGER.trace("Size of documents [{}] and range hours [{}]", documentList.size(), NUM_HOURS_RANGE_ASK);

		if (documentList.size()<numberTh) {
			numberTh = documentList.size();
			LOGGER.trace("Maximum size highest ask [{}]. Reset numberTh to [{}]", numberTh);
		}

		return documentList.get(numberTh-1);
	}

	@Override
	public void insertMany(final String jsonObjectString) {
		UtilDb.insertMany(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public void deleteMany() {
		LOGGER.debug("DeleteMany all collection [{}]", this.getCollection().getNamespace());
		this.getCollection().deleteMany(new Document());
	}
}
