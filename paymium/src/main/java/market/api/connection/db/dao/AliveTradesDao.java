package market.api.connection.db.dao;

import java.util.List;

import market.api.enumtype.Direction;
import market.api.enumtype.State;

import org.bson.Document;



public interface AliveTradesDao {

	/**
	 * Inserts or updates many by primary key
	 * @param jsonObjectString
	 */
	void insertOrUpdateMany(final String jsonObjectString);

	String getNameTableAliveTrades();

	/**
	 * Exists active order with the given keys values
	 *
	 * @param directionLabel
	 * @param directionValue
	 * @param priceLabel
	 * @param priceValue
	 * @return
	 */
	boolean existActiveOrder(String directionLabel, String directionValue,
							 String priceLabel, double priceValue);

	/**
	 * Exists active order with the given keys values
	 *
	 * @param directionLabel
	 * @param directionValue
	 * @param priceLabel
	 * @param priceValue
	 * @param stateLabel
	 * @param stateValue
	 * @return
	 */
	boolean existActiveOrder(String directionLabel, Direction directionValue,
							 String priceLabel, double priceValue,
							 String stateLabel, State stateValue);

	/**
	 * Removes all documents from a collection, not index
	 */
	void deleteMany();

	void insertMany(String jsonObjectString);

	/**
	 * count
	 * @return
	 */
	long count();

	long count(Direction buy);

	/**
	 *
	 * @param direction
	 * @param price
	 * @param state
	 * @return
	 */
	long count(Direction direction, double price, State state);

	/**
	 * Gets alive trades with the given conditions
	 * @param directionLabel
	 * @param directionValue
	 * @return
	 */
	List<Document> getAliveTrades(String directionLabel, Direction directionValue);

	/**
	 * Gets alive trades with with the given direction and price value is greater than the given one
	 * @param directionLabel
	 * @param directionValue
	 * @param priceLabel
	 * @param priceValue
	 * @return
	 */
	List<Document> getAliveTrades(String directionLabel, Direction directionValue,
								  String priceLabel, int priceValue);

	/**
	 * Gets alive trades with with the given direction and price value is greater than the given one
	 * <p>
	 * Sort by the give sortDocument
	 *
	 * @param directionLabel
	 * @param directionValue
	 * @param priceLabel
	 * @param priceValue
	 * @param stateLabel
	 * @param stateValue
	 * @param sortDocument
	 * @return
	 */
	List<Document> getAliveTradesSort(String directionLabel, Direction directionValue,
									  String priceLabel, int priceValue,
									  String stateLabel, State stateValue,
									  Document sortDocument);





}
