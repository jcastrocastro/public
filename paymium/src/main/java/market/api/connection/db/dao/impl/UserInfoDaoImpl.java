package market.api.connection.db.dao.impl;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.UserInfoDao;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class UserInfoDaoImpl implements UserInfoDao {

	private final static Logger LOGGER = LoggerFactory.getLogger(UserInfoDaoImpl.class.getName());

	private static final String NAME_TABLE_USER_INFO = "userInfo";

	private final MongoCollection<Document> collection;

	private final String currentPk = "name";

	public UserInfoDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_USER_INFO);
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public String getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}

	@Override
	public void saveOrUpdate(final String jsonObjectString) {
		// Add last modified filed
		final String json = Document.parse(jsonObjectString).append(LAST_MODIFIED, TimeUtil.getNow()).toJson();
		// Upsert info
		LOGGER.trace("Insert or update one in collection [{}], json [{}]", this.getCollection().getNamespace(), json);
		UtilDb.saveOrUpdateOneBsonString(json, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public Document getUserInfo() {
		Document retDocument = null;

		LOGGER.trace("Get user info from DB");

		// In the table there is only one row
		final FindIterable<Document> iterable = this.getCollection().find(new Document(Constants.STANDART_PK_MONGODB, new Document("$exists", true))).limit(1);

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				retDocument = cursor.next();
			}
		} finally {
			cursor.close();
		}

		return retDocument;
	}

}
