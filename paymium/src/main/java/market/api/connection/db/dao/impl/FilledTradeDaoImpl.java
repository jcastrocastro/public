package market.api.connection.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import market.api.common.Constants;
import market.api.common.Util;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.FilledTradeDao;
import market.api.entity.TradeOrder;
import market.api.enumtype.Direction;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

/**
 * Filled trade from the platform
 *
 * <pre>
 * Order
 *  [uuid=7f794b2a-226b-4fcd-9f21-b2bfcd610d00, amount=0.3, state=FILLED, btcFee=0.0, currencyFee=0.37347, updateAt=2015-09-28T07:57:17.000Z, createdAt=2015-09-27T13:39:35.000Z, currency=EUR, type=LimitOrder,
 *  accountOperations=[AccountOperation [uuid=f6791ee3-61dd-4857-aacf-2655d399717a, name=lock, amount=-0.3, currency=BTC, createdAt=2015-09-27T13:39:35.000Z, createdAtInt=1443361175, isTradingAccount=false],
 *  AccountOperation [uuid=5c447af3-192d-4a08-a829-952c6f0a3b66, name=lock, amount=0.3, currency=BTC, createdAt=2015-09-27T13:39:35.000Z, createdAtInt=1443361175, isTradingAccount=true],
 *  AccountOperation [uuid=6a114d8d-21a8-42ee-93a8-2fa9eacfee9c, name=btc_sale, amount=63.3, currency=EUR, createdAt=2015-09-28T07:57:17.000Z, createdAtInt=1443427037, isTradingAccount=true],
 *  AccountOperation [uuid=ffcdd4b0-9390-463b-abdf-6a8c40fa4a6f, name=btc_sale, amount=-0.3, currency=BTC, createdAt=2015-09-28T07:57:17.000Z, createdAtInt=1443427037, isTradingAccount=true],
 *  AccountOperation [uuid=0981c811-df23-46c7-a440-ee863441e181, name=btc_sale_fee, amount=-0.37347, currency=EUR, createdAt=2015-09-28T07:57:17.000Z, createdAtInt=1443427037, isTradingAccount=true],
 *  AccountOperation [uuid=fc8b685d-7234-4b6a-9129-bdfe77e85abe, name=unlock, amount=62.92, currency=EUR, createdAt=2015-09-28T07:57:17.000Z, createdAtInt=1443427037, isTradingAccount=false],
 *  AccountOperation [uuid=c7340118-ef94-4ada-8194-a5cfe7a230d4, name=unlock, amount=-62.92, currency=EUR, createdAt=2015-09-28T07:57:17.000Z, createdAtInt=1443427037, isTradingAccount=true]]]
 * </pre>
 * @author rcc
 *
 */
public class FilledTradeDaoImpl implements FilledTradeDao{

	private final static Logger LOGGER = LoggerFactory.getLogger(FilledTradeDaoImpl.class.getName());

	private static final String NAME_FILLED_TRADE = "filledTrade";

	private final String currentPk = Constants.ORDER_PK;

	private final MongoCollection<Document> collection;

	public FilledTradeDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_FILLED_TRADE);

		Util.createIndex(TradeOrder.MARK, -1, TradeOrder.DIRECTION, 1, TradeOrder.PRICE, -1, this.collection);
	}

	public static String getNameTableBuyOrder() {
		return NAME_FILLED_TRADE;
	}

	public String getCurrentPk() {
		return this.currentPk;
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public void insertOrUpdateMany(final String jsonObjectString) {
		UtilDb.saveOrUpdateManyBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public void insertOrUpdate(final String jsonObjectString) {
		LOGGER.trace("Insert or update one in collection [{}], json [{}]", this.getCollection().getNamespace(), jsonObjectString);
		UtilDb.saveOrUpdateOneBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}

	@Override
	public List<Document> getNonMarkedSortedBy(final Direction direction, final int ascDesc) {
		final List<Document> retDocList = new ArrayList<>();

		// Non exist mark
		final Document filter = new Document(TradeOrder.DIRECTION, direction.getName()).append(TradeOrder.MARK, new Document("$exists", Boolean.FALSE.booleanValue()));

		final Bson sort = new Document(TradeOrder.PRICE, ascDesc);
		final MongoCursor<Document> cursor = this.getCollection().find(filter).sort(sort).iterator();
		try {
		    while (cursor.hasNext()) {
		        retDocList.add(cursor.next());
		    }
		} finally {
		    cursor.close();
		}
		return retDocList;
	}

	@Override
	public String getCollectionName() {
		return this.getCollection().getNamespace().toString();
	}


}
