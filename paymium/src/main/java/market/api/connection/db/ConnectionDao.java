package market.api.connection.db;

import market.api.connection.db.dao.AliveTradesDao;
import market.api.connection.db.dao.AliveTradesHistDao;
import market.api.connection.db.dao.AskDao;
import market.api.connection.db.dao.BenefitDao;
import market.api.connection.db.dao.BidDao;
import market.api.connection.db.dao.BuySellDao;
import market.api.connection.db.dao.ExecutedTradesMarketDao;
import market.api.connection.db.dao.FilledTradeDao;
import market.api.connection.db.dao.OrderCounterDao;
import market.api.connection.db.dao.TickerDao;
import market.api.connection.db.dao.TokenDao;
import market.api.connection.db.dao.UserInfoDao;
import market.api.connection.db.dao.impl.AliveTradesDaoImpl;
import market.api.connection.db.dao.impl.AliveTradesHistDaoImpl;
import market.api.connection.db.dao.impl.AskMarketDaoImpl;
import market.api.connection.db.dao.impl.BenefitDaoImpl;
import market.api.connection.db.dao.impl.BidMarketDaoImpl;
import market.api.connection.db.dao.impl.BuySellDaoImpl;
import market.api.connection.db.dao.impl.BuySellHistDaoImpl;
import market.api.connection.db.dao.impl.ExecutedTradesMarketDaoImpl;
import market.api.connection.db.dao.impl.FilledTradeDaoImpl;
import market.api.connection.db.dao.impl.OrderCounterImpl;
import market.api.connection.db.dao.impl.TickerDaoImpl;
import market.api.connection.db.dao.impl.TokenDaoImpl;
import market.api.connection.db.dao.impl.UserInfoDaoImpl;

public class ConnectionDao {

	private static TokenDao tokenDao;
	private static AskDao askMarketDao;
	private static BidDao bidMarketDao;
	private static AliveTradesDao aliveTradesDao;
	private static ExecutedTradesMarketDao executedTradesMarketDao;
	private static UserInfoDao userInfoDao;
	private static FilledTradeDao filledTradeDao;
	private static BuySellDao buySellDao;
	private static AliveTradesHistDaoImpl aliveTradesHistDao;
	private static OrderCounterDao orderCounterDao;
	private static BenefitDao benefitDao;
	private static BuySellHistDao buySellHistDao;
	private static TickerDao tickerDao;

	public static BenefitDao getNewBenefitDao() {
		if (benefitDao == null) {
			benefitDao = new BenefitDaoImpl();
		}
		return benefitDao;
	}

	public static TokenDao getNewTokenDao () {
		if (tokenDao == null) {
			tokenDao = new TokenDaoImpl();
		}

		return tokenDao;
	}


	public static AskDao getNewAskMarketDao(){
		if (askMarketDao == null) {
			askMarketDao = new AskMarketDaoImpl();
		}

		return askMarketDao;
	}


	public static BidDao getNewBidMarketDao() {
		if (bidMarketDao == null) {
			bidMarketDao = new BidMarketDaoImpl();
		}

		return bidMarketDao;
	}


	public static AliveTradesDao getNewAliveTradesDao() {
		if (aliveTradesDao == null){
			aliveTradesDao = new AliveTradesDaoImpl();
		}
		return aliveTradesDao;
	}

	public static AliveTradesHistDao getNewAliveTradesHistDao() {
		if (aliveTradesHistDao == null){
			aliveTradesHistDao = new AliveTradesHistDaoImpl();
		}
		return aliveTradesHistDao;
	}


	public static ExecutedTradesMarketDao getNewExecutedTradesMarketDao() {
		if (executedTradesMarketDao == null) {
			executedTradesMarketDao = new ExecutedTradesMarketDaoImpl();
		}

		return executedTradesMarketDao;
	}


	public static UserInfoDao getNewUserInfo() {
		if (userInfoDao == null) {
			userInfoDao = new UserInfoDaoImpl();
		}

		return userInfoDao;
	}

	public static FilledTradeDao getNewFilledTradeDao() {
		if (filledTradeDao == null) {
			filledTradeDao = new FilledTradeDaoImpl();
		}
		return filledTradeDao;
	}

	public static BuySellDao getNewBuySellDao() {
		if (buySellDao == null) {
			buySellDao = new BuySellDaoImpl();
		}
		return buySellDao;
	}


	public static OrderCounterDao getNewOrderCounterDao() {
		if (orderCounterDao == null) {
			orderCounterDao = new OrderCounterImpl();
		}
		return orderCounterDao;
	}

	public static BuySellHistDao getNewBuySellHistDao() {
		if (buySellHistDao == null) {
			buySellHistDao = new BuySellHistDaoImpl();
		}
		return buySellHistDao;
	}

	public static TickerDao getNewTickerDao() {
		if (tickerDao == null) {
			tickerDao = new TickerDaoImpl();
		}
		return tickerDao;
	}


}
