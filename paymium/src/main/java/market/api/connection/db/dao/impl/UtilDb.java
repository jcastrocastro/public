package market.api.connection.db.dao.impl;

import java.util.ArrayList;
import java.util.List;

import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;

import org.bson.BsonDocument;
import org.bson.BsonInt32;
import org.bson.BsonString;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.UpdateOptions;

public class UtilDb {

	private final static Logger LOGGER = LoggerFactory.getLogger(UtilDb.class.getName());

	private final static UpdateOptions updateOptions = new UpdateOptions();

	static {
		updateOptions.upsert(true);
	}

	/**
	 * Gets update options
	 * @return
	 */
	static UpdateOptions getUpdateoptions() {
		return updateOptions;
	}

	/**
	 * Insert or update a single document in the collection
	 * @param collection Collection where to save
	 * @param key key
	 * @param jsonString Json string to insert
	 */
	static void insertOrUpdateOneBsonInt32(final MongoCollection<Document> collection, final BsonInt32 key, final Document document) {
		collection.updateOne(new BsonDocument(Constants.STANDART_PK_MONGODB, key), new Document("$set", document), getUpdateoptions());
	}

	/**
	 * Inserts or updates a single document in the collection according to the specified arguments.
	 * <p>
	 * Saves or update the document comparing with {@link Constants#STANDART_PK_MONGODB }
	 * @param collection
	 * @param document
	 */
	static void saveOrUpdateOneBsonInt32(final MongoCollection<Document> collection, final Document document) {

		UtilDb.insertOrUpdateOneBsonInt32(collection,
								 		  new BsonInt32((int)document.get(Constants.STANDART_PK_MONGODB)),
								 		  document);
	}



	/**
	 * Gets all documents from the collection
	 *
	 * @param collection
	 * @return
	 */
	static List<Document> getAllDocuments (final MongoCollection<Document> collection) {
		final List<Document> retDocs = new ArrayList<>();

		final MongoCursor<Document> cursor = collection.find().iterator();
		try {
		    while (cursor.hasNext()) {
		        retDocs.add(cursor.next());
		    }
		} finally {
		    cursor.close();
		}

		return retDocs;
	}

	/**
	 * Gets the biggest nth document sorted by FK
	 * <p>
	 * <code>
	 * collection.find(findFilter).limit(nth).sort(sortFilter).iterator();
	 * </code>
	 * @param collection
	 * @param findFilter
	 * @param sortFilter
	 * @param nth
	 * @return
	 */
	static List<Document> getNthDocumentsSorted (final MongoCollection<Document> collection, final Bson findFilter, final Document sortFilter, final int nth) {
		final long startTime = TimeUtil.startCountTime();

		final List<Document> retDocs = new ArrayList<>();

		final MongoCursor<Document> cursor = collection.find(findFilter).limit(nth).sort(sortFilter).iterator();
		try {
		    while (cursor.hasNext()) {
		        retDocs.add(cursor.next());
		    }
		} finally {
		    cursor.close();
		}

		if (ApplicationConf.getApplicationConf().isSimulate()){
			for (final Document document : retDocs) {
				LOGGER.trace("Sorted doc [{}]", document.toJson());
			}
		}

		TimeUtil.finishCountTime(startTime, String.format("get [%s] Nth documents sorted time", nth));

		return retDocs;
	}

	/**
	 * Inserts one or more documents. A call to this method is equivalent to a call to the bulkWrite method
	 *
	 * @param jsonObjectString
	 * @param currentPk
	 * @param collection
	 */
	static void insertMany(final String jsonObjectString, final String currentPk, final MongoCollection<Document> collection) {
		final long startTime = TimeUtil.startCountTime();

		final List<Document> docsList = Util.getListDocs(Util.adaptPrimaryKeyToMongoDb(jsonObjectString, currentPk));

		if (Util.isEmpty(docsList)){
			LOGGER.info("Empty json object string [{}]", docsList);
			return;
		}
		LOGGER.trace("Trying insert [{}] nth documents in collection [{}]",
													new Object[]{docsList.size(), collection.getNamespace()});

//		LOGGER.trace("Trying insert size [{}] documents in collection [{}] with json [{}]",
//								new Object[]{docsList.size(), collection.getNamespace(), jsonObjectString});

		collection.insertMany(docsList);

		TimeUtil.finishCountTime(startTime, String.format("Insert many in collection [%s]", collection.getNamespace()));
	}

	/**
	 * Inserts the provided json string. If the document is missing an identifier, should generate one.
	 *
	 * @param jsonObjectString Json object string to be inserted
	 * @param currentPk Current Primary key
	 * @param collection Collection
	 */
	static void insertOne(final String jsonObjectString, final String currentPk, final MongoCollection<Document> collection) {
		LOGGER.trace("Trying insert one document in collection [{}] with json [{}]", collection.getNamespace(), jsonObjectString);

		final Document document = Document.parse(Util.adaptPrimaryKeyToMongoDb(jsonObjectString, currentPk));

		collection.insertOne(document);
	}


	private static void saveOrUpdateManyBsonInt32(final MongoCollection<Document> collection, final List<Document> docsList) {
		for (final Document document : docsList) {
			UtilDb.saveOrUpdateOneBsonInt32(collection, document);
		}
	}

	/**
	 * Inserts or updates many documents in the collection
	 *
	 * @param jsonObjectString Json object
	 * @param currentPk Specifies the Primary key of the collection
	 * @param collection Collection where saves or update
	 */
	static void saveOrUpdateManyBsonInt32(final String jsonObjectString, final String currentPk, final MongoCollection<Document> collection) {
		final long startTime = TimeUtil.startCountTime();

		final List<Document> docsList = Util.getListDocs(Util.adaptPrimaryKeyToMongoDb(jsonObjectString, currentPk));

		LOGGER.trace("Trying upsert [{}] documents in collection [{}], json {}", new Object[]{docsList.size(), collection.getNamespace(), jsonObjectString});

		// Save all documents
		UtilDb.saveOrUpdateManyBsonInt32(collection, docsList);

	 	TimeUtil.finishCountTime(startTime, "Upsert many bson int32 foreign key");
	}

	/**
	 * Saves or updates many documents in the collection according to the specified arguments.
	 *
	 * @param jsonObjectString
	 * @param currentPk
	 * @param collection
	 */
	static void saveOrUpdateManyBsonString(final String jsonObjectString, final String currentPk, final MongoCollection<Document> collection) {
		final long startTime = TimeUtil.startCountTime();

		final List<Document> docsList = Util.getListDocs(Util.adaptPrimaryKeyToMongoDb(jsonObjectString, currentPk));

		LOGGER.trace("Trying upsert [{}] nth documents in collection [{}]", new Object[] {docsList.size(), collection.getNamespace()});
//		LOGGER.trace("Trying upsert [{}] documents in collection [{}], json {}", new Object[]{docsList.size(), collection.getNamespace(), jsonObjectString});

		if (Util.isEmpty(docsList)){
			LOGGER.info("Empty docs [{}]", docsList);
			return;
		}

		// Save all documents
		UtilDb.saveOrUpdateManyString(collection, docsList);

	 	TimeUtil.finishCountTime(startTime, String.format("Upsert many bson String in collection [%s]", collection.getNamespace()));
	}

	private static void saveOrUpdateManyString(final MongoCollection<Document> collection, final List<Document> docsList) {
		for (final Document document : docsList) {
			UtilDb.saveOrUpdateOneString(collection, document);
		}
	}

	private static void saveOrUpdateOneString(final MongoCollection<Document> collection, final Document document) {
		UtilDb.saveOrUpdateOneString(collection,
				 					 (String)document.get(Constants.STANDART_PK_MONGODB),
				 					 document);
	}

	private static void saveOrUpdateOneString(final MongoCollection<Document> collection,
												final String foreignKey,
												final Document document) {

		collection.updateOne(new BsonDocument(Constants.STANDART_PK_MONGODB, new BsonString(foreignKey)),
							 new Document("$set", document),
							 getUpdateoptions());
	}

	/**
	 * Updates a single document in the collection according to the specified arguments.
	 *
	 * @param jsonObjectString
	 * @param currentPk
	 * @param collection
	 */
	static void saveOrUpdateOneBsonString(final String jsonObjectString,
										  final String currentPk,
										  final MongoCollection<Document> collection) {

		final Document doc = Document.parse(Util.adaptPrimaryKeyToMongoDb(jsonObjectString, currentPk));
		UtilDb.saveOrUpdateOneString(collection, doc);
	}


}
