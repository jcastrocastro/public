package market.api.connection.db.dao;

public interface AliveTradesHistDao {

	String getNameTableAliveTradesHist();

	void insertManyWithVersion(String jsonArrayObject);

}
