package market.api.connection.db.dao;

import org.bson.Document;

public interface AskDao {

	void saveOrUpdateOne(final Document document);

	void saveOrUpdateMany(String json);

	/**
	 * Gets the last th ask saved in DB
	 * <pre>
	 * Test query:
	 * db.ask.find({"_id":{$gte:1443720052}}).sort({"price":-1})
	 * </pre>
	 * @param numberTh
	 * @return
	 */
	Document getHighestAsk(final int numberTh);

	void deleteMany();

	void insertMany(String jsonObjectString);

}
