package market.api.connection.db.dao;

public interface ExecutedTradesMarketDao {

	void insertOrUpdateMany(String jsonObjectString);

	String getNameTableExecutedTrades();

}
