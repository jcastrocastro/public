package market.api.connection.db.dao.impl;

import java.util.Arrays;
import java.util.List;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.AliveTradesHistDao;
import market.api.entity.Order;

import org.bson.BsonInt32;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class AliveTradesHistDaoImpl implements AliveTradesHistDao {

	private final static Logger LOGGER = LoggerFactory.getLogger(AliveTradesHistDaoImpl.class.getName());

	/**
	 * Primary key of the collection
	 */
	private final String currentPk = Constants.ORDER_PK;

	private static final String NAME_TABLE_ALIVE_TRADES_HIST = "aliveTradesHist";

	private static final String VERSION = "version";

	private static final String UPDATE = "update";

	private static final String VERSION_DATE = "versionDate";

	private static final String LAST_VERSION = "lastVersion";

	private final MongoCollection<Document> collection;

	public AliveTradesHistDaoImpl(){
		this.collection = ConnectionDbApi.getCollection(NAME_TABLE_ALIVE_TRADES_HIST);
	}

	@Override
	public String getNameTableAliveTradesHist() {
		return this.getCollection().getNamespace().toString();
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	/**
	 * Get current primary key
	 * @return
	 */
	public String getCurrentPk() {
		return this.currentPk;
	}

	@Override
	public void insertManyWithVersion(final String jsonArrayObject) {

		final long startTime = TimeUtil.startCountTime();

		final List<Document> documentList = Util.getListDocs(Util.adaptPrimaryKeyToMongoDb(jsonArrayObject, this.getCurrentPk()));
		for (final Document document : documentList) {

			final Document documentInDb = this.existByPk(document);
			// test ------------------------------------------
//			document.put(Order.STATE, State.FILLED.getName());
			// ------------------------------------------
			if (documentInDb==null) { // Not exist in DB
				LOGGER.trace("Trying insert one document in collection [{}] with json [{}]", this.collection.getNamespace(), document.toJson());
				final List<Document> listItem = Arrays.asList(new Document(VERSION, new BsonInt32(0)).append(VERSION_DATE, TimeUtil.getNow()).append(Order.STATE, document.getString(Order.STATE)));
				this.getCollection().insertOne(document.append(LAST_VERSION, new BsonInt32(0)).append(UPDATE, listItem));

			} else if (!documentInDb.getString(Order.STATE).equals(document.getString(Order.STATE))){
				LOGGER.trace("Trying update one document in collection [{}] with state [{}] and json [{}]",
																		new Object[]{this.collection.getNamespace(), document.getString(Order.STATE), document.toJson()});
				int currentVersion = 0;

				final Integer currentVersionInteger = documentInDb.getInteger(LAST_VERSION);
				if (currentVersionInteger!=null) {
					currentVersion = currentVersionInteger.intValue();
					currentVersion++;
				}
				final String docStateValue = document.getString(Order.STATE);
				final Document listItem = new Document(UPDATE, new Document(VERSION, new BsonInt32(currentVersion)).append(VERSION_DATE, TimeUtil.getNow()).append(Order.STATE, docStateValue));

				this.getCollection().updateOne(new Document(Constants.STANDART_PK_MONGODB, documentInDb.get(Constants.STANDART_PK_MONGODB)),
											   new Document("$inc", new Document(LAST_VERSION, 1)).append("$set", new Document(Order.STATE, docStateValue)).  // update last version and state in the document
											   append("$push", listItem));  						 // insert element in the array
			}
		}

		TimeUtil.finishCountTime(startTime, String.format("Insert many alive trades historic.", documentList.size()));
	}

	/**
	 * Exists the document looking for its primary key
	 * @param document
	 * @return The document in the DB or null if it doesn't exist
	 */
	private Document existByPk(final Document document) {
		Document retDoc = null;

		// Look for the document using the PK
		final MongoCursor<Document> cursor = this.getCollection().find(new Document(Constants.STANDART_PK_MONGODB, document.getString(Constants.STANDART_PK_MONGODB))).limit(1).iterator();
		try {
			if (cursor.hasNext()) {
				retDoc = cursor.next();
			}
		} finally {
			cursor.close();
		}

		return retDoc;
	}


}
