package market.api.connection.db;


public interface IConnection {

	Object getConnectionClient();

	Object getDatabase();

	void close();

}
