package market.api.connection.db.dao;

public interface BenefitDao {

	void save(double total);

	Object getNameCollection();

}
