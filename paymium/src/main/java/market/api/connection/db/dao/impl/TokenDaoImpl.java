package market.api.connection.db.dao.impl;

import market.api.common.Constants;
import market.api.connection.db.ConnectionDbApi;
import market.api.connection.db.dao.TokenDao;
import market.api.entity.Token;
import market.api.enumtype.StaticCollection;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

public class TokenDaoImpl implements TokenDao {

	private final static Logger LOGGER = LoggerFactory.getLogger(TokenDaoImpl.class.getName());

	private final MongoCollection<Document> collection = ConnectionDbApi.getCollection(StaticCollection.TOKEN.getName());

	private final String currentPk = Constants.STANDART_PK_MONGODB;

	public String getCurrentPk() {
		return this.currentPk;
	}

	public MongoCollection<Document> getCollection() {
		return this.collection;
	}

	@Override
	public String getNameCollection() {
		return this.getCollection().getNamespace().toString();
	}

	@Override
	public Token getToken() {
		Token retToken = null;

		final FindIterable<Document> iterable = this.getCollection().find(new Document(this.getCurrentPk(), new Document("$exists", true))).limit(1);

		final MongoCursor<Document> cursor = iterable.iterator();
		try {
			if (cursor.hasNext()) {
				final Document document = cursor.next();
				retToken = this.fillToken(document);
			}
		} finally {
			cursor.close();
		}

		return retToken;
	}

	/**
	 * Uses this ok because only is going to be used once
	 * @param document
	 * @return
	 */
	private Token fillToken(final Document document) {
		final Token retToken = new Token();
        retToken.setId(document.getString(Token.ID));
        retToken.setApiKey(document.getString(Token.API_KEY));
        retToken.setSecretKey(document.getString(Token.SECRET_KEY));
        return retToken;
	}

	@Override
	public void saveToken(final String jsonObjectString) {
		LOGGER.trace("Insert or update one in collection [{}], json [{}]", this.getCollection().getNamespace(), jsonObjectString);
		UtilDb.saveOrUpdateOneBsonString(jsonObjectString, this.getCurrentPk(), this.getCollection());
	}
}
