package market.api.connection.db.dao;

import java.util.List;

import market.api.enumtype.Direction;

import org.bson.Document;

public interface FilledTradeDao {

	/**
	 * Inserts or updates many document
	 * @param json
	 */
	void insertOrUpdateMany(String jsonObjectString);

	/**
	 * Gets non marked sorted by
	 * <pre>
	 * db.filledTrade.find({"direction":"buy", "mark":{"$exists":false}},{"price":1, "amount":1}).sort({"price":-1})
	 * db.filledTrade.find({"direction":"buy", "mark":{"$exists":true}},{"price":1, "amount":1})
	 * </pre>
	 * @param direction Direction
	 * @param ascDesc 1 or -1. Ascendent or descendent
	 * @return
	 */
	List<Document> getNonMarkedSortedBy(Direction direction, int ascDesc);

	/**
	 * Inserts or updates one document
	 * @param json
	 */
	void insertOrUpdate(String json);

	String getCollectionName();

}
