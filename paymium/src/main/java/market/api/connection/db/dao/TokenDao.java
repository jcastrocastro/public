package market.api.connection.db.dao;

import market.api.entity.Token;

public interface TokenDao {

	/**
	 * Gets only one token from DB. Got the first one
	 * @return
	 */
	Token getToken();

	/**
	 * Insert or update the token information in the token collection
	 *
	 * @param json
	 */
	void saveToken(String json);

	/**
	 * Gets name collection
	 * @return
	 */
	String getNameCollection();
}
