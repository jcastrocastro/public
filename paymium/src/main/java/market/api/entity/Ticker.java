package market.api.entity;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import market.api.enumtype.Currency;
import market.api.platformPaymium.publicall.AskTicker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Ticker {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTicker.class.getName());

	public static final String HIGH = "high";
	public static final String LOW = "low";
	public static final String VOLUME = "volume";
	public static final String BID = "bid";
	public static final String ASK = "ask";
	public static final String MIDPOINT = "midpoint";
	public static final String AT = "at";
	public static final String PRICE = "price";
	public static final String VWAP = "vwap";
	public static final String VARIATION = "variation";
	public static final String CURRENCY = "currency";
	public static final String OPEN = "open";
	public static final String TRADEID = "trade_id";
	public static final String SIZE = "size";

	// Cex IO parameter
	public static final String TIMESTAMP = "timestamp";
	public static final String LAST = "last";
	public static final String VOLUME30D = "volume30d";

	//	24h high price
	private double high;
	//	24h low price
	private double low;
	//	24h volume
	private double volume;
	//	bid price
	private double bid;
	//	ask price
	private double ask;
	//	midpoint price
	private double midpoint;
	//	24h volume-weighted average price
	private double vwap;
	//	timestamp
	private long at;
	//	price of latest trade
	private double price;
	// Open price
	private double open;
	//	24h variation (percentage)
	private double variation;
	//	currency
	private Currency currency;
	// Trade Id
	private String tradeId;
	// size
	private double size;



	public double getSize() {
		return this.size;
	}



	public void setSize(final double size) {
		this.size = size;
	}



	public double getOpen() {
		return this.open;
	}



	public void setOpen(final double open) {
		this.open = open;
	}



	public String getTradeId() {
		return this.tradeId;
	}



	public void setTradeId(final String tradeId) {
		this.tradeId = tradeId;
	}



	public double getHigh() {
		return this.high;
	}



	public void setHigh(final double high) {
		this.high = high;
	}



	public double getLow() {
		return this.low;
	}



	public void setLow(final double low) {
		this.low = low;
	}



	public double getVolume() {
		return this.volume;
	}



	public void setVolume(final double volume) {
		this.volume = volume;
	}



	public double getBid() {
		return this.bid;
	}



	public void setBid(final double bid) {
		this.bid = bid;
	}



	public double getAsk() {
		return this.ask;
	}



	public void setAsk(final double ask) {
		this.ask = ask;
	}



	public double getMidpoint() {
		return this.midpoint;
	}



	public void setMidpoint(final double midpoint) {
		this.midpoint = midpoint;
	}



	public long getAt() {
		return this.at;
	}



	public void setAt(final long at) {
		this.at = at;
	}



	public double getPrice() {
		return this.price;
	}



	public void setPrice(final double price) {
		this.price = price;
	}



	public double getVwap() {
		return this.vwap;
	}



	public void setVwap(final double vwap) {
		this.vwap = vwap;
	}



	public double getVariation() {
		return this.variation;
	}



	public void setVariation(final double variation) {
		this.variation = variation;
	}



	public Currency getCurrency() {
		return this.currency;
	}



	public void setCurrency(final Currency currency) {
		this.currency = currency;
	}



	@Override
	public String toString() {
		return "Ticker [tradeId=" + this.tradeId + ", high=" + this.high + ", low=" + this.low + ", volume=" + this.volume
				+ ", bid=" + this.bid + ", ask=" + this.ask + ", at=" + this.print(this.at) + ", price="
				+ this.price + ", open=" + this.open + "]";
	}



	private String print(final long timestamp) {
		LOGGER.trace("Timestamp at [{}]", timestamp);
		final Date date = new Date(timestamp*1000L);
		final Format format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return format.format(date);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (this.tradeId == null ? 0 : this.tradeId.hashCode());
		return result;
	}



	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Ticker other = (Ticker) obj;
		if (this.tradeId == null) {
			if (other.tradeId != null) {
				return false;
			}
		} else if (!this.tradeId.equals(other.tradeId)) {
			return false;
		}
		return true;
	}




}
