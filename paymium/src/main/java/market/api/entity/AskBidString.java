package market.api.entity;

public class AskBidString {

	private final String ask;
	
	private final String bid;
	
	public AskBidString(final String asks, final String bids){
		this.ask = asks;
		this.bid = bids;
	}

	public String getAsk() {
		return this.ask;
	}

	public String getBid() {
		return this.bid;
	}

	@Override
	public String toString() {
		return "AskBidString [ask=" + this.ask + ", bid=" + this.bid + "]";
	}
}
