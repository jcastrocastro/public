package market.api.entity;

import market.api.enumtype.Currency;


public class Trade {
	public static final String UUID = "uuid";
	public static final String TRADED_BTC = "traded_btc";
	public static final String TRADED_CURRENCY = "traded_currency";
	public static final String CREATED_AT = "created_at";
	public static final String CURRENCY = "currency";
	public static final String PRICE = "price";
	public static final String CREATED_AT_INT = "created_at_int";
			
	//		unique ID of trade
	private String uuid;
	
	//		amount of BTC traded
	private double traded_btc;
	
	//		amount of currency traded
	private double traded_currency;
	
	//		date created
	private String created_at;
	
	//		currency
	private Currency currency;
	
	//	price per BTC
	private double price;
	
	//		int timestamp
	private int created_at_int;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public double getTraded_btc() {
		return traded_btc;
	}

	public void setTraded_btc(double traded_btc) {
		this.traded_btc = traded_btc;
	}

	public double getTraded_currency() {
		return traded_currency;
	}

	public void setTraded_currency(double traded_currency) {
		this.traded_currency = traded_currency;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCreated_at_int() {
		return created_at_int;
	}

	public void setCreated_at_int(int created_at_int) {
		this.created_at_int = created_at_int;
	}

	@Override
	public String toString() {
		return "Trade [uuid=" + uuid + ", traded_btc=" + traded_btc
				+ ", traded_currency=" + traded_currency + ", created_at="
				+ created_at + ", currency=" + currency + ", price="
				+ price + ", created_at_int=" + created_at_int + "]";
	}
	
}
