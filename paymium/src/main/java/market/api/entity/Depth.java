package market.api.entity;

import org.json.JSONArray;

public class Depth {
	private JSONArray bids;
	private JSONArray asks;

	public JSONArray getBids() {
		return bids;
	}

	public void setBids(JSONArray bids) {
		this.bids = bids;
	}

	public JSONArray getAsks() {
		return asks;
	}

	public void setAsks(JSONArray asks) {
		this.asks = asks;
	}

	public boolean isEmpty(){
		return bids==null || asks == null || bids.length()==0 || asks.length()==0;
	}		
}