package market.api.entity;

import java.util.ArrayList;
import java.util.List;

public class AskBidDepth {

	private List<PriceDepth> bids;
	private List<PriceDepth> asks;

	public AskBidDepth (){
		bids = new ArrayList<>();
		asks = new ArrayList<>();
	}

	public List<PriceDepth> getBids() {
		return bids;
	}
	public void setBids(List<PriceDepth> bids) {
		this.bids = bids;
	}
	public List<PriceDepth> getAsks() {
		return asks;
	}
	public void setAsks(List<PriceDepth> asks) {
		this.asks = asks;
	}


}

