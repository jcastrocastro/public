package market.api.entity;

import market.api.enumtype.Currency;

public class AccountOperation {
	public static final String UUID="uuid";
	public static final String NAME="name";
	public static final String AMOUNT="amount";
	public static final String CURRENCY="currency";
	public static final String CREATED_AT="created_at";
	public static final String CREATED_AT_INT="created_at_int";
	public static final String IS_TRADING_ACCOUNT="is_trading_account";
	
	private String uuid;
	private String name;
	private double amount;
	private Currency currency;
	private String createdAt;
	private int createdAtInt;
	private boolean isTradingAccount;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public boolean isTradingAccount() {
		return isTradingAccount;
	}
	public void setTradingAccount(boolean isTradingAccount) {
		this.isTradingAccount = isTradingAccount;
	}
	public int getCreatedAtInt() {
		return createdAtInt;
	}
	public void setCreatedAtInt(int createdAtInt) {
		this.createdAtInt = createdAtInt;
	}
	@Override
	public String toString() {
		return "AccountOperation [uuid=" + uuid + ", name=" + name
				+ ", amount=" + amount + ", currency=" + currency
				+ ", createdAt=" + createdAt + ", createdAtInt="
				+ createdAtInt + ", isTradingAccount=" + isTradingAccount
				+ "]";
	}

}
