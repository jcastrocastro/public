package market.api.entity;

import market.api.common.Constants;
import market.api.common.Util;
import market.api.enumtype.Currency;

public class PriceDepth {
	public static final String TIMESTAMP = Constants.STANDART_PK_MONGODB; // PK
	public static final String AMOUNT = "amount";
	public static final String PRICE = "price";
	public static final String CURRENCY = "currency";

	private int timestamp;
	private double amount;
	private double price;
	private Currency currency;

	public int getTimestamp() {
		return this.timestamp;
	}
	public void setTimestamp(final int timestamp) {
		this.timestamp = timestamp;
	}
	public double getAmount() {
		return this.amount;
	}
	public void setAmount(final double amount) {
		this.amount = amount;
	}
	public double getPrice() {
		return this.price;
	}
	public void setPrice(final double price) {
		this.price = price;
	}
	public Currency getCurrency() {
		return this.currency;
	}
	public void setCurrency(final Currency currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "PriceDepth [timestamp=" + this.timestamp + ", amount=" + this.amount
				+ ", price=" + Util.convertDecimalNotation(this.price)+ ", currency=" + this.currency + "]";
	}

}