package market.api.entity;

/**
 * Example output: 
 * {"locked_eur":459.5,"locked_btc":3.7363,"balance_btc":1.658E-5,"balance_eur":252.34,"locale":"en","name":"PM-U62729767"}
 * @author raul
 *
 */
public class User {
	public static final String NAME="name";
	public static final String LOCALE="locale";
	public static final String BALANCE_BTC="balance_btc";
	public static final String LOCKED_BTC="locked_btc";
	public static final String BALANCE_EUR ="balance_eur";
	public static final String LOCKED_EUR="locked_eur";
	
	
	private String name;
	private String locale;
	private double balanceBtc;
	private double locked_btc;
	private double balanceEur;
	private double locked_eur;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public double getBalanceBtc() {
		return balanceBtc;
	}
	public void setBalanceBtc(double balanceBtc) {
		this.balanceBtc = balanceBtc;
	}
	public double getLocked_btc() {
		return locked_btc;
	}
	public void setLocked_btc(double lockedBtc) {
		this.locked_btc = lockedBtc;
	}
	public double getBalanceEur() {
		return balanceEur;
	}
	public void setBalanceEur(double balanceEur) {
		this.balanceEur = balanceEur;
	}
	public double getLocked_eur() {
		return locked_eur;
	}
	public void setLocked_eur(double lockedEur) {
		this.locked_eur = lockedEur;
	}
	@Override
	public String toString() {
		return "User [name=" + name + ", locale=" + locale
				+ ", balanceBtc=" + balanceBtc + ", locked_btc="
				+ locked_btc + ", balanceEur=" + balanceEur
				+ ", locked_eur=" + locked_eur + "]";
	}	
}