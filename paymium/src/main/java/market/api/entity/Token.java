package market.api.entity;

import market.api.common.Constants;

public class Token {

	public static final String ID = Constants.STANDART_PK_MONGODB;
	public static final String SECRET_KEY = Constants.API_SECRET_KEY;
	public static final String API_KEY = Constants.API_KEY;

	private String id;
	private String secretKey;
	private String apiKey;
	public String getId() {
		return this.id;
	}
	public void setId(final String id) {
		this.id = id;
	}
	public String getSecretKey() {
		return this.secretKey;
	}
	public void setSecretKey(final String secretKey) {
		this.secretKey = secretKey;
	}
	public String getApiKey() {
		return this.apiKey;
	}
	public void setApiKey(final String apiKey) {
		this.apiKey = apiKey;
	}

}
