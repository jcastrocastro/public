package market.api.entity;

import java.util.List;

import market.api.enumtype.Currency;
import market.api.enumtype.State;

public class Order {
	public static final String UUID="uuid";
	public static final String AMOUNT="amount";
	public static final String STATE="state";
	public static final String BTC_FEE="btc_fee";		
	public static final String CURRENCY_FEE="currency_fee";
	public static final String UPDATED_AT="updated_at";
	public static final String CREATED_AT="created_at";
	public static final String CURRENCY="currency";
	public static final String TYPE="type";
	public static final String ACCOUNT_OPERATIONS="account_operations";		
	
	private String uuid;
	private Double amount;
	private State state;
	private double btcFee;
	private double currencyFee;
	private String updateAt;
	private String createdAt;
	private Currency currency;
	private String type;
	private List<AccountOperation> accountOperations;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public State getState() {
		return state;
	}
	public void setState(State state) {
		this.state = state;
	}
	public double getBtcFee() {
		return btcFee;
	}
	public void setBtcFee(double btcFee) {
		this.btcFee = btcFee;
	}
	public double getCurrencyFee() {
		return currencyFee;
	}
	public void setCurrencyFee(double currencyFee) {
		this.currencyFee = currencyFee;
	}
	public String getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(String updateAt) {
		this.updateAt = updateAt;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<AccountOperation> getAccountOperations() {
		return accountOperations;
	}
	public void setAccountOperations(List<AccountOperation> accountOperations) {
		this.accountOperations = accountOperations;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Order [uuid=" + uuid + ", amount=" + amount
				+ ", state=" + state + ", btcFee=" + btcFee
				+ ", currencyFee=" + currencyFee + ", updateAt=" + updateAt
				+ ", createdAt=" + createdAt + ", currency=" + currency
				+ ", type=" + type + ", accountOperations="
				+ accountOperations + "]";
	}
}