package market.api.entity;


import java.util.List;

import market.api.enumtype.Currency;
import market.api.enumtype.Direction;
import market.api.enumtype.State;

public class TradeOrder {

	public static final String UUID = "uuid";
	public static final String AMOUNT = "amount";
	public static final String STATE = "state";
	public static final String BTC_FEE = "btc_fee";
	public static final String CURRENCY_FEE = "currency_fee";
	public static final String UPDATED_AT = "updated_at";
	public static final String CREATED_AT = "created_at";
	public static final String CURRENCY = "currency";
	public static final String TYPE = "type";
	public static final String TRADED_BTC = "traded_btc";
	public static final String TRADED_CURRENCY = "traded_currency";
	public static final String DIRECTION = "direction";
	public static final String PRICE = "price";
	public static final String ACCOUNT_OPERATIONS = "account_operations";

	public static final String MARK = "mark";
	public static final String ID_BUY = "idBuy";

	/** unique id */
	private String uuid;
	/** currency amount */
	private double amount;
	/** order state */
	private State state;
	/** BTC fee collected */
	private double btcFee;
	/** currency fee collected */
	private double currencyFee;
	/** date updated */
	private String updateAt;
	/** date created */
	private String createdAt;
	/** currency */
	private Currency currency;
	/** order type */
	private String type;
	/** BTC exchanged */
	private double tradedBtc;
	/** currency exchanged */
	private double traded_currency;
	/** trade direction ("buy" or "sell") */
	private Direction direction;
	/** price per BTC */
	private double price;
	/** An array of account operations executed */
	private List<AccountOperation> account_operations;

	public String getUuid() {
		return this.uuid;
	}
	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}
	public double getAmount() {
		return this.amount;
	}
	public void setAmount(final double amount) {
		this.amount = amount;
	}
	public State getState() {
		return this.state;
	}
	public void setState(final State state) {
		this.state = state;
	}
	public double getBtcFee() {
		return this.btcFee;
	}
	public void setBtcFee(final double btcFee) {
		this.btcFee = btcFee;
	}
	public double getCurrencyFee() {
		return this.currencyFee;
	}
	public void setCurrencyFee(final double currencyFee) {
		this.currencyFee = currencyFee;
	}
	public String getUpdateAt() {
		return this.updateAt;
	}
	public void setUpdateAt(final String updateAt) {
		this.updateAt = updateAt;
	}
	public String getCreatedAt() {
		return this.createdAt;
	}
	public void setCreatedAt(final String createdAt) {
		this.createdAt = createdAt;
	}
	public Currency getCurrency() {
		return this.currency;
	}
	public void setCurrency(final Currency currency) {
		this.currency = currency;
	}
	public String getType() {
		return this.type;
	}
	public void setType(final String type) {
		this.type = type;
	}
	public double getTradedBtc() {
		return this.tradedBtc;
	}
	public void setTradedBtc(final double tradedBtc) {
		this.tradedBtc = tradedBtc;
	}
	public double getTraded_currency() {
		return this.traded_currency;
	}
	public void setTraded_currency(final double traded_currency) {
		this.traded_currency = traded_currency;
	}
	public Direction getDirection() {
		return this.direction;
	}
	public void setDirection(final Direction direction) {
		this.direction = direction;
	}
	public double getPrice() {
		return this.price;
	}
	public void setPrice(final double price) {
		this.price = price;
	}
	public List<AccountOperation> getAccount_operations() {
		return this.account_operations;
	}
	public void setAccount_operations(final List<AccountOperation> account_operations) {
		this.account_operations = account_operations;
	}
	@Override
	public String toString() {
		return "TradeOrder [uuid=" + this.uuid + ", amount=" + this.amount + ", state="
				+ this.state + ", btcFee=" + this.btcFee + ", currencyFee=" + this.currencyFee
				+ ", updateAt=" + this.updateAt + ", createdAt=" + this.createdAt
				+ ", currency=" + this.currency + ", type=" + this.type + ", tradedBtc="
				+ this.tradedBtc + ", traded_currency=" + this.traded_currency
				+ ", direction=" + this.direction + ", price=" + this.price
				+ ", account_operations=" + this.account_operations + "]";
	}

}
