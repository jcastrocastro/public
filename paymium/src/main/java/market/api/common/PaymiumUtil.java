package market.api.common;

import market.api.application.ApplicationConf;
import market.api.entity.TradeOrder;
import market.api.exception.ServiceException;

import org.bson.Document;

public class PaymiumUtil {

	/**
	 * Gets euro fee truncating to 8 decimals
	 *
	 * @param document
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate or The platform fee is undefined
	 */
	public static double getCurrencyFeeOrderPaymium(final Document document)
																		throws ServiceException {
		final String jsonDoc = document.toJson();
		return Util.getCurrencyFee(Util.getDouble(jsonDoc, TradeOrder.AMOUNT), Util.getDouble(jsonDoc, TradeOrder.PRICE), ApplicationConf.getPlatformCostFee());
	}

	/**
	 * Gets btc fee truncating to 8 decimals
	 *
	 * @param document
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate or The platform fee is undefined
	 */
	public static double getBtcFeeOrderPaymium(final Document document) throws ServiceException {
		return Util.getBtcFee(Util.getDouble(document.toJson(), TradeOrder.AMOUNT), ApplicationConf.getPlatformCostFee());
	}

	/**
	 * Calculates amount minus fee with truncate
	 *
	 * @param amountWithoutFee
	 * @param cutDecimals
	 * @return
	 * @throws ServiceException The platform fee is undefined
	 */
	public static double calculateAmountWithFee(final double amountWithoutFee, final int cutDecimals)
																						throws ServiceException {

		return Util.truncateDoubleDecimal(amountWithoutFee * (1 - ApplicationConf.getPlatformCostFee()), cutDecimals);
	}

	/**
	 * Calculates amount minus fee without truncate
	 *
	 * @param amountWithoutFee
	 * @return
	 * @throws ServiceException
	 * 						The platform fee is undefined
	 */
	public static double calculateAmountWithFee(final double amountWithoutFee)
																	throws ServiceException {
		return amountWithoutFee * (1 - ApplicationConf.getPlatformCostFee());
	}
}
