package market.api.common;

public class Constants {

	// Paymium Web addresses
	public static final String TICKER_URL_WEB = "https://paymium.com/api/v1/data/eur/ticker";
	public static final String LATEST_TRADES_WEB = "https://paymium.com/api/v1/data/eur/trades";
	public static final String MARKET_DEPTH_WEB = "https://paymium.com/api/v1/data/eur/depth";
	public static final String USER_INFO_URL = "https://paymium.com/api/v1/user";
	public static final String USER_ORDER_URL = "https://paymium.com/api/v1/user/orders";

	// CEX IO WEB
	public static final String TICKER_URL_CEX = "https://cex.io/api/ticker/BTC/EUR/";

	// Platform name
	public static final String PLATFORM_CEX_IO = "CEX-IO";
	public static final String PLATFORM_PAYMIUM = "PAYMIUM";

	// Conventions
	public static final String UTF_8 = "UTF-8";
	public static final String CHARSET = "UTF-8";

	// Constants Strings
	public static final String BIDS = "bids";
	public static final String ASKS = "asks";

	public static final String API_KEY = "Api-Key";

	public static final String API_SIGNATURE = "Api-Signature";

	public static final String API_NONCE = "Api-Nonce";

	public static final String API_WEB_USER = "Api-user";

	public static final String POST_REQUEST_HTTP = "POST";

	public static final String GET_REQUEST_HTTP = "GET";

	public static final String API_SECRET_KEY = "Hello";

	public static final String BODY_HTTP_MSG = "";

	public static final String CONTENT_TYPE = "Content-Type";

	public static final String INFINITUM_SENTENCE = "Infinitum";

	/** Mongo constants strings */
	public static final String GREATER_THAN = "$gt";

	/** Mark to know the buy price of the operation to associate it with a selling order */
	public static final String BUY_PRICE = "buyPrice";

	/* Empty String */
	public static final String EMPTY_STRING = "";

	// Web constants
	public static final String WEB_USER_AGENT = "Mozilla/5.0";

	// Constants numbers
	public static final long ONE_MINUTE_IN_MILLIS = 60000L;

	public static final int NUMBER_MINUTES_IN_ONE_YEAR = 365*24*60; // 365 days, 24 hours, 60 minutes

	public static final int SEC_IN_ONE_MINUTE = 60;

	public static final int STANDART_CUT_DECIMALS = 8;

	public static final int SHORT_CUT_DECIMALS = 2;

	// Engine Configuration files
	public static final String PATH_CONFIGURATION_FILE_DB = "/configuration.xml";

	public static final String PATH_CONFIGURATION_VERSION_FILE = "/version.xml";

	// Standard PK mongodb: _id
	public static final String STANDART_PK_MONGODB = "_id";

	public static final String PARAM_QUARTZ_KEY = "param";

	public static final String TIMESTAMP_PK = "timestamp";

	public static final String ORDER_PK = "uuid";

	// Paymium accuracy
	public static final double PAYMIUM_ACCURACY = 1e-3;

	// Symbols
	public static final String POINT = ".";
	public static final String Z_UTC = "Z";

}
