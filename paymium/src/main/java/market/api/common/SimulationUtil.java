package market.api.common;

import java.util.Map;

import market.api.application.ApplicationConf;
import market.api.entity.TradeOrder;
import market.api.enumtype.State;
import market.api.exception.ServiceException;

import org.json.JSONObject;

public class SimulationUtil {

	private SimulationUtil() {}

	/**
	 * Set trade fee
	 * @param valueByKeyTradeOrder
	 * @return
	 * @throws ServiceException The platform fee is undefined
	 */
	public static String setTradeFee(final Map<String, Object> valueByKeyTradeOrder)
																				throws ServiceException {
		final double amount = (double)valueByKeyTradeOrder.get(TradeOrder.AMOUNT);
		final double price = (double)valueByKeyTradeOrder.get(TradeOrder.PRICE);
		final double feePlatform = ApplicationConf.getPlatformCostFee();
		valueByKeyTradeOrder.put(TradeOrder.BTC_FEE, Util.getBtcFee(amount, feePlatform));
		valueByKeyTradeOrder.put(TradeOrder.CURRENCY_FEE, Util.getCurrencyFee(amount, price, feePlatform));
		valueByKeyTradeOrder.put(TradeOrder.STATE, State.SIMULATED.getName());
		valueByKeyTradeOrder.put(TradeOrder.UPDATED_AT, TimeUtil.getNow());
		valueByKeyTradeOrder.put(TradeOrder.CREATED_AT, TimeUtil.getNow());
//		valueByKeyTradeOrder.put(TradeOrder.ACCOUNT_OPERATIONS, new ArrayList<AccountOperation>());

		return JSONObject.valueToString(valueByKeyTradeOrder);
	}


}
