package market.api.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import market.api.entity.Depth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Conversor {

	private final static Logger LOGGER = LoggerFactory.getLogger(Conversor.class.getName());

	public static List<JSONObject> getListJsonObjects(final BufferedReader reader) throws JSONException, IOException {

		final List<JSONObject> retJsonObjectList = new ArrayList<>();

		for (String line; (line = reader.readLine()) != null;) {
			if (!Util.isEmpty(line)){
				retJsonObjectList.add(new JSONObject(line));
			} else {
				LOGGER.error("Empty line list json objects");
			}
		}
		LOGGER.trace("Length [{}] of List of json objects", retJsonObjectList.size());

		// Close reader
		reader.close();

		return retJsonObjectList;
	}

	public static JSONObject getJsonObject(final BufferedReader reader) throws JSONException, IOException {
		JSONObject retJsonObject = null;

		if (Util.isEmpty(reader)){
			LOGGER.warn("Something is wrong in the GET/POST");
			return null;
		}

		String line = null;
		if ((line = reader.readLine()) != null) {
			if (!Util.isEmpty(line)){
				retJsonObject = new JSONObject(line);
			} else {
				LOGGER.error("Empty line list json objects");
			}
		}

		// Close the reader
		reader.close();

		return retJsonObject;
	}

	public static JSONArray getJsonArray(final BufferedReader reader) throws JSONException, IOException {
		JSONArray retJsonArray = null;

		String line = null;
		if ((line = reader.readLine()) != null) {
			if (!Util.isEmpty(line)){
				retJsonArray = new JSONArray(line);
			} else {
				LOGGER.error("Empty line array json objects");
			}
			LOGGER.trace("Length of Json Array [{}]", retJsonArray.length());
		}

		reader.close();

		return retJsonArray;
	}

	public static Depth getMarketDepth(final BufferedReader reader) throws IOException {
		final Depth retDepth = new Depth();

		String line = null;

		if ((line = reader.readLine()) != null) {
			// Asks
			setAsks(retDepth, line);
			// Bids
			setBids(retDepth, line);
		}

		return retDepth;
	}

	private static void setBids(final Depth retDepth, final String line) {
		final JSONObject jsonObject = new JSONObject(line);

		final JSONArray jsonArrayObj = (JSONArray)jsonObject.get(Constants.BIDS);
		if (!Util.isEmpty(jsonArrayObj)){
			retDepth.setBids(jsonArrayObj);
		} else {
			LOGGER.error("Empty bids json object");
		}
	}

	private static void setAsks(final Depth depth, final String line) {
		final JSONObject jsonObject = new JSONObject(line);

		final JSONArray jsonArrayObj = (JSONArray)jsonObject.get(Constants.ASKS);
		if (!Util.isEmpty(jsonArrayObj)){
			depth.setAsks(jsonArrayObj);
		} else {
			LOGGER.error("Empty asks json object");
		}
	}

}
