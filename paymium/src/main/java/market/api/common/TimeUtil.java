package market.api.common;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import market.api.exception.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeUtil {

	private final static Logger LOGGER = LoggerFactory.getLogger(TimeUtil.class.getName());

	private static final String PATTERN_DAY_HOUR_MIN_SEC = "yyyy-MM-dd HH:mm:ss";

	private static final String PATTERN_UTC = "yyyy-MM-dd'T'HH:mm:ss'Z'";

	/**
	 * UTC time format now
	 *
	 * @return
	 */
	public static String getUtcTimeNow(){
		return Instant.now().toString();
	}

	/**
	 * Get time now in millis
	 * @return
	 */
	public static long getTimeNow(){
		return System.currentTimeMillis();
	}

	/**
	 * Start count time in millis from now
	 * @return
	 */
	public static long startCountTime() {
		return getTimeNow();
	}

	public static long getTime(final LocalDateTime localDateTime) {
		final ZoneId zoneId = ZoneId.systemDefault();
		return localDateTime.atZone(zoneId).toEpochSecond();
	}

	public static Date getNow(){
		return new Date();
	}

	public static LocalDateTime getNowLocalDateTime() {
		return LocalDateTime.now();
	}

	public static String printTime(final long millis) {
		return String.format("%d min, %d sec, %d millis",
			    TimeUnit.MILLISECONDS.toMinutes(millis),
			    TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)),
			    millis%1000
			);
	}

	public static LocalDateTime addMinutes(final LocalDateTime localDeateTime, final int minutes){
		return localDeateTime.plusMinutes(minutes);
	}

	public static java.util.Date convertToDate(final LocalDateTime localDateTime){
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	public static LocalDateTime convertToLocalDateTime(final java.util.Date date){
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

	public static Date convertTimestampToDate(final Long timestamp) {
		if (timestamp==null){
			return null;
		}

		return new Date(new Timestamp(timestamp).getTime());
	}

	public static void finishCountTime(final long startTime, final String freeTextToPrint) {
		LOGGER.debug("Estimated time [{}]. {}", TimeUtil.printTime(System.currentTimeMillis() - startTime), freeTextToPrint);
	}

	public static Date addMinutes(final Date date, final int minutes) {
		final LocalDateTime localDateTimeDate = convertToLocalDateTime(date);
		return convertToDate(localDateTimeDate.plusMinutes(minutes));
	}

	/**
	 * localDateTime1 > localDateTime2
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isGreater(final LocalDateTime localDateTime1, final LocalDateTime localDateTime2) {
		final LocalDate localDate1 = localDateTime1.toLocalDate();
		final LocalDate localDate2 = localDateTime2.toLocalDate();

		return localDate1.compareTo(localDate2)>0; //the comparator value, negative if less, positive if greater
	}

	/**
	 * localDateTime1 < localDateTime2
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean isLower(final LocalDateTime localDateTime1, final LocalDateTime localDateTime2) {
		final LocalDate localDate1 = localDateTime1.toLocalDate();
		final LocalDate localDate2 = localDateTime2.toLocalDate();

		return localDate1.compareTo(localDate2)<0; //the comparator value, negative if less, positive if greater
	}



	public static boolean isEqual(final LocalDateTime localDateTime1, final LocalDateTime localDateTime2) {
		final LocalDate localDate1 = localDateTime1.toLocalDate();
		final LocalDate localDate2 = localDateTime2.toLocalDate();
		return localDate1.compareTo(localDate2)==0; //the comparator value, negative if less, positive if greater
	}

	public static LocalDateTime addDays(final LocalDateTime localDateTime, final int numDays) {
		return localDateTime.plusDays(numDays);
	}

	public static LocalDateTime addHours(final LocalDateTime localDateTime, final int numHours) {
		return localDateTime.plusHours(numHours);
	}

	/**
	 * Gets date given a string
	 *
	 * @param dateString with pattern: yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static Date getDate(final String dateString) {
		if (Util.isEmpty(dateString)) {
			return null;
		}

		final LocalDateTime localDateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern(PATTERN_DAY_HOUR_MIN_SEC, Locale.ENGLISH));
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * Gets date given a string
	 *
	 * @param dateString with UTC pattern: yyyy-MM-dd'T'HH:mm:ss'Z'
	 * @return
	 * @throws ServiceException
	 */
	public static Date getDateUtc(final String dateUtcString) throws ServiceException {
		if (Util.isEmpty(dateUtcString)) {
			LOGGER.error("Empty string [{}]", dateUtcString);
			return null;
		}

		final ZonedDateTime utcZoneDateTime = ZonedDateTime.of(LocalDateTime.parse(dateUtcString, DateTimeFormatter.ofPattern(PATTERN_UTC)), ZoneOffset.UTC);

		return Date.from(utcZoneDateTime.toInstant());
	}

}

