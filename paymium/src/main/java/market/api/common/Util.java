package market.api.common;


import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mongodb.client.MongoCollection;

import market.api.application.configuration.Configuration.Engine.JobPlan;
import market.api.application.configuration.Configuration.Engine.JobPlan.Params;
import market.api.application.configuration.Configuration.InitApplicationjobPlans.JobPlan1;
import market.api.connection.db.BuySellHistDao;
import market.api.connection.db.dao.BuySellDao;
import market.api.entity.PriceDepth;
import market.api.entity.TradeOrder;
import market.api.enumtype.Currency;
import market.api.enumtype.TypeOrder;
import market.api.exception.ServiceException;

public class Util {

	private final static Logger LOGGER = LoggerFactory.getLogger(Util.class.getName());

	private static final StringBuilder stringBuilder = new StringBuilder(Constants.PARAM_QUARTZ_KEY);

	public static boolean isEmpty(final Collection<?> list) {
		return list == null || list.isEmpty();
	}

	public static boolean isEmpty(final String string) {
		return string==null || string.isEmpty();
	}

	public static boolean isEmpty(final JSONArray jsonArray) {
		return jsonArray == null || jsonArray.length()==0;
	}

	public static boolean isEmpty(final Object obj) {
		return obj==null;
	}

	@SafeVarargs
	public static <E> boolean isEmpty(final E... array) {
		return array==null || Util.isEmpty(Lists.newArrayList(array));
	}

	/**
	 * Converts Decimal notation. Default 8 decimals
	 * @param doubleScientificNotation Double scientific notation
	 * @return String with the scientific notation
	 */
	public static String convertDecimalNotation (final double doubleScientificNotation) {
		return Util.convertDecimalNotation(doubleScientificNotation, Constants.STANDART_CUT_DECIMALS);
	}

	/**
	 * Converts Decimal notation.
	 * @param doubleScientificNotation Double scientific notation
	 * @param numberDecimal Number of decimal
	 * @return String with the scientific notation
	 */
	public static String convertDecimalNotation (final double doubleScientificNotation, final int numberDecimal) {
		return new DecimalFormat("#." + Util.repeatCharacter("#", numberDecimal)).format(doubleScientificNotation);
	}

	/**
	 * Get a number of character
	 * @param character Character to repeat
	 * @param numberChar Number of character
	 * @return
	 */
	public static String repeatCharacter(final String character, final int numberChar) {
		return Strings.repeat(character, numberChar);
	}

	/**
	 * Truncates value with the given number of decimals
	 * @param value Value
	 * @param decimalNumber Number of decimals
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate
	 */
	public static double truncateDoubleDecimal(final double value, final int decimalNumber) throws ServiceException {
		double calculateExp = 1e1;
		switch (decimalNumber) {
		case 1:
			calculateExp = 1e1;
			break;
		case 2:
			calculateExp = 1e2;
			break;
		case 3:
			calculateExp = 1e3;
			break;
		case 4:
			calculateExp = 1e4;
			break;
		case 5:
			calculateExp = 1e5;
			break;
		case 6:
			calculateExp = 1e6;
			break;
		case 7:
			calculateExp = 1e7;
			break;
		case 8:
			calculateExp = 1e8;
			break;
		default:
			LOGGER.error("Non supported decimal cut number [{}]. Should be between 1 and 8 inclusives", decimalNumber);
			throw new ServiceException("Non supported decimal cut number");
		}

	    return Math.floor(value * calculateExp) / calculateExp;
	}

	public static<E> E ifNotNull(final E instance) {
		return instance==null ? null: instance;
	}

	public static boolean isEqual(final long long1, final long long2) {
		return long1==long2;
	}

	/**
	 * Gets a list of documents from a String json array
	 * @param jsonArray
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Document> getListDocs(final String jsonArray) {
		List<Document> retList = new ArrayList<>();

		final Document doc = Document.parse(new StringBuilder("{ \"list\":").append(jsonArray).append("}").toString());

		final Object list = doc.get("list");
		if(list instanceof List<?>) {
			retList = (List<Document>) doc.get("list");
		}

		return retList;
	}

	public static List<JobPlan> conversor(final List<JobPlan1> jobList) {
		final List<JobPlan> retList = new ArrayList<>();
		for (final JobPlan1 job: jobList){
			final JobPlan jobPlan = new JobPlan();

			jobPlan.setJobPlanClass(job.getJobPlanClass());
			jobPlan.setInterval(job.getInterval());
			jobPlan.setDelay(job.getDelay());
			jobPlan.setStartTime(job.getStartTime());
			jobPlan.setEndTime(job.getEndTime());
			jobPlan.setMaxTime(job.getMaxTime());
			copyPlanParams(jobPlan, job);

			retList.add(jobPlan);
		}

		return retList;
	}

	/**
	 * Copies from jobPlan to jobPlan1
	 *
	 * @param targetPlan
	 * @param sourcePlan
	 */
	private static void copyPlanParams(final JobPlan targetPlan, final JobPlan1 sourcePlan) {
		if (!Util.isEmpty(sourcePlan.getParams())){
			if (targetPlan.getParams()==null) {
				targetPlan.setParams(new Params());
			}

			for (final String param: sourcePlan.getParams().getParam()) {
				targetPlan.getParams().getParam().add(param);
			}
		}
	}

	@Deprecated
	/**
	 * Deprecated for inefficient
	 * @param jsonObject
	 * @param currentPk
	 * @return
	 */
	public static JSONObject adaptPrimaryKeyToMongoDb(final JSONObject jsonObject, final String currentPk) {
		final String tempPkValue = jsonObject.getString(currentPk);
		jsonObject.remove(currentPk);
		jsonObject.put(Constants.STANDART_PK_MONGODB, tempPkValue);
		return jsonObject;
	}

	public static String adaptPrimaryKeyToMongoDb(final String jsonObjectString, final String currentPk) {
		return jsonObjectString.replace(currentPk, Constants.STANDART_PK_MONGODB);
	}

	/**
	 * Reads the reader and put the content in only one string
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	public static String getString(final BufferedReader reader) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();

		String line = null;
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
		}

		return stringBuilder.toString();
	}

	public static PriceDepth fullMarketDepth(final JSONObject jsonObject) {
		final PriceDepth priceDepth = new PriceDepth();

		priceDepth.setTimestamp(jsonObject.getInt(PriceDepth.TIMESTAMP));
		priceDepth.setAmount(jsonObject.getDouble(PriceDepth.AMOUNT));
		priceDepth.setPrice(jsonObject.getDouble(PriceDepth.PRICE));
		priceDepth.setCurrency(Currency.getCurrency(jsonObject.getString(PriceDepth.CURRENCY)));

		return priceDepth;
	}

	/**
	 *	Gets btc fee truncating to 8 decimals
	 *
	 * @param amount
	 * @param feePlatform
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate
	 */
	public static double getBtcFee(final Double amount, final double feePlatform) throws ServiceException {
		return Util.truncateDoubleDecimal(amount*feePlatform, 8);
	}

	/**
	 *Gets euro fee truncating to 8 decimals
	 *
	 * @param amount
	 * @param price
	 * @param feePlatform
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate
	 */
	public static double getCurrencyFee(final Double amount, final Double price, final double feePlatform) throws ServiceException {
		return Util.truncateDoubleDecimal(amount*price*feePlatform, 8);
	}

	/**
	 * d1 == d2
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isEquals(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) == Double.doubleToLongBits(d2);
	}

	/**
	 * d1 > d2
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isGreater(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) > Double.doubleToLongBits(d2);
	}

	/**
	 * d1 > d2
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isGreaterOrEqual(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) >= Double.doubleToLongBits(d2);
	}

	/**
	 * d1 < d2
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isLower(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) < Double.doubleToLongBits(d2);
	}

	/**
	 * d1 <= d2
	 * @param d1
	 * @param d2
	 * @return
	 */
	public static boolean isLowerOrEqual(final double d1, final double d2) {
		return Double.doubleToLongBits(d1) <= Double.doubleToLongBits(d2);
	}

	public static String getParamsName(final int numParam) {
		String retString = null;
		retString = stringBuilder.append(numParam).toString();

		//remove last added
		final int sizeNum = numParam > 9 ? 2 : 1;
		final int lastIdx = stringBuilder.lastIndexOf(String.valueOf(numParam));
		stringBuilder.replace(lastIdx, lastIdx + sizeNum, Constants.EMPTY_STRING);

		return retString;
	}

	public static double getDouble(final Document jsonString, final String key)
																		throws ServiceException {
		return Util.getDouble(jsonString.toJson(), key);
	}

	/**
	 *
	 * @param jsonString Json string
	 * @param key Key to look for value in the given json string
	 * @return Double
	 * @throws ServiceException Error the jsonString key value is not a number
	 */
	public static double getDouble(final String jsonString, final String key)
																		throws ServiceException {
		double retValue = 0.0;

		final Object value = Document.parse(jsonString).get(key);

		if (value instanceof Double) {
			retValue = (double)value;
		} else if (value instanceof Integer) {
			retValue = (int)value;
		} else if (value instanceof String) {
			try {
				retValue = Double.valueOf((String)value);
			} catch (final NumberFormatException nexc){
				LOGGER.error("Number format exception [{}]", value);
				throw new ServiceException(nexc);
			}
		} else if (value instanceof Long) {
			retValue = (long)value;
		} else {
			LOGGER.error("The value of the key [{}] doesn't exist or is not a number. Json [{}]", key, jsonString);
			throw new ServiceException("The key in the json string is NOT a number");
		}

		return retValue;
	}

	/**
	 * Round up max two decimals
	 * @param val
	 * @return
	 */
	public static double roundUp(final double val) {
		return new BigDecimal(val).setScale(Constants.SHORT_CUT_DECIMALS, RoundingMode.HALF_UP).doubleValue();
	}

	/**
	 * Moves mark to historic
	 *
	 * @param markSellDoc Mark sell doc
	 * @param buySellHistDao
	 * @param buySellDao
	 */
	public static void moveMarkToHist(final Document markSellDoc, final BuySellDao buySellDao, final BuySellHistDao buySellHistDao) {
		// Copy the pending mark sell into the historic collection
		buySellHistDao.insertOrUpdate(markSellDoc.toJson());

		// Remove the pending mark to sell
		buySellDao.delete(markSellDoc);
	}

	/**
	 * Is the date +  minutes > current time now
	 * @param date
	 * @param minutes
	 * @return
	 */
	public static boolean isGreaterDate(final Date date, final int minutes) {
		final LocalDateTime lastModifiedUserInfoUpdated = TimeUtil.addMinutes(TimeUtil.convertToLocalDateTime(date), minutes);
		final LocalDateTime timeNow = TimeUtil.getNowLocalDateTime();

		LOGGER.trace("Compare if date1 [{}] is greater than current date now [{}]", lastModifiedUserInfoUpdated, timeNow);

		return lastModifiedUserInfoUpdated.compareTo(timeNow)>0;
	}


	public static void createIndex(final String indexKey, final int ascDesc, final String indexKey2, final int ascDesc2, final String indexKey3, final int ascDesc3, final MongoCollection<Document> collection) {
			LOGGER.info("Create index [{}], ascDesc [{}], index2 [{}], ascDesc2 [{}], index3 [{}], ascDesc3 [{}] in collection [{}]",
															new Object[]{indexKey, ascDesc, indexKey2, ascDesc2, indexKey3, ascDesc3, collection.getNamespace()});
			collection.createIndex(new Document(indexKey, ascDesc).append(indexKey2, ascDesc2).append(indexKey3, ascDesc3));
	}

	public static void createIndex(final String indexKey, final int ascDesc, final String indexKey2, final int ascDesc2, final MongoCollection<Document> collection) {
		LOGGER.trace("Create index [{}], index2[{}], ascDesc [{}], ascDesc2 [{}] in collection [{}]",
																new Object[]{indexKey, ascDesc, indexKey2, ascDesc2, collection.getNamespace()});
		collection.createIndex(new Document(indexKey, ascDesc).append(indexKey2, ascDesc2));
	}

	public static void createIndex(final String indexKey, final int ascDesc, final MongoCollection<Document> collection) {
		LOGGER.trace("Create index [{}], ascDesc [{}] in collection [{}]",
																	new Object[]{indexKey, ascDesc, collection.getNamespace()});
		collection.createIndex(new Document(indexKey, ascDesc));
	}

	public static JSONArray keepOnlyGivenType(final JSONArray jsonAliveArray, final TypeOrder typeOrder) {
		// Remove all not limit orders
		for (int cnt=0; cnt<jsonAliveArray.length();cnt++) {
			 final JSONObject jsonObject = jsonAliveArray.getJSONObject(cnt);
			 if (!jsonObject.getString(TradeOrder.TYPE).equals(typeOrder.getName())){
				 jsonAliveArray.remove(cnt);
			 }
		}

		return jsonAliveArray;
	}
}
