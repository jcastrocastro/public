package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Direction {
	
	BUY ("buy"),
	SELL ("sell");
	
	private final static Logger LOGGER = LoggerFactory.getLogger(Direction.class.getName());
	
	private String name;

	Direction(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static Direction getDirection(String directionString){
		if (Util.isEmpty(directionString)) {
			LOGGER.error("Direction can not be empty");
			return null;
		}
		
		for (Direction direction: Direction.values()){
			if (direction.getName().equals(directionString)){
				return direction;
			}
		}
		
		LOGGER.error("Not found direction string [{}]", directionString);
		return null;
	}
	
}
