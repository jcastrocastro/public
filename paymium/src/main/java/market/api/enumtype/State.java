package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum State {
	// Paymiun states
	PENDING_EXECUTION ("pending_execution"),
	PENDING ("pending"),
	PROCESSING ("processing"),
	AUTHORIZED ("authorized"),
	/** Processed orders not matched */
	ACTIVE ("active"),
	/** Already executed orders */
	FILLED ("filled"),
	EXECUTED ("executed"),
	CANCELED ("canceled"),

	// System simulation states
	SIMULATED ("simulated"),

	// System state used for it
	MARK_APPL_SELL_STATE("mark_appl_sell_state");

	private final static Logger LOGGER = LoggerFactory.getLogger(State.class.getName());

	private final String name;

	State(final String name){
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	/**
	 *
	 * @param nameState
	 * @return The string name of the state or null if not found
	 */
	public static State getState(final String nameState) {
		if (Util.isEmpty(nameState)) {
			LOGGER.error("NameState can not be empty");
			return null;
		}

		for (final State state: State.values()) {
			if (state.getName().equals(nameState)){
				return state;
			}
		}

		LOGGER.error("Not found name state [{}]", nameState);
		return null;
	}
}
