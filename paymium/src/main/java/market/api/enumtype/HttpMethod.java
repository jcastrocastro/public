package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum HttpMethod {
	/** GET */
	GET ("GET"),
	/** POST */
	POST ("POST"),
	/** PUT */
	PUT ("PUT"),
	/** DELETE */
	DELETE ("DELETE"),
	/** HEAD */
	HEAD ("HEAD"),
	/** OPTIONS */
	OPTIONS ("OPTIONS");
	
	private final static Logger LOGGER = LoggerFactory.getLogger(HttpMethod.class.getName());
	
	private String name;

	HttpMethod(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static HttpMethod getDirection(String httpMethodString){
		if (Util.isEmpty(httpMethodString)) {
			LOGGER.error("httpMethodString can not be empty");
			return null;
		}
		
		for (HttpMethod httpMethod: HttpMethod.values()){
			if (httpMethod.getName().equals(httpMethodString)){
				return httpMethod;
			}
		}
		
		LOGGER.error("Not found httpMethod string [{}]", httpMethodString);
		return null;
	}
}