package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum StaticTable {
	
	SCHEDULER ("scheduler");
	
	private final static Logger LOGGER = LoggerFactory.getLogger(StaticTable.class.getName());
	
	private final String name;

	StaticTable(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public static StaticTable getStaticTable(String staticTableString){
		if (Util.isEmpty(staticTableString)) {
			LOGGER.error("StaticTable can not be empty");
			return null;
		}
		
		for (StaticTable staticTable: StaticTable.values()){
			if (staticTable.getName().equals(staticTableString)){
				return staticTable;
			}
		}
		
		LOGGER.error("Not found [{}] string [{}]", StaticTable.class.getSimpleName(), staticTableString);
		return null;
	}
	
}
