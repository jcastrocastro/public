package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum StaticCollection {

	TOKEN ("token"),
	MAX_ORDER_BY_DAY("maxOrdersByDay");
	
	private final static Logger LOGGER = LoggerFactory.getLogger(StaticCollection.class.getName());
	
	private String name;

	StaticCollection (final String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public static StaticCollection getStaticCollection(final String staticCollectionString){
		if (Util.isEmpty(staticCollectionString)) {
			LOGGER.error("StaticCollectionString can not be empty");
			return null;
		}
		
		for (final StaticCollection staticCollection: StaticCollection.values()){
			if (staticCollection.getName().equals(staticCollectionString)){
				return staticCollection;
			}
		}
		
		LOGGER.error("Not found direction string [{}]", staticCollectionString);
		return null;
	}
}
