package market.api.enumtype;

import market.api.common.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum Currency {

	EUR ("EUR"),
	BTC ("BTC");
	
	private final static Logger LOGGER = LoggerFactory.getLogger(Currency.class.getName());
	
	private final String name;
	
	Currency(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public static Currency getCurrency(String currencyString){
		if (Util.isEmpty(currencyString)) {
			LOGGER.error("currency can not be empty");
			return null;
		}
		
		for (Currency currency: Currency.values()){
			if (currency.getName().equals(currencyString)){
				return currency;
			}
		}
		
		LOGGER.error("Not found currency string [{}]", currencyString);
		return null;
	}
	
}
