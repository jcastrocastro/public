package market.api.enumtype;


public enum TypeOrder {

	LIMIT_ORDER ("LimitOrder");

	private final String name;

	TypeOrder(final String name){
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}
