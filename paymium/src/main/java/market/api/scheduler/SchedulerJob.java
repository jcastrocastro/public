package market.api.scheduler;

import java.util.Date;

import market.api.application.configuration.Configuration.Engine.JobPlan.Params;
import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.exception.ServiceException;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerJob {

	private final static Logger LOGGER = LoggerFactory.getLogger(SchedulerJob.class.getName());

	/**
	 * Job from the start date to the end date in range
	 *
	 * @param jobClass
	 * @param startTime Start time
	 * @param endTime End time.  If null, the end time is indefinite.
	 * @param intervalMinutes Interval in minutes
	 * @throws ServiceException Error scheduling job
	 */
	public void startJobMinutes (final Class<? extends Job> jobClass, final Date startTime, final Date endTime, final int intervalMinutes, final Params params) throws ServiceException {

		this.commonInRange (jobClass, startTime, endTime, intervalMinutes*Constants.SEC_IN_ONE_MINUTE, params);

		LOGGER.info("Finish planning job [{}]", jobClass.getSimpleName());
	}

	/**
	 * Job from the start date to the end date in range
	 *
	 * @param jobClass
	 * @param startTime Start time. if it's null, will take now how execution start time.
	 * @param endTime End time.  If null, the end time is indefinite.
	 * @param intervalInSeconds Interval in secs
	 * @param params Parameters
	 * @throws ServiceException Error scheduling job
	 */
	public void startJobSeconds(final Class<? extends Job> jobClass, final Date startTime, final Date endTime, final int intervalInSeconds, final Params params) throws ServiceException {

		this.commonInRange (jobClass, startTime, endTime, intervalInSeconds, params);

		LOGGER.info("Finish planning job [{}]", jobClass.getSimpleName());
	}

	/**
	 *
	 * @param jobClass
	 * @param startTime Start time. If null then starts now
	 * @param endTime End time. Null if infinite
	 * @param intervalInSeconds
	 * @param params
	 * @throws ServiceException
	 */
	private void commonInRange(final Class<? extends Job> jobClass, Date startTime, final Date endTime, final int intervalInSeconds, final Params params) throws ServiceException {

		if (intervalInSeconds<0) {
			LOGGER.error("Interval [{}] has to be bigger than 0", intervalInSeconds);
		}

		if (startTime == null) {
			startTime = TimeUtil.getNow();
		}

		LOGGER.info("Start planning job [{}], start time [{}], end time [{}] in interval [{}] secs or minutes [{}]",
				new Object[]{ jobClass.getSimpleName(),
							  startTime,
							  this.replaceInfinitum(endTime),
							  intervalInSeconds,
							  Util.truncateDoubleDecimal((double)intervalInSeconds/Constants.SEC_IN_ONE_MINUTE, Constants.STANDART_CUT_DECIMALS)});

		// define the job and tie it to the class
		final JobDetail job = JobBuilder.newJob(jobClass).
										withIdentity(jobClass.getSimpleName()).
										build();

		// Trigger the job
		final Trigger trigger = TriggerBuilder.newTrigger()
										.withSchedule(
												SimpleScheduleBuilder.simpleSchedule()
												.withIntervalInSeconds(intervalInSeconds)
												.repeatForever())
										.startAt(startTime)
										.endAt(endTime)
										.build();

		try {
			if (params!=null && !Util.isEmpty(params.getParam())) {
				final JobDataMap jobDataMap = job.getJobDataMap();
				for (int cnt=0; cnt<params.getParam().size(); cnt++) {
					jobDataMap.put(Constants.PARAM_QUARTZ_KEY + cnt, params.getParam().get(cnt));
				}
			}

			// Tell quartz to schedule the job using our trigger
			SchedulerPlanner.getInstance().getScheduler().scheduleJob(job, trigger);
		} catch (final SchedulerException e) {
			LOGGER.error("Error schedule job [{}]", jobClass.getName());
			throw new ServiceException("Error scheduling job");
		}
	}

	private String replaceInfinitum(final Date endTime) {
		String retValue = null;
		if (endTime == null) {
			retValue = Constants.INFINITUM_SENTENCE;
		}
		return retValue;
	}

}
