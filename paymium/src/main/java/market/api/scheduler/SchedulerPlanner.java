package market.api.scheduler;

import java.util.Properties;

import market.api.application.ApplicationConf;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerPlanner {

	private final static Logger LOGGER = LoggerFactory.getLogger(SchedulerPlanner.class.getName());

	private Scheduler scheduler;

    // Private constructor. Prevents instantiation from other classes.
    private SchedulerPlanner(){}

    public Scheduler getScheduler() {
		return this.scheduler;
	}

	/**
     * Initializes singleton.
     *
     * {@link SingletonHolder} is loaded on the first execution of {@link Singleton#getInstance()} or the first access to
     * {@link SingletonHolder#INSTANCE}, not before.
     */
    private static class SingletonHolder {
            private static final SchedulerPlanner INSTANCE = new SchedulerPlanner();
    }

    public static SchedulerPlanner getInstance() {
            return SingletonHolder.INSTANCE;
    }

    public void start () throws SchedulerException {
    	if (this.scheduler == null) {
    		this.scheduler = new StdSchedulerFactory(this.getQuartzProperties()).getScheduler();
    	}
		this.scheduler.start();
    }

	private Properties getQuartzProperties() {
		final java.util.Properties prop = new java.util.Properties();
		prop.put("org.quartz.scheduler.instanceName", "Quartz_SwapManager");
		prop.put("org.quartz.threadPool.threadCount", ApplicationConf.getApplicationConf().getNumberThreads());
		prop.put("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
		prop.put("org.quartz.scheduler.skipUpdateCheck", Boolean.TRUE.toString());
		return prop;
	}

	/**
	 * Is alive
	 * @return true is alive, otherwise false
	 * @throws SchedulerException Error while checks is started
	 */
	public boolean isAlive() throws SchedulerException {
		if (this.scheduler == null){
			LOGGER.debug("The scheduler is null. I may not be initialized.");
			return false;
		}

		return !this.scheduler.isShutdown() && !this.scheduler.isInStandbyMode();
	}

	/**
	 *
	 * @throws SchedulerException Error checking is alive or standby the scheduler
	 */
	public void standBy () throws SchedulerException {
		LOGGER.info("StandBy scheduler.");

		if (this.isAlive()){
			this.scheduler.standby();
		}
	}

	public void shutdown() throws SchedulerException {
		this.scheduler.shutdown();
	}
}
