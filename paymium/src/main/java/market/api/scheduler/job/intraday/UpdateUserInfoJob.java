package market.api.scheduler.job.intraday;

import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;
import market.api.scheduler.engine.interfase.IEngine;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateUserInfoJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(UpdateUserInfoJob.class.getName());


	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		// Get user info from platform
		final String userDoc = getUserInfoFromPlatform();

		// Save document user info from platform
		ConnectionDao.getNewUserInfo().saveOrUpdate(userDoc);
	}


	private static String getUserInfoFromPlatform() throws JobExecutionException {
		// Gets document user info
		String userInfoJson = null;
		try {
			// Get user info
			userInfoJson = PrivatePaymiumApi.getInstance().getUserInfo();
		} catch (final ServiceException e) {
			LOGGER.error("Error get user info for selling", e);
			throw new JobExecutionException(e);
		}

		LOGGER.info("Last retrieved info of user account {}", userInfoJson);

		return userInfoJson;
	}


	@Override
	public String getDescription() {
		return "Update User info job from platform into DB.";
	}


	@Override
	public String getEngineName() {
		return UpdateUserInfoJob.class.getSimpleName();
	}

}
