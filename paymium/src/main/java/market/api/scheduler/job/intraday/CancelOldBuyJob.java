package market.api.scheduler.job.intraday;

import java.util.Date;
import java.util.List;

import market.api.application.Application;
import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.TradeOrder;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;
import market.api.scheduler.engine.interfase.IEngine;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Cancel the alive buy operations in the platform after spend some time
 *
 * @author raul
 *
 */
public class CancelOldBuyJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(CancelOldBuyJob.class.getName());

	private int minBuyPriceCancelAllowed;

	private int maxBuyTime;

	public int getMinBuyPriceCancelAllowed() {
		return this.minBuyPriceCancelAllowed;
	}

	private void setMinBuyPriceCancelAllowed(final int minBuyPriceCancelAllowed) {
		this.minBuyPriceCancelAllowed = minBuyPriceCancelAllowed;
	}

	public int getMaxBuyTime() {
		return this.maxBuyTime;
	}

	private void setMaxBuyTime(final int maxBuyTime) {
		this.maxBuyTime = maxBuyTime;
	}

	@Override
	public String getDescription() {
		return "Cancel the alive buy operations in the platform after spend some time";
	}

	@Override
	public String getEngineName() {
		return CancelOldBuyJob.class.getSimpleName();
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {

		LOGGER.info("Start cancel old buy alive trades [{}]", this.getEngineName());

		try {
			this.setMaxBuyTime(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(0)));
			this.setMinBuyPriceCancelAllowed(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(1)));

			final List<Document> buyAliveTrades = ConnectionDao.getNewAliveTradesDao().getAliveTradesSort(TradeOrder.DIRECTION, Direction.BUY,
																									      TradeOrder.PRICE, this.getMinBuyPriceCancelAllowed(),
																									      TradeOrder.STATE, State.ACTIVE,
																									      new Document(TradeOrder.CREATED_AT, 1));

			LOGGER.info("Number of buySell orders trying to be deleted [{}]", buyAliveTrades.size());

			for (final Document aliveTradeDoc : buyAliveTrades) {
				final String direction = aliveTradeDoc.getString(TradeOrder.DIRECTION);
				final Document buySellDoc = ConnectionDao.getNewBuySellDao().getDocument(aliveTradeDoc);

				if (direction.equals(Direction.BUY.getName()) && buySellDoc!=null) {
					LOGGER.trace("Trying to delete trade with ID [{}] and buy price [{}], sell price [{}]",
																buySellDoc.getString(Constants.STANDART_PK_MONGODB),
																Util.getDouble(buySellDoc, Constants.BUY_PRICE),
																Util.getDouble(buySellDoc, TradeOrder.PRICE));

					final double tradedBtc = Util.getDouble(aliveTradeDoc, TradeOrder.TRADED_BTC);

					if (Util.isEquals(tradedBtc, 0.0) && !Util.isGreaterDate(this.getDate(aliveTradeDoc, TradeOrder.CREATED_AT), this.getMaxBuyTime())){
						LOGGER.debug("Document with id [{}] and creation date [{}] already pass the time limit [{}] min = [{}] hour to make a buy order. It will be canceled",
																													aliveTradeDoc.getString(Constants.STANDART_PK_MONGODB),
																													this.getDate(aliveTradeDoc, TradeOrder.CREATED_AT),
																													this.getMaxBuyTime(),
																													this.getMaxBuyTime() / 60);
						// Cancel order in platform
						PrivatePaymiumApi.getInstance().cancelOrder(aliveTradeDoc);

						// Change state of buy/sell trade
						LOGGER.info("Change buy/sell trade with price [{}] in direction [{}] with new state [{}]",
																							Util.getDouble(aliveTradeDoc.toJson(), TradeOrder.PRICE),
																							aliveTradeDoc.getString(TradeOrder.DIRECTION),
																							State.CANCELED);
						ConnectionDao.getNewBuySellDao().changeStateSellMarkAndSave(aliveTradeDoc, TradeOrder.STATE, State.CANCELED);

						// Change state to mark appl sell state
						ConnectionDao.getNewBuySellDao().changeStateSellMarkAndSave(buySellDoc, TradeOrder.STATE, State.CANCELED);

					} else {
						LOGGER.info("No more buySell to be cancelled or tradeBtc [{}] is not cero", tradedBtc);
						return;
					}
				} else {
					String currentState = "";
					if (buySellDoc!=null){
						currentState = buySellDoc.getString(TradeOrder.STATE);
					}
					LOGGER.error("It's not a buy trade [{}] or its state is not active. Current state [{}], id [{}] or buy sell doc is null [{}]",
																														aliveTradeDoc.getString(TradeOrder.DIRECTION),
																														currentState,
																														aliveTradeDoc.getString(Constants.STANDART_PK_MONGODB),
																														buySellDoc);
				}
			}
		} catch (final ServiceException sexc){
			Application.applicationError(null);
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
			//			Application.stopAndSendEmail(null);
		}
	}

	private Date getDate(final Document doc, final String keyLabel)
															throws ServiceException {
		String keyValue = doc.getString(keyLabel);

		if (keyValue.contains(Constants.POINT)){
			final int index = keyValue.indexOf(Constants.POINT);
			keyValue = new StringBuilder(keyValue.substring(0, index)).append(Constants.Z_UTC).toString();
		}

		return TimeUtil.getDateUtc(keyValue);
	}

}

