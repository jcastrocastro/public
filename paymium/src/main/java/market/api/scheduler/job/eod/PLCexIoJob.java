package market.api.scheduler.job.eod;

import java.time.LocalDateTime;
import java.util.List;

import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.PaymiumUtil;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.interfase.IEngine;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PLCexIoJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(PLCexIoJob.class.getName());

	/** Session date */
	private static LocalDateTime currentSessionDate = TimeUtil.getNowLocalDateTime();

	public static LocalDateTime getCurrentSessionDate() {
		return currentSessionDate;
	}

	public static void setCurrentSessionDate(final LocalDateTime sessionDate) {
		PLCexIoJob.currentSessionDate = sessionDate;
	}

	@Override
	public String getDescription() {
		return "P&L Cex IO strategy engine";
	}

	@Override
	public String getEngineName() {
		return PLCexIoJob.class.getSimpleName();
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {

		LOGGER.info("Execute [{}] job", this.getEngineName());

		final Document userDoc = ConnectionDao.getNewUserInfo().getUserInfo();
		if (userDoc==null){
			LOGGER.error("User doc in collection [{}] cannot be null", ConnectionDao.getNewUserInfo().getNameCollection());
			Application.stopAndSendEmail(null);
		}

		// Eur info
		double balanceEur = 0.0;
		try {
			balanceEur = Util.getDouble(userDoc.toJson(), User.BALANCE_EUR);
			final int totalInv = ApplicationConf.getApplicationConf().getTotalInv();

			//		1. Get all buy alive trades and buy/sell at the same price
			final double buyAliveTradeEur = this.calculateValueTrade(ConnectionDao.getNewAliveTradesDao().getAliveTrades(TradeOrder.DIRECTION, Direction.BUY), false);

			//		2. Get all sell alive trades and sell all of them
			final double sellAliveTradeEur = this.calculateValueTrade(ConnectionDao.getNewAliveTradesDao().getAliveTrades(TradeOrder.DIRECTION, Direction.SELL), true);

			// Saves estimated P&L
			final double totalBenefit = this.calculateBenefit(totalInv, balanceEur, buyAliveTradeEur, sellAliveTradeEur);
			LOGGER.info("Saves benefit [{}]. Balance eur[{}], buy alive trade eur [{}], sell alive trade eur [{}] in collection [{}]",
																											totalBenefit,
																											balanceEur,
																											buyAliveTradeEur,
																											sellAliveTradeEur,
																											ConnectionDao.getNewBenefitDao().getNameCollection());
			ConnectionDao.getNewBenefitDao().save(totalBenefit);

		} catch (final ServiceException e) {
			LOGGER.error("Getting balance eur from doc [{}] in collection [{}]", userDoc, ConnectionDao.getNewUserInfo().getNameCollection());
			Application.applicationError(null);
		}
	}


	private double calculateValueTrade(final List<Document> aliveDocList, final boolean includeFee) throws ServiceException {
		double retValue = 0.0;
		for (final Document document : aliveDocList) {
			retValue = retValue + this.calculateValueTrade(document, includeFee);
		}
		return retValue;
	}

	private double calculateValueTrade(final Document doc, final boolean includeFee) throws ServiceException {
		double retValue = 0.0;
		final String jsonString = doc.toJson();
		final double price = Util.getDouble(jsonString, TradeOrder.PRICE);
		final double amount = Util.getDouble(jsonString, TradeOrder.AMOUNT);

		if (includeFee){
			retValue = PaymiumUtil.calculateAmountWithFee(price*amount);
		} else {
			retValue = price*amount;
		}

		return retValue;
	}

	/**
	 *
	 * @param totalInv
	 * @param balanceEur
	 * @param buyAliveTradeEur
	 * @param sellAliveTradeEur
	 * @return
	 * @throws ServiceException Error truncate decimal
	 */
	private double calculateBenefit(final int totalInv,
									final double balanceEur,
									final double buyAliveTradeEur,
									final double sellAliveTradeEur)
																throws ServiceException {

		return Util.truncateDoubleDecimal(balanceEur + buyAliveTradeEur + sellAliveTradeEur - totalInv, Constants.SHORT_CUT_DECIMALS);
	}
}
