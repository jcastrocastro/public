package market.api.scheduler.job.eod;

import market.api.common.Constants;
import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPublicPaymiumApi;
import market.api.platformPaymium.publicall.PublicPaymiumApi;
import market.api.scheduler.engine.interfase.IEngine;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Gets the nth order already made of executed trades in the Paymium platform and update info in DB
 *
 * @author raul
 *
 */
public class UpdateExecutedTradeMarketJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(UpdateExecutedTradeMarketJob.class.getName());

	@Override
	public void execute(final JobExecutionContext context)
												throws JobExecutionException {

		LOGGER.info("Start job [{}]", UpdateExecutedTradeMarketJob.class.getSimpleName());

		// Save nth executed trades
		final int nthExecutedTrade = context.getJobDetail().getJobDataMap().getInt(new StringBuilder(Constants.PARAM_QUARTZ_KEY).append(0).toString());

		// Get connection and retrieve info from platform
		final IPublicPaymiumApi publicApi = PublicPaymiumApi.getInstance();
		String jsonObjectString;
		try {
			jsonObjectString = publicApi.getOrderMade(nthExecutedTrade);
		} catch (final ServiceException e) {
			throw new JobExecutionException(e);
		}

		// Saving info into DB
		LOGGER.info("Insert or update all alive trades in collection [{}]", ConnectionDao.getNewExecutedTradesMarketDao().getNameTableExecutedTrades());
		ConnectionDao.getNewExecutedTradesMarketDao().insertOrUpdateMany(jsonObjectString);

		LOGGER.info("Finish job [{}]", UpdateExecutedTradeMarketJob.class.getSimpleName());

	}

	@Override
	public String getDescription() {
		return "Insert or update all alive trades in collection";
	}

	@Override
	public String getEngineName() {
		return UpdateExecutedTradeMarketJob.class.getSimpleName();
	}

}
