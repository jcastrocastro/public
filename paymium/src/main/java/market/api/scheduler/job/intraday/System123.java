package market.api.scheduler.job.intraday;

import market.api.application.Application;
import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDao;
import market.api.entity.Trade;
import market.api.entity.TradeOrder;
import market.api.exception.ServiceException;
import market.api.platformPaymium.publicall.PublicPaymiumApi;
import market.api.scheduler.engine.interfase.IEngine;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks for created tickers sort by order from new to old ones.
 *
 * >
 * db.ticker.find({}, {"created_at":1, "price":1}).sort({"at":-1})
 *
 * @author raul
 *
 */
public class System123 implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(System123.class.getName());

	@Override
	public String getDescription() {
		return "Semi-automatic implementation of 123 system";
	}

	@Override
	public String getEngineName() {
		return System123.class.getSimpleName();
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {
		// XXX
		// It doesn't save the time of the ticker
		try {
			// Save ticker

			final String jsonTickerString = Document.parse(PublicPaymiumApi.getInstance().getTicker()).
															append(TradeOrder.CREATED_AT, TimeUtil.getUtcTimeNow()).
															append(Trade.CREATED_AT_INT, TimeUtil.getTimeNow()).toJson();

			ConnectionDao.getNewTickerDao().save(jsonTickerString);
		} catch (final ServiceException sexc){
			Application.applicationError(null);
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
			Application.stopAndSendEmail(null);
		}
	}

}
