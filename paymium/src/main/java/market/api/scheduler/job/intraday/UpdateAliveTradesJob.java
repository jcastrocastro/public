package market.api.scheduler.job.intraday;

import market.api.connection.db.ConnectionDao;
import market.api.entity.Order;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPrivatePaymiumApi;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;
import market.api.scheduler.engine.interfase.IEngine;

import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateAliveTradesJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(UpdateAliveTradesJob.class.getName());

	@Override
	public String getDescription() {
		return "Update alive trades in the system";
	}

	@Override
	public String getEngineName() {
		return UpdateAliveTradesJob.class.getSimpleName();
	}

	@Override
	public void execute(final JobExecutionContext context)
															throws JobExecutionException {

		LOGGER.info("Start updating alive trades process [{}]", this.getEngineName());

		// Update alive trades and save them
		try {
			this.updateAliveTradesAndSave();
		} catch (final ServiceException e) {
			throw new JobExecutionException();
		}

		// Get recent alive and filled trades from platform
		final IPrivatePaymiumApi privateApi = PrivatePaymiumApi.getInstance();
		String jsonFilledObjectString;
		try {
			jsonFilledObjectString = privateApi.getAllRecentOrders();
		} catch (final ServiceException e) {
			throw new JobExecutionException(e);
		}

		// Get filled trades
		final JSONArray jsonArray = new JSONArray(jsonFilledObjectString);
		final JSONArray jsonFilledArray = new JSONArray();
		for (int cnt=0; cnt<jsonArray.length(); cnt++) {
			final JSONObject jsonObject = (JSONObject)jsonArray.get(cnt);
			if (jsonObject.getString(Order.STATE).equals(State.FILLED.getName())){
				jsonFilledArray.put(jsonObject);
			}
		}

		// fill historic collection for all recent trades
		ConnectionDao.getNewAliveTradesHistDao().insertManyWithVersion(jsonArray.toString());

		// Fill collection up with filled trades (already executed trades)
		ConnectionDao.getNewFilledTradeDao().insertOrUpdateMany(jsonFilledArray.toString());

		LOGGER.info("Finish updating alive trades process [{}]", UpdateAliveTradesJob.class.getSimpleName());
	}

	public synchronized void updateAliveTradesAndSave()
													throws ServiceException{

		final IPrivatePaymiumApi privateApi = PrivatePaymiumApi.getInstance();

		// drop alive trades collection
		ConnectionDao.getNewAliveTradesDao().deleteMany();

		// Insert many in the table of alive trades
		// Get alive trades
		final JSONArray jsonAliveArray = privateApi.getAllActiveOrders2();
		LOGGER.info("Number of alive trades [{}]", jsonAliveArray.length());
		ConnectionDao.getNewAliveTradesDao().insertMany(jsonAliveArray.toString());
	}

}
