package market.api.scheduler.job.eod;

import java.time.LocalDateTime;

import market.api.common.TimeUtil;
import market.api.connection.db.ConnectionDao;
import market.api.enumtype.Direction;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.interfase.IEngine;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * EOD job.
 * Task to do:
 * 		Updates PLMark collection everyday
 *
 * 		Resets "executed": 0, "remained":86400, "maximum":86400
 *
 * 		db.ordersByDay.insert( {"_id":"1", "executed": 0, "remained":86400, "maximum":86400,  "lastModified": new Date() } )
 * @author raul
 *
 */
public class PLJob implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(PLJob.class.getName());

	/** Session date */
	private static LocalDateTime currentSessionDate = TimeUtil.getNowLocalDateTime();

	public static LocalDateTime getCurrentSessionDate() {
		return currentSessionDate;
	}

	public static void setCurrentSessionDate(final LocalDateTime sessionDate) {
		PLJob.currentSessionDate = sessionDate;
	}

	@Override
	public String getDescription() {
		return "P&L engine";
	}

	@Override
	public String getEngineName() {
		return PLJob.class.getSimpleName();
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {

		LOGGER.info("Execute [{}] job", this.getEngineName());

		// Reset number order counter
		final LocalDateTime nowLocalDateTime = TimeUtil.getNowLocalDateTime();
		if (!TimeUtil.isEqual(getCurrentSessionDate(), nowLocalDateTime)) {
			ConnectionDao.getNewOrderCounterDao().reset();
			setCurrentSessionDate(nowLocalDateTime);
		}

		double totalCostSell = 0.0;
		try {
			totalCostSell = this.getTotalCost(Direction.SELL);
		} catch (final ServiceException e) {
			LOGGER.error("Can not calculate the sell benefits");
			return;
		}

		double totalCostBuy = 0.0;
		try {
			totalCostBuy = this.getTotalCost(Direction.BUY);
		} catch (final ServiceException e) {
			LOGGER.error("Can not calculate the buy benefits");
			return;
		}

		// calculate P&L
		ConnectionDao.getNewBenefitDao().save(totalCostSell - totalCostBuy);
	}

	/**
	 * Gets total cost from the buySell collection using the marked documents
	 *
	 * @param direction
	 * @return
	 * @throws ServiceException Wrong number of decimals to truncate
	 */
	private double getTotalCost(final Direction direction) throws ServiceException {
		return ConnectionDao.getNewBuySellDao().getCostMarkedTrades(direction);
	}

}
