package market.api.scheduler.engine;

import java.util.List;

import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.PaymiumUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.interfase.AEngineSell;
import market.api.scheduler.job.intraday.UpdateUserInfoJob;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymiumToCexSellEngineJob extends AEngineSell{

	private final static Logger LOGGER = LoggerFactory.getLogger(PaymiumToCexSellEngineJob.class.getName());

	private static final UpdateUserInfoJob updateUserInfoJob = new UpdateUserInfoJob();

	private int maxNumOrders;

	private double limitSell;

	private double amountBtcMakeSellOrder;

	private int maxTimeUpdateUser;

	/**
	 * Gets amount btc make sell order
	 *
	 * @param totalBalanceBtc Total balance Btc in account
	 * @param mainPlatform Main platform is null
	 * @return amount btc to sell
	 * @throws ServiceException
	 */
	private double getAmountBtcMakeSellOrder(final double totalBalanceBtc, final String mainPlatform)
																								throws ServiceException {
		if (Util.isEmpty(mainPlatform)){
			LOGGER.error("Main platform cannot be null");
			throw new ServiceException("Main platform cannot be null");
		}

		double retValue = this.getAmountBtcMakeSellOrder();

		final double platformAccuracy = this.getPlatformAccuracy(mainPlatform);
		if (Math.abs(retValue - totalBalanceBtc) < platformAccuracy){ // Compare until third decimal
			retValue = totalBalanceBtc;
		}

		return retValue;
	}

	private double getPlatformAccuracy(final String platformName) {
		double retValue = Constants.PAYMIUM_ACCURACY;
		if (Util.isEmpty(platformName)){
			LOGGER.error("The the platform name is null. Taking [{}] by default", platformName, Constants.PAYMIUM_ACCURACY);
		} else if (ApplicationConf.isPaymiumMainPlatform()) {
			retValue = Constants.PAYMIUM_ACCURACY;
		} else {
			LOGGER.error("Error the platform name [{}] should be a well known one. Taking [{}] by default", platformName, Constants.PAYMIUM_ACCURACY);
		}

		return retValue;
	}

	private double getAmountBtcMakeSellOrder() {
		return this.amountBtcMakeSellOrder;
	}

	public void setAmountBtcMakeSellOrder(final double amountBtcMakeSellOrder) {
		this.amountBtcMakeSellOrder = amountBtcMakeSellOrder;
	}

	public int getMaxNumOrders() {
		return this.maxNumOrders;
	}

	private void setMaxNumOrders(final int maxNumOrders) {
		this.maxNumOrders = maxNumOrders;
	}

	public double getLimitSell() {
		return this.limitSell;
	}

	private void setLimitSell(final double limitSell) {
		this.limitSell = limitSell;
	}

	@SuppressWarnings("unused")
	private int getMaxTimeUpdateUser() {
		return this.maxTimeUpdateUser;
	}

	public void setMaxTimeUpdateUser(final int maxTimeUpdateUser) {
		this.maxTimeUpdateUser = maxTimeUpdateUser;
	}

	@Override
	public String getDescription() {
		return "Sell Engine using the info from cex to make order through Paymium platform";
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {

		try {
			// Read parameters from the configuration xml of the engine
			this.setAmountBtcMakeSellOrder(PaymiumUtil.calculateAmountWithFee(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(0)), Constants.STANDART_CUT_DECIMALS));
			this.setLimitSell(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(1)));
			this.setMaxNumOrders(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(2)));
			this.setMaxTimeUpdateUser(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(3)));

			long currentNumberSellOrders;
			if (this.getMaxNumOrders() != 0 && (currentNumberSellOrders = this.getNumberOrders(Direction.SELL, State.MARK_APPL_SELL_STATE)) > this.getMaxNumOrders()){
				LOGGER.info("You already passed the limit of selling orders [{}], total number [{}]. Please increases it or non limited it",
						this.getMaxNumOrders(),
						currentNumberSellOrders);
				// Stop Application
				Application.stopAndSendEmail();
			}

			LOGGER.info("Start executing engine [{}] with limit sell [{}]", new Object[]{ this.getEngineName() , this.getLimitSell()});

			final List<Document> processingNonMarkedSellDocList = ConnectionDao.getNewBuySellDao().getSortDocumentList(this.getMarkApplSellState(), // mark_appl_sell_state
					Direction.SELL,
					new Document(TradeOrder.PRICE, -1)); // Increase sell price
			if (Util.isEmpty(processingNonMarkedSellDocList)){
				LOGGER.info("Sorry next time, please. The buy orders pool is empty. Cannot make a sell order.");
			} else {
				this.processNonMarkedSellDocList(processingNonMarkedSellDocList);
			}

		} catch (final ServiceException sexc){
			Application.applicationError(null);
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
			Application.stopAndSendEmail(null);
		}

		LOGGER.info("Finish Selling");
	}

	/**
	 *
	 * @param pendingExecutionSellDocList Pending execution sell documents
	 * @throws ServiceException  Error the jsonString key value in the document is not a number
	 * @return Sell document or null if the user doc in database doesn't exist
	 */
	private String processNonMarkedSellDocList(final List<Document> pendingExecutionSellDocList)
																							throws ServiceException {
		String sellDocString = null;

		for (final Document pendingMarkSellDoc : pendingExecutionSellDocList) {

			final double priceToSell = this.calculatePrice(pendingMarkSellDoc, 0);

			if (pendingMarkSellDoc.getString(TradeOrder.DIRECTION).equals(Direction.SELL.getName())) {
				// Get platform name
				final String mainPlatform = ApplicationConf.getApplicationConf().getMainPlatform();

				// Always update user info and get balance btc
				final Document userAccountDoc = this.retrievedInfoUserAndSave(null, 0, updateUserInfoJob);
				if (userAccountDoc == null) {
					LOGGER.error("The user doc cannot be null");
				} else {
					this.setAmountBtcMakeSellOrder(this.getAmountBuyOrderIfCero(this.getAmountBtcMakeSellOrder(), Util.getDouble(pendingMarkSellDoc, TradeOrder.AMOUNT)));
					final double totalBalanceBtc = Util.getDouble(userAccountDoc.toJson(), User.BALANCE_BTC);

					// Check if can sell btc
					if (this.canSell(userAccountDoc,
							this.getAmountBtcMakeSellOrder(totalBalanceBtc, mainPlatform),
							priceToSell)){

						// Make the real selling and save the platform info selling in db
						sellDocString = this.sell(priceToSell,
												  0, // don't cut decimals
												  totalBalanceBtc);

						// Move mark to historic
						Util.moveMarkToHist(pendingMarkSellDoc, ConnectionDao.getNewBuySellDao(), ConnectionDao.getNewBuySellHistDao());

					} else {
						LOGGER.info("Non enough [{}] btc in the account to make the sell order with price [{}] and amount btc [{}]",
																			new Object[]{totalBalanceBtc,
																						 priceToSell,
																						 this.getAmountBtcMakeSellOrder(totalBalanceBtc, mainPlatform)});
					}
				}
			} else if (pendingMarkSellDoc.getString(TradeOrder.DIRECTION).equals(Direction.BUY)){
				LOGGER.error("Below query should not retrieve buy orders uuid [{}] from the collection [{}]",
																	pendingMarkSellDoc.get(ConnectionDao.getNewBuySellDao().getCurrentPk()),
																	pendingMarkSellDoc.get(ConnectionDao.getNewBuySellDao().getNameCollection()));
			} else {
				LOGGER.error("Unexpected error getting order _id [{}] from the collection [{}]",
																			pendingMarkSellDoc.get(ConnectionDao.getNewBuySellDao().getCurrentPk()),
																			pendingMarkSellDoc.get(ConnectionDao.getNewBuySellDao().getNameCollection()));
			}
		}

		return sellDocString;
	}

	private double getAmountBuyOrderIfCero(final double amountSell, final double amountBuy) throws ServiceException {
		double retValue = amountSell;
		if (Util.isEquals(amountSell, 0.0)){
			retValue = PaymiumUtil.calculateAmountWithFee(amountBuy, Constants.STANDART_CUT_DECIMALS);
		}

		return retValue;
	}

	@Override
	public boolean canSell(final Document userAccountDoc, final double amountBtcSellOrder, final double priceMakeSellOrder)
																										throws ServiceException {
		boolean retValue = false;

		final double balanceBtc = Util.getDouble(userAccountDoc.toJson(), User.BALANCE_BTC);

		if (this.hasPassedLimit(amountBtcSellOrder, balanceBtc)){
			LOGGER.debug("Can not sell btc. Max Investment Eur [{}]. Amount btc sell order [{}], balanceBtc [{}]. Not btc to sell or the price is too high. Json User {}",
					new Object[]{ApplicationConf.getApplicationConf().getTotalInv(), amountBtcSellOrder, balanceBtc, userAccountDoc.toJson()});
		} else if (Util.isLowerOrEqual(priceMakeSellOrder, this.getLimitSell())){
			LOGGER.info("Can't make the sell order because the price [{}] already catch the bottom limit up [{}]", priceMakeSellOrder, this.getLimitSell());
		} else {
			retValue = true;
		}

		return retValue;
	}

	@Override
	public String sell(final double priceMakeSellOrder, final int decimalCut, final double totalBalanceBtc) throws ServiceException {

		LOGGER.debug("Start trying to make a sell order with price [{}]", priceMakeSellOrder);

		final double amountMakeSellOrder = this.getAmountBtcMakeSellOrder(totalBalanceBtc, ApplicationConf.getApplicationConf().getMainPlatform());
		final String jsonSellString = this.makeRealSellOrder(priceMakeSellOrder, amountMakeSellOrder);

		LOGGER.debug("Finish trying to make a sell order");

		return jsonSellString;
	}


	@Override
	public void sell(final int nth,
					 final int decimalCut,
					 final double sellAmount,
					 final double minBefenitAllowed,
					 final double lockedBtc)
							 				throws ServiceException {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean hasPassedLimit(final double amountBtcSellOrder, final double balanceBtc) {
		return  Util.isLower(balanceBtc, amountBtcSellOrder)
				||
				Util.isLowerOrEqual(amountBtcSellOrder, 0.0);
	}


	@Override
	public boolean existOrderValue(final String priceKey, final double priceValue) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public double calculatePrice(final Document sellDoc, final int decimalCut)
																	throws ServiceException {
		return Util.getDouble(sellDoc.toJson(), TradeOrder.PRICE);
	}

	@Override
	public String getEngineName() {
		return PaymiumToCexSellEngineJob.class.getSimpleName();
	}

	@Override
	public boolean existOrderValue(final String priceKey,
								   final double priceValue,
								   final State state) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public double getSpread() {
		// Don't use it
		return 0;
	}

	@Override
	public boolean canSell(final Document userAccountDoc, final double amountBtcSellOrder)
																	throws ServiceException {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final int repeationBuyTimes, final double priceToBuy, final State active, final Direction direction) {
		throw new RuntimeException("NON USED");
	}

}
