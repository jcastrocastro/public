package market.api.scheduler.engine;

import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.Ticker;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.platformCexIo.publicCall.PublicCexIoApi;
import market.api.platformPaymium.publicall.PublicPaymiumApi;
import market.api.scheduler.engine.interfase.AEngineBuy;
import market.api.scheduler.job.intraday.UpdateAliveTradesJob;
import market.api.scheduler.job.intraday.UpdateUserInfoJob;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymiumToCexBuyEngineJob extends AEngineBuy{

	private final static Logger LOGGER = LoggerFactory.getLogger(PaymiumToCexBuyEngineJob.class.getName());

	private double amountBtcBuyOrder;

	private int maxTimeUpdateUser;

	private double spread;

	private int buyNumberDecimals;

	private int sellNumberDecimals;

	private double limitBuy;

	private int maxNumOrdersSell;

	private double minAllowedBenefit;

	private int repeationBuySell;

	private int maxNumberAllowedRepetion;

	private double spreadToSell;

	private int maxNumberAliveTrades;

	private double maxRiskSupported;

	private boolean isInvertPrice;

	private static final UpdateAliveTradesJob updateAliveTradesJob = new UpdateAliveTradesJob();

	private static final UpdateUserInfoJob updateUserInfoJob = new UpdateUserInfoJob();

	private double getMaxRiskSupported() {
		return this.maxRiskSupported;
	}

	private void setMaxRiskSupported(final double percent) {
		this.maxRiskSupported = percent;
	}

	public int getMaxNumberAliveTrades() {
		return this.maxNumberAliveTrades;
	}

	public void setMaxNumberAliveTrades(final int maxNumberAliveTrades) {
		this.maxNumberAliveTrades = maxNumberAliveTrades;
	}

	public UpdateAliveTradesJob getUpdateAliveTradesJob() {
		return updateAliveTradesJob;
	}

	public double getAmountBtcBuyOrder() {
		return this.amountBtcBuyOrder;
	}

	public void setAmountBtcBuyOrder(final double amountBtcBuyOrder) {
		this.amountBtcBuyOrder = amountBtcBuyOrder;
	}

	public int getMaxTimeUpdateUser() {
		return this.maxTimeUpdateUser;
	}

	private void setMaxTimeUpdateUser(final int maxTimeUpdateUser) {
		this.maxTimeUpdateUser = maxTimeUpdateUser;
	}

	/**
	 * Spread to buy
	 */
	@Override
	public double getSpread() {
		return this.spread;
	}

	private void setSpread(final double spread) {
		this.spread = spread;
	}

	/**
	 * Spread to sell
	 */
	public double getSpreadToSell() {
		return this.spreadToSell;
	}

	private void setSpreadToSell(final double spreadToSell) {
		this.spreadToSell = spreadToSell;
	}

	public int getBuyNumberDecimals() {
		return this.buyNumberDecimals;
	}

	private void setBuyNumberDecimals(final int buyNumberDecimals) {
		this.buyNumberDecimals = buyNumberDecimals;
	}

	public int getSellNumberDecimals() {
		return this.sellNumberDecimals;
	}

	private void setSellNumberDecimals(final int sellNumberDecimals) {
		this.sellNumberDecimals = sellNumberDecimals;
	}

	@Override
	public double getLimitBuy() {
		return this.limitBuy;
	}

	private void setLimitBuy(final double limitBuy) {
		this.limitBuy = limitBuy;
	}

	public int getMaxNumOrdersSell() {
		return this.maxNumOrdersSell;
	}

	private void setMaxNumOrdersSell(final int maxNumOrdersSell) {
		this.maxNumOrdersSell = maxNumOrdersSell;
	}

	public double getMinAllowedBenefit() {
		return this.minAllowedBenefit;
	}

	private void setMinAllowedBenefit(final double minAllowedBenefit) {
		this.minAllowedBenefit = minAllowedBenefit;
	}

	public int getMaxNumberAllowedRepetion() {
		return this.maxNumberAllowedRepetion;
	}

	public void setMaxNumberAllowedRepetion(final int maxNumberAllowedRepetion) {
		this.maxNumberAllowedRepetion = maxNumberAllowedRepetion;
	}

	public int getRepeationBuySell() {
		return this.repeationBuySell;
	}

	private void setRepeationBuySell(final int repeationBuySell) {
		this.repeationBuySell = repeationBuySell;
	}

	public boolean isInvertPrice() {
		return this.isInvertPrice;
	}

	public void setInvertPrice(final boolean isInvertPrice) {
		this.isInvertPrice = isInvertPrice;
	}

	@Override
	public String getDescription() {
		return "Buy Engine using the info from cex to make order through Paymium platform";
	}

	@Override
	public void execute(final JobExecutionContext context)
													throws JobExecutionException {
		try {
			// Read parameters from the configuration xml of the engine
			this.setAmountBtcBuyOrder(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(0)));
			this.setMaxTimeUpdateUser(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(1)));
			this.setBuyNumberDecimals(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(2)));
			this.setSellNumberDecimals(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(3)));
			this.setSpread(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(4))); // spread to buy
			this.setSpreadToSell(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(5))); // spread to buy
			this.setLimitBuy(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(6)));
			this.setMaxNumOrdersSell(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(7)));
			this.setMinAllowedBenefit(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(8)));
			this.setMaxNumberAllowedRepetion(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(9)));
			this.setRepeationBuySell(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(10)));
			this.setMaxNumberAliveTrades(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(11)));
			this.setMaxRiskSupported(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(12)));
			this.setInvertPrice(context.getJobDetail().getJobDataMap().getBoolean(Util.getParamsName(13)));

			LOGGER.info("Start executing engine [{}] with Spread [{}], limit buy [{}] and max time update user info [{}] min",
					new Object[]{this.getEngineName(), this.getSpread(), this.getLimitBuy(), this.maxTimeUpdateUser});

			// Check max number allowed repetition
			if (this.getRepeationBuySell() > this.getMaxNumberAllowedRepetion()){
				LOGGER.warn("You can not make that much of repetition [{}]. It is above the allowed maximum number of repetition [{}]",
																														this.getRepeationBuySell(),
																														this.getMaxNumberAllowedRepetion());
				Application.stopAndSendEmail();
			}

			// Check max number allowed alive trades
			final long currentNumberAliveTrades = ConnectionDao.getNewAliveTradesDao().count();
			if (this.getMaxNumberAliveTrades() != 0 && currentNumberAliveTrades > this.getMaxNumberAliveTrades()){
				LOGGER.warn("You already passed the max number [{}] of permited alive trades. Current number alive trades [{}] in collection [{}] ",
																														this.getRepeationBuySell(),
																														currentNumberAliveTrades,
																														ConnectionDao.getNewAliveTradesDao().getNameTableAliveTrades());
				// Finish buying
				return;
			}

			// Check max number buy orders in alive trades
			long currentNumberOrders;
			if ( this.getMaxNumOrdersSell()!=0 && (currentNumberOrders = ConnectionDao.getNewAliveTradesDao().count()) >= this.getMaxNumOrdersSell()){
//		XXX		if ( this.getMaxNumOrdersSell()!=0 &&  (currentNumberOrders = ConnectionDao.getNewAliveTradesDao().count(Direction.SELL)) > this.getMaxNumOrdersSell()){
				LOGGER.warn("The number of buy orders [{}] is greater than the specified maximum [{}] sell orders in collection [{}]",
																														currentNumberOrders,
																														this.getMaxNumOrdersSell(),
																														ConnectionDao.getNewAliveTradesDao().getNameTableAliveTrades());
				return;
			}

			// Update user info in DB
			final Document userDoc = this.retrievedInfoUserAndSave(ConnectionDao.getNewUserInfo().getUserInfo(),
					   										this.getMaxTimeUpdateUser(),
					   										updateUserInfoJob);

			// Check allowed risk
			final double currentBalanceEur = Util.getDouble(ConnectionDao.getNewUserInfo().getUserInfo().toJson(), User.BALANCE_EUR);
			final double maxAllowedRiskEur = ApplicationConf.getApplicationConf().getTotalInv() * Util.roundUp(1.0 - this.getMaxRiskSupported());
			if (maxAllowedRiskEur > currentBalanceEur){
				LOGGER.info("Too much risk to make a buy/sell limit order. Current Balance eur [{}], max allowed risk eur [{}], Risk {}%",
																																currentBalanceEur,
																																maxAllowedRiskEur,
																																this.getMaxRiskSupported()*100);
				return;
			}

			final PriceBean priceBean = this.getPriceBean(this.getBuyNumberDecimals(), this.getSellNumberDecimals(), this.isInvertPrice());
			if (this.canBuy(userDoc, priceBean.getPriceToBuy(), priceBean.getPriceToSell())){
				for (int cnt=1; cnt<=this.getRepeationBuySell(); cnt++){
					// buy and save info
					final String jsonBuy = this.buy(priceBean.getPriceToBuy(),
					  	     		   		  this.getBuyNumberDecimals(),
					  	     		   		  Util.getDouble(userDoc.toJson(), User.BALANCE_EUR));


					// Save info to sell price in pair sell/buy table. This trade is used to know what price the sell engine needs to make to do the selling order
					final String currentUtcTime = TimeUtil.getUtcTimeNow();
					ConnectionDao.getNewBuySellDao().insertOrUpdate(new Document(TradeOrder.UUID, currentUtcTime).
																			     append(TradeOrder.PRICE, priceBean.getPriceToSell()).
																			     append(Constants.BUY_PRICE, priceBean.getPriceToBuy()).
																	 			 append(TradeOrder.DIRECTION, Direction.SELL.getName()).
																	 			 append(TradeOrder.STATE, this.getMarkApplSellState().getName()).
																	 			 append(TradeOrder.AMOUNT, this.getAmountBtcBuyOrder()).
																	 			 append(TradeOrder.CREATED_AT, currentUtcTime).
																	 			 append(TradeOrder.ID_BUY, Document.parse(jsonBuy).getString(TradeOrder.UUID)).
																	 			 toJson());
				}
			} else {
				LOGGER.info("Cannot buy price [{}] and sell price [{}]", priceBean.getPriceToBuy(), priceBean.getPriceToSell());
			}
		} catch (final ServiceException sexc){
			Application.applicationError(null);
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
			Application.stopAndSendEmail(null);
		}

		LOGGER.info("Finish buying");
	}



	private PriceBean getPriceBean(final int buyNumberDecimals, final int sellNumberDecimals, final boolean isInvert)
																				throws ServiceException {
		final PriceBean priceBean = new PriceBean();

		double priceToBuy = this.getPriceToBuyThroughPlatform(buyNumberDecimals);
		double priceToSell = this.getPriceToSellThroughPlatform(sellNumberDecimals);

		if (isInvert){
			final double priceTemp = priceToBuy;
			priceToBuy = priceToSell;
			priceToSell = priceTemp;
		}

		priceBean.setPriceToBuy(priceToBuy);
		priceBean.setPriceToSell(priceToSell);

		return priceBean;
	}

	/**
	 * Gets price to sell through {@link Constants#PLATFORM_CEX_IO} platform
	 * @param cutDecimals
	 * @return
	 * @throws ServiceException Error getting ticker from the platform or error processing the ticker info
	 */
	private double getPriceToSellThroughPlatform(final int cutDecimals) throws ServiceException {
		final String jsonObjectString = PublicCexIoApi.getInstance().getTicker();
		return Util.truncateDoubleDecimal(Util.getDouble(jsonObjectString, Ticker.LAST) + this.getSpreadToSell(), cutDecimals);
	}

	/**
	 * Gets price to buy based on the {@link Constants#PLATFORM_PAYMIUM} platform
	 *
	 * @param decimalCut Number of decimals to make a cut of the price
	 * @return Price to make a buy
	 *
	 * @throws ServiceException Error access to the platform or the decimal cut is not between 1-8
	 */
	private double getPriceToBuyThroughPlatform(final int decimalCut)
																throws ServiceException {
		final String jsonObjectString = PublicPaymiumApi.getInstance().getTicker();
		return this.calculatePrice(Document.parse(jsonObjectString), decimalCut);
	}

	@Override
	public boolean canBuy(final Document userInfoDoc, final double priceToBuy, final double priceToSell)
																							throws ServiceException {
		boolean retValue = false;

		// Has passed buy/sell limit
		if (this.hasPassedLimit(priceToBuy, this.getAmountBtcBuyOrder()) || this.hasPassedLimit(priceToSell, this.getAmountBtcBuyOrder())){
			LOGGER.info("The buy price [{}] or sell price [{}] or amount [{}] is off of the limit to make a buy/sell order", priceToBuy, priceToSell, this.getAmountBtcBuyOrder());
			return false;
		}

		// Update alive trades
		updateAliveTradesJob.updateAliveTradesAndSave();

		// if buy order value is greater than the allowed repeation buy/sell
		if (this.existOrderValue(this.getRepeationBuySell(), priceToBuy, State.ACTIVE, Direction.BUY)) {
			LOGGER.info("Already exists an order with direction [{}] price [{}] and state [{}] and a repeation order of [{}] time/s in collection [{}]",
																										    Direction.BUY,
																											priceToBuy,
																											State.ACTIVE,
																											this.getRepeationBuySell(),
																											ConnectionDao.getNewAliveTradesDao().getNameTableAliveTrades());
			return false;
		}

		// if sell order value is greater than the allowed repeation buy/sell
		if (this.existOrderValue(this.getRepeationBuySell(), priceToSell, State.ACTIVE, Direction.SELL)) {
			LOGGER.info("Already exists an order with direction [{}] price [{}] and state [{}] and a repeation order of [{}] time/s in collection [{}]",
																											Direction.SELL,
																											priceToSell,
																											State.ACTIVE,
																											this.getRepeationBuySell(),
																											ConnectionDao.getNewAliveTradesDao().getNameTableAliveTrades());
			return false;
		}

		// User info doc is null
		if (userInfoDoc==null){
			LOGGER.error("The user info cannot be null");
			Application.stopAndSendEmail();
		}

		double eurInAccount = 0.0;
		try {
			eurInAccount = Util.getDouble(userInfoDoc.toJson(), User.BALANCE_EUR);
		} catch (final ServiceException dexc) {
			LOGGER.error("Error getting fee or the platform fee is undefined", dexc);
			return false;
		}
		if (this.isEnoughMoneyInAccount(eurInAccount, this.getAmountBtcBuyOrder(), priceToBuy)){
			double stimatedBenefitToBuySell = -1;
			try {
				stimatedBenefitToBuySell = this.calculateBenefitAnyPlatform(this.getAmountBtcBuyOrder(), priceToBuy, priceToSell, ApplicationConf.getPlatformCostFee());
			} catch (final ServiceException exc) {
				LOGGER.error("Error getting fee or the platform fee is undefined", exc);
				return false;
			}

			if (Util.isGreaterOrEqual(stimatedBenefitToBuySell, this.getMinAllowedBenefit())){
				retValue = true;
				LOGGER.info("The stimated benefit [{}] is GOOD enough for a minimun of [{}] the buy [{}], sell [{}]",
						new Object[]{stimatedBenefitToBuySell, this.getMinAllowedBenefit(), priceToBuy, priceToSell});
			} else {
				LOGGER.info("The stimated benefit [{}] is NEGATIVE or LOWER than the minimun [{}] expected. Prices of the buy order [{}] and sell order [{}]",
												new Object[]{stimatedBenefitToBuySell, this.getMinAllowedBenefit(), priceToBuy, priceToSell});
			}
		} else {
			LOGGER.info("Non enough money to make a buy order in account. Total required amount [{}] EUR", this.getAmountBtcBuyOrder() * priceToBuy);
		}

		return retValue;
	}

	/**
	 * 	If EUR in account > AMOUNT_BTC_BUY_ORDER * priceToBuy
	 *  and
	 *	eurInAccount > 0
	 * @param eurInAccount Eur in account
	 * @param amountBtcBuyOrder amount of btc to make a buy order
	 * @param priceToBuy Price to make a buy order
	 * @return True if there is enough money, otherwise false
	 */
	private boolean isEnoughMoneyInAccount(final double eurInAccount,
										   final double amountBtcBuyOrder,
										   final double priceToBuy) {

		return Util.isGreater(eurInAccount, 0.0) && Util.isLowerOrEqual(priceToBuy*amountBtcBuyOrder, eurInAccount);
	}

	@Override
	public String buy(final double price, final int decimalCut, final double balanceEur)
			throws ServiceException {

		LOGGER.info("Try making a buy order with price [{}], amount [{}] in an account with balance [{}]. Decimal cut [{}]",
																				new Object[]{price,
																							 this.getAmountBtcBuyOrder(),
																							 balanceEur,
																							 decimalCut});


		final String jsonBuyDoc = this.makeALimitedBuyOrderAndSave(price, this.getAmountBtcBuyOrder());
		LOGGER.info("A buy order with price [{}] in an account with balance [{}], json buy [{}] was made",
																				new Object[]{price,
																							 balanceEur,
																							 jsonBuyDoc});

		return jsonBuyDoc;
	}

	@Override
	public boolean hasPassedLimit(final double price, final double amountBtcOrder) {
		return Util.isGreater(price, this.getLimitBuy()) || Util.isLowerOrEqual(price, 0.0);
	}

	@Override
	public boolean existOrderValue(final String priceKey, final double priceValue, final State state) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final int repeationBuyTimes, final double priceToBuy, final State state, final Direction direction) {
		return ConnectionDao.getNewAliveTradesDao().count(direction, priceToBuy, state) >= repeationBuyTimes;
	}

	@Override
	public boolean existOrderValue(final String priceKey, final double priceValue) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public double calculatePrice(final Document doc, final int decimalCut)
														throws ServiceException {
		return Util.truncateDoubleDecimal(Util.getDouble(doc.toJson(), Ticker.PRICE) + this.getSpread(), decimalCut);
	}

	@Override
	public String getEngineName() {
		return PaymiumToCexBuyEngineJob.class.getSimpleName();
	}

	@Override
	public String buy(final int nth, final int decimalCut, final double balanceEur)
																			throws ServiceException {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean canBuy(final Document userInfoDoc, final double amountBtcBuyOrder) {
		throw new RuntimeException("NON USED");
	}

}
