package market.api.scheduler.engine.interfase;

import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;

import org.bson.Document;

public abstract class AEngineSell extends AEngine {

//	private final static Logger LOGGER = LoggerFactory.getLogger(AEngineSell.class.getName());

	/**
	 * Can sell making an order?
	 *
	 * @param userAccountDoc user account doc
	 * @param amountBtcSellOrder Amount btc sell order
	 * @return True I can make a sell, otherwise false
	 * @throws ServiceException Error the jsonString key value in the document is not a number
	 */
	public abstract boolean canSell(final Document userAccountDoc, double amountBtcSellOrder) throws ServiceException;

	/**
	 * Can sell making an order?
	 *
	 * @param userAccountDoc user account doc
	 * @param amountBtcSellOrder Amount btc sell order
	 * @param priceToSell Price to sell
	 * @return True I can make a sell, otherwise false
	 * @throws ServiceException Error the jsonString key value in the document is not a number
	 */
	public abstract boolean canSell(Document userAccountDoc, double amountBtcSellOrder, double priceToSell) throws ServiceException;

	/**
	 * Logic of the engine to sell
	 *
	 * @param nth The order of the place
	 * @param decimalCut Decimal to cut
	 * @param sellAmount Sell amount
	 * @param minBefenitAllowed Minimum benefit allowed to be considered to sell
	 * @param lockedBtc Locked BTC
	 *
	 * @throws ServiceException
	 */
	public abstract void sell(int nth, int decimalCut, double sellAmount, double minBefenitAllowed, double lockedBtc) throws ServiceException;


	/**
	 *  Logic of the engine to sell
	 *
	 * @param priceToSell Price to sell
	 * @param decimalCut Decimal to cut
	 * @param lockedBtc Locked BTC
	 * @return json sell string
	 * @throws ServiceException Error access to platform
	 */
	public abstract String sell(double priceToSell, int decimalCut, double lockedBtc) throws ServiceException;


	/**
	 * Has passed the limit to sell? Specifies the top and bottom roof.
	 * The amount order of btc is bigger than 0
	 * <p>
	 * There is a criterion to establish limit of buying/selling
	 * @param totalEur Total eur to be checked
	 * @param balanceBtc balance btc in account
	 * @return true if passed, otherwise false
	 */
	public abstract boolean hasPassedLimit(double amountSellOrder, double balanceBtc);

	/**
	 * 1.  Real execution order in platform
	 * 2.  Change the jsonSell to associate it with a selling order in 'buySell' collection
	 * 	   Saves the info getting from the execution's answer
	 * 3.  Mark buy order in the 'filledTrade' collection
	 * 	   Save buy order with mark in the buySell collection
     *
	 * @param priceMakeSellOrder price to sell
	 * @param amountSellOrder Amount to sell order
	 * @return json String sell order
	 *
	 * @throws ServiceException Error access to platform
	 */
	protected String makeRealSellOrder(final double priceMakeSellOrder, final double amountSellOrder)
																			throws ServiceException {
		// Real execution order in platform
		final String jsonSell = PrivatePaymiumApi.getInstance().makeLimitedSellOrder(priceMakeSellOrder, amountSellOrder);

		// Change the jsonSell to associate it with a selling order in 'buySell' collection
		// Saves the info getting from the execution's answer
		ConnectionDao.getNewBuySellDao().insertOrUpdate(this.addMarkJson(jsonSell));

		return jsonSell;
	}

	private String addMarkJson(final String json) {
		final Document doc = Document.parse(json);
		this.addMarkDocument(doc);
		return doc.toJson();
	}



}
