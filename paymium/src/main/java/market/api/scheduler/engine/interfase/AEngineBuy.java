package market.api.scheduler.engine.interfase;

import market.api.connection.db.ConnectionDao;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.PrivatePaymiumApi;

import org.bson.Document;


public abstract class AEngineBuy extends AEngine {

//	private final static Logger LOGGER = LoggerFactory.getLogger(AEngineBuy.class.getName());


	/**
	 * Can buy currency
	 * <pre>
	 * Doesn't buy too much euros. Top roof
	 *
	 * At least can make one step. Bottom roof
	 * </pre>
	 * @return True if can buy, otherwise false
	 */
	public abstract boolean canBuy(final Document userInfoDoc, double amountBtcBuyOrder);

	/**
	 * Can buy currency?
	 * @param userInfoDoc
	 * @param priceToBuy
	 * @param priceToSell
	 * @return True if can buy, otherwise false
	 * @throws ServiceException Connection error
	 */
	public abstract boolean canBuy(Document userInfoDoc, double priceToBuy, double priceToSell)
																						throws ServiceException;

	/**
	 * Gets maximum price limit to make an order buy
	 * @return
	 */
	public abstract double getLimitBuy();


	/**
	 * Logic of the engine to buy
	 * <p>
	 * If a buy order with the price doesn't exist in the alive trades and the price is greater than the sell limit then buy
	 * @param nth
	 * @param decimalCut Number of decimal to be considered
	 * @return json buy doc
	 * @throws ServiceException
	 */
	public abstract String buy(int nth, int decimalCut, double balanceEur) throws ServiceException;

	/**
	 * Logic of the engine to buy
	 * <p>
	 * If a buy order with the price doesn't exist in the alive trades and the price is greater than the sell limit then buy
	 * @param price to make the buy
	 * @param decimalCut Number of decimal to be considered
	 * @throws ServiceException
	 */
	public abstract String buy(final double price, final int decimalCut, final double balanceEur) throws ServiceException;

	/**
	 * Has passed the limit to buy ? Specifies the top and bottom roof.
	 * The amount order of btc is bigger than 0
	 * <p>
	 * There is a criterion to establish limit of buying/selling
	 * @param totalEur Total eur to be checked
	 * @param amountBtcOrder amount btc to make the order
	 * @return true if passed, otherwise false
	 */
	public abstract boolean hasPassedLimit(double totalEur, double amountBtcOrder);

	/**
	 * Makes a limited buy order in the platform and insert the trade in the new buySell collection in DB
	 *
	 * @param priceMakeBuyOrder Price to make a buy order
	 * @param amountBtcBuyOrder Amount of btc to buy
	 * @return String with the json buy of the order
	 *
	 * @throws ServiceException Error calling to the platform or accessing to DB
	 */
	protected String makeALimitedBuyOrderAndSave(final double priceMakeBuyOrder, final double amountBtcBuyOrder)
																								throws ServiceException {
		// Real execution order
		final String jsonBuy = PrivatePaymiumApi.getInstance().makeLimitedBuyOrder(priceMakeBuyOrder, amountBtcBuyOrder);

		// Saves the info getting from the execution's answer.  It saves the btc and currency fee
		ConnectionDao.getNewBuySellDao().insertOneTradeAddingFeeInfo(jsonBuy);

		return jsonBuy;
	}

}
