package market.api.scheduler.engine;


import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.PriceDepth;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.interfase.AEngineBuy;
import market.api.scheduler.job.intraday.UpdateAliveTradesJob;
import market.api.scheduler.job.intraday.UpdateUserInfoJob;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleBuyEngineJob extends AEngineBuy{

	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleBuyEngineJob.class.getName());

	private double limitBuy;

	/**
	 * Spread in the ask. Granularity
	 * <p>
	 * OrderBuy - spread
	 */
	private double spread;

	/**
	 * Amount buy order
	 */
	private final double amountBtcBuyOrder = 0.1;

	/**
	 * Max number of buy order can make
	 */
	private int maxNumberOrdersBuy;

	/**
	 * Max time in minutes to consider the user info is not updated
	 */
	private int maxTimeUpdateUser;

	private int numberAsks;

	private int numberBids;

	/**
	 *  Number of decimals to consider to make a buy order
	 */
	private int numberDecimals;

	private int nthLowestBid;

	/** Update alive trades job */
	private static final UpdateAliveTradesJob updateAliveTradesJob = new UpdateAliveTradesJob();

	/** Update user info job */
	private static final UpdateUserInfoJob updateUserInfoJob = new UpdateUserInfoJob();


	public static UpdateUserInfoJob getUpdateUserinfojob() {
		return updateUserInfoJob;
	}

	public static UpdateAliveTradesJob getUpdateAliveTradesJob() {
		return updateAliveTradesJob;
	}

	public int getMaxTimeUpdateUser() {
		return this.maxTimeUpdateUser;
	}

	public void setMaxTimeUpdateUser(final int maxTimeUpdateUser) {
		this.maxTimeUpdateUser = maxTimeUpdateUser;
	}

	@Override
	public double getLimitBuy() {
		return this.limitBuy;
	}

	public void setLimitBuy(final double limitBuy) {
		this.limitBuy = limitBuy;
	}

	private double getAmountBtcBuyOrder() {
		return this.amountBtcBuyOrder;
	}

	@Override
	public double getSpread() {
		return this.spread;
	}

	public void setSpread(final double spread) {
		this.spread = spread;
	}

	public int getMaxNumberOrdersBuy() {
		return this.maxNumberOrdersBuy;
	}

	private void setMaxNumberOrdersBuy(final int maxBuy) {
		this.maxNumberOrdersBuy = maxBuy;
	}

	public int getNumberBids() {
		return this.numberBids;
	}

	private void setNumberBids(final int numberBids) {
		this.numberBids = numberBids;
	}

	public int getNumberAsks() {
		return this.numberAsks;
	}

	private void setNumberAsks(final int numberAsks) {
		this.numberAsks = numberAsks;
	}

	public int getNumberDecimals() {
		return this.numberDecimals;
	}

	private void setNumberDecimals(final int numberDecimals) {
		this.numberDecimals = numberDecimals;
	}

	private int getNthLowestBid() {
		return this.nthLowestBid;
	}

	private void setNthLowestBid(final int nthLowestBid) {
		this.nthLowestBid = nthLowestBid;
	}

	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {

		try {
			this.setMaxNumberOrdersBuy(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(7)));

			// Checks orders in the collection buySell
			if (this.getMaxNumberOrdersBuy() != 0 && this.getNumberOrders(Direction.BUY)>this.getMaxNumberOrdersBuy()){
				LOGGER.info("You already passed the limit of buy orders [{}]. Please increases it or non limited it", this.getMaxNumberOrdersBuy());
				// Stop Application
				Application.stopAndSendEmail();
			}

			this.setNumberAsks(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(0)));

			this.setNumberBids(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(1)));

			this.setNthLowestBid(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(2)));

			this.setMaxTimeUpdateUser(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(3)));

			this.setNumberDecimals(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(4)));

			this.setSpread(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(5)));

			this.setLimitBuy(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(6)));

			LOGGER.info("Start executing engine [{}] with Spread [{}], limit buy [{}] and max time user [{}]",
					new Object[]{ this.getEngineName(), this.getSpread(), this.getLimitBuy(), this.maxTimeUpdateUser});

			final Document userDoc = ConnectionDao.getNewUserInfo().getUserInfo();
			this.updateUserInfoInDB(userDoc, this.getMaxTimeUpdateUser(), getUpdateUserinfojob());

			if (this.canBuy(userDoc, this.getAmountBtcBuyOrder())){

				// Update ask and bid
				this.updateAsksBidsInfo(this.getNumberAsks(), this.getNumberBids());
				//	Buy the nth elements
				this.buy(this.getNthLowestBid(),
						 this.getNumberDecimals(),
						 Util.getDouble(userDoc.toJson(), User.BALANCE_EUR));
			}
		} catch (final ServiceException sexc){
			Application.applicationError();
		} catch (final Exception exc){
			LOGGER.error("Unexpected error");
			Application.stopAndSendEmail();
		}

		LOGGER.info("Finish buy with spread [{}]", this.getSpread());
	}

	@Override
	public String getDescription (){
		return "Simple buy Engine to buy Btc/Euro. Has limit amount to trade and get the fifth strategy to run with.";
	}


	@Override
	public String buy(final int nth, final int decimalCut, final double balanceEur) throws ServiceException {
		String buyJsonDoc = null;

		// look at for the fifth highest Bid - spread = price
		final Document bidDoc = ConnectionDao.getNewBidMarketDao().getLowestBid(nth);

		LOGGER.debug("Start trying to make a buy order [{}]", bidDoc.toJson());

		final double priceMakeBuyOrder = this.calculatePrice(bidDoc, decimalCut);

		// Update alive trades job
		try {
			getUpdateAliveTradesJob().execute(null);
		} catch (final JobExecutionException e) {
			throw new ServiceException(e);
		}

		// If price doesn't exist in the 'aliveTrades' a buy order with this price / amount
		//		db.aliveTrades.find({}, {price:1, direction:1})
		if (this.existOrderValue(TradeOrder.PRICE, priceMakeBuyOrder)) {
			LOGGER.info("The order with the price [{}] was already made. Not allow to make two buy orders with the same price", priceMakeBuyOrder);
		} else if (Util.isGreaterOrEqual(priceMakeBuyOrder, this.getLimitBuy())) {
			LOGGER.info("Can't make the order because the price [{}] already catch the roof limit up", priceMakeBuyOrder);
		} else if (Util.isGreater(this.getAmountBtcBuyOrder()*priceMakeBuyOrder, balanceEur)){
			LOGGER.info("Not enough euros in the account to make a buy order with price [{}] and amount [{}]", priceMakeBuyOrder, this.getAmountBtcBuyOrder());
		} else {
			LOGGER.info("NOT exist an order with price [{}] direction buy in collection aliveTrades. Try to execute a buy order.", priceMakeBuyOrder);

			buyJsonDoc = this.makeALimitedBuyOrderAndSave(priceMakeBuyOrder, this.getAmountBtcBuyOrder());
		}

		LOGGER.debug("Finish trying to make a buy order");

		return buyJsonDoc;
	}


	/**
	 * Calculates the price to real make an order
	 * @param doc
	 * @param decimalCut
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public double calculatePrice(final Document doc, final int decimalCut) throws ServiceException {
		return Util.truncateDoubleDecimal(doc.getDouble(PriceDepth.PRICE) - this.getSpread(), decimalCut);
	}

	@Override
	public boolean existOrderValue(final String priceKey, final double priceValue) {
		// Look for price in data base (buy collection)
		return ConnectionDao.getNewAliveTradesDao().existActiveOrder(TradeOrder.DIRECTION, Direction.BUY.getName(), priceKey, priceValue);
	}

	@Override
	public boolean canBuy(final Document userInfoDoc, final double amountBtcBuyOrder) {
		boolean retValue = false;

		final double balanceEur = userInfoDoc.getDouble(User.BALANCE_EUR);

		if (this.hasPassedLimit(balanceEur, amountBtcBuyOrder)){
			LOGGER.trace("Can not buy btc. Balance Eur [{}], Max Investment Eur [{}], amountBtcBuyOrder [{}]. User info {}",
					new Object[]{balanceEur, ApplicationConf.getApplicationConf().getTotalInv(), amountBtcBuyOrder, userInfoDoc.toJson()});
		} else {
			retValue = true;
		}

		return retValue;
	}

	@Override
	public boolean hasPassedLimit(final double balanceEur, final double amountBtcBuyOrder) {
		//	if the locked (Total amount Eur) / 2 > currentEur
		return Util.isGreater(ApplicationConf.getApplicationConf().getTotalInv()/2.0, balanceEur) // Doesn't buy too much euros. Top roof
				|| Util.isLowerOrEqual(amountBtcBuyOrder, 0.0); // The amount is Lower or Equal than 0
	}

	@Override
	public String getEngineName() {
		return SimpleBuyEngineJob.class.getSimpleName();
	}

	@Override
	public String buy(final double price, final int decimalCut, final double balanceEur)
			throws ServiceException {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean canBuy(final Document userInfoDoc, final double priceToBuy, final double priceToSell) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final String priceKey,
								   final double priceValue,
								   final State state) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final int repeationBuyTimes, final double priceToBuy, final State active, final Direction direction) {
		throw new RuntimeException("NON USED");
	}
}
