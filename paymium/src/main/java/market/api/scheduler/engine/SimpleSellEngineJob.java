package market.api.scheduler.engine;

import java.util.List;

import market.api.application.Application;
import market.api.application.ApplicationConf;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.PriceDepth;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.scheduler.engine.interfase.AEngineSell;

import org.bson.Document;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleSellEngineJob extends AEngineSell{

	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleSellEngineJob.class.getName());

	/**
	 * Spread in the bid. Granularity
	 * <p>
	 * OrderBuy - spread
	 */
	private double spread;

	private final double amountSellOrder = 0.1;

	private double limitSell;

	private int maxNumberOrdersSell;

	private double minBenefitAllowed;

	private int nthHighestAsk;

	private int decimalMakeOrder;

	private int numberBids;

	private int numberAsks;

	public double getMinBenefitAllowed() {
		return this.minBenefitAllowed;
	}

	private void setMinBenefitAllowed(final double minBenefitAllowed) {
		this.minBenefitAllowed = minBenefitAllowed;
	}

	public int getMaxNumberOrdersSell() {
		return this.maxNumberOrdersSell;
	}

	public void setMaxNumberOrdersSell(final int maxNumberOrderSell) {
		this.maxNumberOrdersSell = maxNumberOrderSell;
	}

	@Override
	public double getSpread() {
		return this.spread;
	}

	public void setSpread(final double spread) {
		this.spread = spread;
	}

	public int getNthHighestAsk() {
		return this.nthHighestAsk;
	}

	private void setNthHighestAsk(final int nthHighestAsk) {
		this.nthHighestAsk = nthHighestAsk;
	}

	public int getNumberDecimalMakeOrder() {
		return this.decimalMakeOrder;
	}

	private void setNumberDecimalMakeOrder(final int numberDecimalMakeOrder) {
		this.decimalMakeOrder = numberDecimalMakeOrder;
	}

	public int getNumberBids() {
		return this.numberBids;
	}

	private void setMaxBids(final int numberBids) {
		this.numberBids = numberBids;
	}

	public int getNumberAsks() {
		return this.numberAsks;
	}

	private void setNumberAsks(final int numberAsks) {
		this.numberAsks = numberAsks;
	}


	@Override
	public String getDescription() {
		return "Simple sell Engine to sell Btc/Euro. Has limit amount to trade and get the fifth strategy to run with.";
	}

	@Override
	public void execute(final JobExecutionContext context)
														throws JobExecutionException {

		try{
			this.setMaxNumberOrdersSell(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(6)));

			if (this.getMaxNumberOrdersSell() != 0 && this.getNumberOrders(Direction.SELL)>this.getMaxNumberOrdersSell()){
				LOGGER.info("You already passed the limit of sell orders [{}]. Please increases it or non limited it", this.getMaxNumberOrdersSell());
				// Stop Application
				Application.stopAndSendEmail();
			}

			this.setNumberAsks(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(0)));
			this.setMaxBids(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(1)));
			this.setNthHighestAsk(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(2)));
			this.setNumberDecimalMakeOrder(context.getJobDetail().getJobDataMap().getInt(Util.getParamsName(3)));
			this.setSpread(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(4)));
			this.setLimitSell(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(5)));
			this.setMinBenefitAllowed(context.getJobDetail().getJobDataMap().getDouble(Util.getParamsName(7)));

			LOGGER.info("Start executing engine [{}] with Price (spread) [{}], limit sell [{}] and min benefit allowed [{}]",
					new Object[]{this.getEngineName() , this.getSpread(), this.getLimitSell(), this.getMinBenefitAllowed()});

			final Document userAccountDoc = ConnectionDao.getNewUserInfo().getUserInfo();

			if (userAccountDoc==null || userAccountDoc.get(User.BALANCE_BTC)==null){
				LOGGER.error("Finish selling account info is null");
			} else if (this.canSell(userAccountDoc, this.getAmountSellOrder())){
				this.updateAsksBidsInfo(this.getNumberAsks(), this.getNumberBids());

				this.sell(this.getNthHighestAsk(),
						this.getNumberDecimalMakeOrder(),
						this.getAmountSellOrder(),
						this.getMinBenefitAllowed(),
						Util.getDouble(userAccountDoc.toJson(), User.LOCKED_BTC));
			}
		} catch (final ServiceException sexc){
			Application.applicationError();
		} catch (final Exception exc){
			LOGGER.error("Unexpected error");
			Application.stopAndSendEmail();
		}

		LOGGER.info("Finish selling.");
	}

	@Override
	public void sell(final int nth, final int decimalCut, final double sellAmount, final double minBefenitAllowed, final double lockedBtc)
																													throws ServiceException {
		// look at for the fifth highest Ask
		final Document askDoc = ConnectionDao.getNewAskMarketDao().getHighestAsk(nth);

		final double priceMakeSellOrder = this.calculatePrice(askDoc, decimalCut);

		LOGGER.debug("Start trying to make a sell order with price [{}]", priceMakeSellOrder);

		// If price doesn't exist in the alive trades a buy order with this price / amount
		Document buyDoc = null;

		final double topLimitInEur = ApplicationConf.getApplicationConf().getTotalInv()/2.0;

		// Dont make twice the same order
		if (this.existOrderValue(TradeOrder.PRICE, priceMakeSellOrder)) {
			LOGGER.info("The order with the price [{}] was already made. Not allow to make two sell orders with the same price", priceMakeSellOrder);
		} else if (Util.isLowerOrEqual(priceMakeSellOrder, this.getLimitSell())){
			LOGGER.info("Can't make the sell order because the price [{}] already catch the bottom limit up", priceMakeSellOrder);
		} else if (Util.isGreater(this.getLockedBtcInEur(priceMakeSellOrder, lockedBtc), topLimitInEur)){
			LOGGER.info("Can't make the sell order with amount [{}] btc because the number of locked btc [{}] with price [{}] already catch the top limit [{}] Eur",
																												new Object[]{this.getAmountSellOrder(), lockedBtc, priceMakeSellOrder, topLimitInEur});
		// Has benefit or not looking for trades in filledTrade collection
		} else if ((buyDoc = this.hasBenefit(priceMakeSellOrder, sellAmount, minBefenitAllowed))!=null) {
			LOGGER.info("Real execution of the sell order with price [{}] and amount [{}]", priceMakeSellOrder, this.getAmountSellOrder());

			this.makeRealSellOrder(priceMakeSellOrder, this.getAmountSellOrder());

			this.addMarkTrueTodDocAndSaveFilledTradeCollection(buyDoc);
		} else {
			LOGGER.info("There is not benefit to sell to this selling price [{}]", priceMakeSellOrder);
		}

		LOGGER.debug("Finish trying to make a sell order");
	}

	private double getLockedBtcInEur(final double priceMakeSellOrder, final double lockedBtc) {
		return priceMakeSellOrder * lockedBtc;
	}

	/**
	 * Calculates if there is benefit or not. It only makes the calculation and not save info in DB
	 *
	 * @param priceToSell
	 * @param sellAmount
	 * @param minBefenitAllowed
	 * @return
	 * @throws ServiceException
	 */
	private Document hasBenefit(final double priceToSell, final double sellAmount, final double minBefenitAllowed) throws ServiceException {
		final List<Document> filledBuyDoc = ConnectionDao.getNewFilledTradeDao().getNonMarkedSortedBy(Direction.BUY, -1); // It only works if the amount is the same between buy and sell orders

		if (filledBuyDoc==null) {
			LOGGER.info("Non available buy trade in collection [{}] NON benefits if the selling order has this price [{}]",
												ConnectionDao.getNewFilledTradeDao().getCollectionName(),
												priceToSell);
		} else {
			for (final Document doc : filledBuyDoc) {
				final double benefit = this.calculateBenefitPaymium(doc, priceToSell, sellAmount);
				final String jsonDoc = doc.toJson();
				if (Util.isGreater(benefit, minBefenitAllowed)) {
					LOGGER.info("There is benefit [{}] EUR for price sell [{}] with sell amount [{}], buy price [{}] and buy amount [{}]",
											new Object[]{benefit, priceToSell, sellAmount, Util.getDouble(jsonDoc, TradeOrder.PRICE), Util.getDouble(jsonDoc,TradeOrder.AMOUNT)});
					return doc;
				} else {
					LOGGER.info("Non benefit [{}] for price sell [{}] with sell amount [{}], buy price [{}] and buy amount [{}]",
													new Object[]{benefit, priceToSell, sellAmount, Util.getDouble(jsonDoc, TradeOrder.PRICE), Util.getDouble(jsonDoc, TradeOrder.AMOUNT)});
				}
			}
		}

		return null;
	}

	private double getAmountSellOrder() {
		return this.amountSellOrder;
	}

	private double getLimitSell() {
		return this.limitSell;
	}

	public void setLimitSell(final double limitSell) {
		this.limitSell = limitSell;
	}

	@Override
	public boolean canSell(final Document userAccountDoc, final double amountBtcSellOrder)
																							throws ServiceException {
		boolean retValue = false;

		final String userAccountJson = userAccountDoc.toJson();
		double balanceBtc;

		balanceBtc = Util.getDouble(userAccountJson, User.BALANCE_BTC);

		if (this.hasPassedLimit(amountBtcSellOrder, balanceBtc)){
			LOGGER.debug("Can not sell btc. Max Investment Eur [{}]. Amount btc sell order [{}], balanceBtc [{}]. Not btc to sell or the price is too high. Json User {}",
					new Object[]{ApplicationConf.getApplicationConf().getTotalInv(), amountBtcSellOrder, balanceBtc, userAccountJson});
		} else {
			retValue = true;
		}

		return retValue;
	}

	@Override
	public boolean hasPassedLimit(final double amountSellOrder, final double balanceBtc) {
		return  Util.isLower(balanceBtc, amountSellOrder)
				||
				Util.isLowerOrEqual(amountSellOrder, 0.0);
	}

	@Override
	public boolean existOrderValue(final String priceKey, final double priceValue) {
		// Look for price in data base (sell collection)
		return ConnectionDao.getNewAliveTradesDao().existActiveOrder(TradeOrder.DIRECTION, Direction.SELL.getName(), priceKey, priceValue);
	}

	@Override
	public double calculatePrice(final Document doc, final int decimalCut) throws ServiceException {
		return Util.truncateDoubleDecimal(doc.getDouble(PriceDepth.PRICE) - this.getSpread(), decimalCut);
	}

	@Override
	public String getEngineName() {
		return SimpleSellEngineJob.class.getSimpleName();
	}

	@Override
	public String sell(final double priceToSell, final int decimalCut, final double lockedBtc) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final String priceKey,
								   final double priceValue,
								   final State state) {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean canSell(final Document userAccountDoc, final double amountBtcSellOrder, final double priceToSell)
																										throws ServiceException {
		throw new RuntimeException("NON USED");
	}

	@Override
	public boolean existOrderValue(final int repeationBuyTimes, final double priceToBuy, final State active, final Direction direction) {
		throw new RuntimeException("NON USED");
	}
}
