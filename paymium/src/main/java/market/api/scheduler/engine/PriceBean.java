package market.api.scheduler.engine;

public class PriceBean {

	private double priceToBuy;

	private double priceToSell;

	public double getPriceToBuy() {
		return this.priceToBuy;
	}

	public void setPriceToBuy(final double priceToBuy) {
		this.priceToBuy = priceToBuy;
	}

	public double getPriceToSell() {
		return this.priceToSell;
	}

	public void setPriceToSell(final double priceToSell) {
		this.priceToSell = priceToSell;
	}

	@Override
	public String toString() {
		return "PriceBean [priceToBuy=" + this.priceToBuy + ", priceToSell="+ this.priceToSell + "]";
	}
}
