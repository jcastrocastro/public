package market.api.scheduler.engine.interfase;

import org.quartz.Job;

public interface IEngine extends Job {

	/**
	 * Description of the logic of the Engine
	 * @return
	 */
	String getDescription ();

	/**
	 * Gets the name of the engine
	 *
	 * @return
	 */
	String getEngineName();
}
