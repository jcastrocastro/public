package market.api.scheduler.engine.interfase;

import org.bson.Document;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.connection.db.dao.UserInfoDao;
import market.api.entity.AskBidString;
import market.api.entity.TradeOrder;
import market.api.entity.User;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPublicPaymiumApi;
import market.api.platformPaymium.publicall.PublicPaymiumApi;
import market.api.scheduler.job.intraday.UpdateUserInfoJob;

public abstract class AEngine implements IEngine{

	private final static Logger LOGGER = LoggerFactory.getLogger(AEngine.class.getName());

	/**
	 * Granularity buying/selling
	 * <pre>
	 * orderBuy - spread
	 * orderSell + spread
	 * </pre>
	 * @return
	 */
	public abstract double getSpread();

	/**
	 * Looks at if the order was already made in collection 'aliveTrades'
	 * <pre>
	 * Checks results
	 * db.aliveTrades.find({"direction":"buy"},{price: 1, direction:1})
	 * </pre>
	 * @param priceKey
	 * @param priceValue
	 * @return
	 */
	public abstract boolean existOrderValue(final String priceKey, final double priceValue);

	/**
	 * Looks at if the order was already made in collection 'buySell'
	 * <pre>
	 * Checks results
	 * db.buySell.find({"direction":"buy", "price": <priceValue>, "state": <state>},{price: 1, direction:1})
	 * </pre>
	 * @param priceKey
	 * @param priceValue
	 * @param state
	 * @return
	 */
	public abstract boolean existOrderValue(String priceKey, double priceValue, State state);

	/**
	 * Looks at if the order was already made in collection 'buySell'
	 * <pre>
	 * Checks results
	 * db.buySell.find({"direction":"buy", "price": <priceValue>, "state": <state>},{price: 1, direction:1})
	 * </pre>
	 *
	 * @param repeationBuyTimes
	 * @param priceToBuy
	 * @param active
	 * @param direction
	 * @return
	 */
	public abstract boolean existOrderValue(final int repeationBuyTimes, final double priceToBuy, final State active, final Direction direction);

	/**
	 * Calculates prices
	 * @param doc
	 * @param decimalCut
	 * @return
	 * @throws ServiceException Error if the decimal cut is not between 1-8
	 */
	public abstract double calculatePrice(Document doc, int decimalCut) throws ServiceException;

	/**
	 * Gets the sell application mark state. Only used for the application
	 * @return
	 */
	protected State getMarkApplSellState(){
		return State.MARK_APPL_SELL_STATE;
	}

	/**
	 * Checks the number of order with the given direction in the collection buySell
	 * @param direction
	 * @return
	 */
	protected long getNumberOrders(final Direction direction) {
		return ConnectionDao.getNewBuySellDao().numberOrders(direction);
	}

	/**
	 * Checks the number of order with the given direction in the collection buySell
	 * @param direction
	 * @return
	 */
	protected long getNumberOrders(final Direction direction, final State state) {
		return ConnectionDao.getNewBuySellDao().numberOrders(direction, state);
	}

	/**
	 * Gets total eur = Balance eur + locked eur
	 *
	 * @param userInfoDoc
	 * @return
	 */
	protected double getTotalEur(final Document userInfoDoc) {
		return userInfoDoc.getDouble(User.BALANCE_EUR) + userInfoDoc.getDouble(User.LOCKED_EUR);
	}

	/**
	 * Updates ask and bid in DB
	 * @param context
	 * @throws ServiceException Connection error to the public Api of the platform
	 */
	protected void updateAsksBidsInfo(final int nthAsks, final int nthBids)
																		throws ServiceException {

		LOGGER.info("Start asking ask/bid job. Num Asks [{}] and bids [{}]", nthAsks, nthBids);

		// Get connection and retrieve info from platform
		final IPublicPaymiumApi publicApi = PublicPaymiumApi.getInstance();

		// Get last nth market depth json string
		final AskBidString askBidString = publicApi.getLastNthMarketDepthJson(nthAsks, nthBids);

		// Saving info into DB
		LOGGER.info("Saving the latest {}th in ask table", nthAsks);
		ConnectionDao.getNewAskMarketDao().deleteMany();
		ConnectionDao.getNewAskMarketDao().insertMany(askBidString.getAsk());

		LOGGER.info("Saving the latest {}th in ask table", nthBids);
		ConnectionDao.getNewBidMarketDao().deleteMany();
		ConnectionDao.getNewBidMarketDao().insertMany(askBidString.getBid());

		LOGGER.info("Finish asking ask/bid job");
	}


	/**
	 * Mark buy order in the 'filledTrade' collection
	 * <p>
	 * Save buy order with mark in the buySell collection
	 *
	 * @param doc Doc to be marked and saved in filled trade collection
	 */
	protected void addMarkTrueTodDocAndSaveFilledTradeCollection(final Document doc) {
		if (doc!=null){
			this.addMarkDocument(doc);
			ConnectionDao.getNewFilledTradeDao().insertOrUpdate(doc.toJson());
		} else {
			LOGGER.error("Buy doc cannot be null");
		}
	}

	/**
	 * Mark buy order in the 'filledTrade' collection
	 * <p>
	 * Save buy order with mark in the buySell collection
	 *
	 * @param doc Doc to be marked and saved in filled trade collection
	 */
	protected void addMarkTrueTodDocAndSaveBuySellCollection(final Document doc) {
		if (doc!=null){
			this.addMarkDocument(doc);
			ConnectionDao.getNewBuySellDao().insertOrUpdate(doc.toJson());
		} else {
			LOGGER.error("Buy doc cannot be null");
		}
	}

	protected void addMarkDocument(final Document doc) {
		doc.put(TradeOrder.MARK, Boolean.TRUE.booleanValue());
	}

	/**
	 * Calculates benefit for PAYMIUM Platform
	 * @param buyDoc
	 * @param sellPrice
	 * @param sellAmount
	 * @return
	 * @throws ServiceException Error the platform fee is undefined
	 */
	protected double calculateBenefitPaymium(final Document buyDoc, final double sellPrice, final double sellAmount)
																										throws ServiceException {
		final String jsonBuyString = buyDoc.toJson();
		final double buyPrice = Util.getDouble(jsonBuyString, TradeOrder.PRICE);
		final double buyAmount = Util.getDouble(jsonBuyString, TradeOrder.AMOUNT);

		final double platformCostFee = ApplicationConf.getPlatformCostFee();
		final double sellTotal = sellPrice * sellAmount * (1 - platformCostFee);
		final double buyTotal = buyPrice * buyAmount * (1 + platformCostFee);

		return Util.truncateDoubleDecimal(sellTotal - buyTotal, Constants.STANDART_CUT_DECIMALS);
	}

	/**
	 * Calculates benefits in any platform with 8 decimals of precision
	 *
	 * @param amountBtcBuyOrder
	 * @param priceToBuy
	 * @param priceToSell
	 * @param costFee
	 * @return estimated benefit
	 * @throws ServiceException
	 */
	protected double calculateBenefitAnyPlatform(final double amountBtcBuyOrder,
							     				 final double priceToBuy,
							     				 final double priceToSell,
							     				 final double costFee)
								    		 					throws ServiceException {

		final double remainedBtc = amountBtcBuyOrder - Util.getBtcFee(amountBtcBuyOrder, costFee);
		final double tradingBuy = amountBtcBuyOrder * priceToBuy;
		final double tradingSell = remainedBtc * priceToSell - Util.getCurrencyFee(remainedBtc, priceToSell, costFee);
		final double totalFee =  Util.truncateDoubleDecimal((priceToSell - priceToBuy)*amountBtcBuyOrder - (tradingSell - tradingBuy), Constants.SHORT_CUT_DECIMALS);
		
		final double retBenefit = Util.truncateDoubleDecimal(tradingSell - tradingBuy, Constants.SHORT_CUT_DECIMALS);

		LOGGER.info("Calculate benefits.");
		LOGGER.info("Sell [{}], Buy [{}], Amount [{}], Remained Btc [{}] after buying. Total Fee [{}], Total Benefit [{}]",
																											new Object[] {priceToSell,
																					  									  priceToBuy,
																					  									  amountBtcBuyOrder,
																					  									  remainedBtc,
																					  									  totalFee,
																					  									  retBenefit});
		return retBenefit;
	}

	/**
	 * Updates user info in DB. Compares the current time with the document UserInfoDao.LAST_MODIFIED value
	 *
	 * @param userDoc User doc
	 * @param maxTimeUpdateUserInMinutes Max time to update user info in db measured in minutes.
	 * @param UpdateUserInfoJob updateUserInfoJob object
	 *
	 * @throws JobExecutionException Exception getting user info from platform
	 */
	protected void updateUserInfoInDB(final Document userDoc,
									  final int maxTimeUpdateUserInMinutes,
									  final UpdateUserInfoJob updateUserInfoJob)
											  					throws JobExecutionException {

		if (userDoc == null || !Util.isGreaterDate(userDoc.getDate(UserInfoDao.LAST_MODIFIED), maxTimeUpdateUserInMinutes)) {
			updateUserInfoJob.execute(null);

		}
	}

	/**
	 * Retrieving user info and update database if it is needed
	 * @param userInfoDoc
	 * @param maxTimeUpdateUser
	 * @param updateUserInfoJob
	 * @return
	 * @throws ServiceException Error retrieving info user
	 */
	protected Document retrievedInfoUserAndSave(final Document userInfoDoc,
										 final int maxTimeUpdateUser,
										 final UpdateUserInfoJob updateUserInfoJob)
												 							throws ServiceException {
		try {
			this.updateUserInfoInDB(userInfoDoc,
									maxTimeUpdateUser,
									updateUserInfoJob);
		} catch (final JobExecutionException e) {
			LOGGER.error("Error retrieving info user", e);
			throw new ServiceException("Error retrieving info user");
		}

		// Getting again the user info just in case
		return ConnectionDao.getNewUserInfo().getUserInfo();
	}
}

