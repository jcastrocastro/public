package market.api.scheduler;

import java.util.Date;
import java.util.List;

import market.api.application.configuration.Configuration.Engine.JobPlan;
import market.api.common.TimeUtil;
import market.api.exception.ServiceException;

import org.quartz.Job;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SchedulerJobApi {

	private final static Logger LOGGER = LoggerFactory.getLogger(SchedulerJobApi.class.getName());

	/** Scheduler to plan jobs*/
	private final SchedulerJob schedulerJob;

	public SchedulerJobApi(){
		this.schedulerJob = new SchedulerJob();
	}

	private SchedulerJob getSchedulerJob() {
		return this.schedulerJob;
	}

	/**
	 * Scheduler planner is alive?
	 * @return
	 * @throws ServiceException  Error while checks is started
	 */
	public boolean isAliveSchedulerPlanner()
											throws ServiceException {
		try {
			return SchedulerPlanner.getInstance().isAlive();
		} catch (final SchedulerException exc) {
			LOGGER.error("Error starting the scheduler", exc);
			throw new ServiceException(exc);
		}
	}

	/**
	 * Starts the scheduler planner
	 * @throws ServiceException
	 */
	public void startSchedulerPlanner() throws ServiceException {
		try {
			SchedulerPlanner.getInstance().start();
		}catch (final SchedulerException exc){
			LOGGER.error("Error starting the scheduler", exc);
			throw new ServiceException(exc);
		}
	}

	/**
	 * Stops the scheduler planer forever
	 * @throws ServiceException
	 */
	public void stopSchedulerPlaner() throws ServiceException {
		if (SchedulerPlanner.getInstance().getScheduler()!=null) {
			try {
				SchedulerPlanner.getInstance().shutdown();
			}catch (final SchedulerException exc){
				LOGGER.error("Error stopping the scheduler", exc);
				throw new ServiceException(exc);
			}
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * Configures all jobs in seconds
	 * @param jobList List of jobs
	 *
	 * @throws ServiceException Error scheduling job or job class not found
	 */
	public void configureAllJobsInSeconds(final List<JobPlan> jobList) throws ServiceException {

		for (final JobPlan jobPlan: jobList){
			Class<? extends Job> classJob;
			try {
				classJob = (Class<? extends Job>)Class.forName(jobPlan.getJobPlanClass());
			} catch (final ClassNotFoundException e) {
				LOGGER.error("Wrong getting class for class name [{}]", jobPlan.getJobPlanClass());
				throw new ServiceException(e);
			} catch (final ClassCastException cexc) {
				LOGGER.error("Can not cast [{}] to [{}]", jobPlan.getJobPlanClass(), "Job");
				throw new ServiceException(cexc);
			}

			this.getSchedulerJob().startJobSeconds(classJob,
												   this.addMinutesWithDelay(TimeUtil.getDate(jobPlan.getStartTime()), jobPlan.getDelay()),
												   TimeUtil.getDate(jobPlan.getEndTime()),
												   jobPlan.getInterval(),
												   jobPlan.getParams());
		}
	}

	private Date addMinutesWithDelay(final Date date, final int delayInMinutes) {
		Date retValue = date;

		if (date == null){
			retValue = TimeUtil.getNow();
		}

		if (delayInMinutes != 0){
			retValue = TimeUtil.addMinutes(retValue, delayInMinutes);
		}

		return retValue;
	}

	@SuppressWarnings("unchecked")
	public void configureAllJobsInMinutes(final List<JobPlan> jobList) throws ServiceException {


		for (final JobPlan jobPlan: jobList){
			Class<? extends Job> classJob;
			try {
				classJob = (Class<? extends Job>)Class.forName(jobPlan.getJobPlanClass());
			} catch (final ClassNotFoundException e) {
				LOGGER.error("Wrong getting class for class name [{}]", jobPlan.getJobPlanClass());
				throw new ServiceException(e);
			} catch (final ClassCastException cexc) {
				LOGGER.error("Can not cast [{}] to [{}]", jobPlan.getJobPlanClass(), "Job");
				throw new ServiceException(cexc);
			}
			this.getSchedulerJob().startJobMinutes(classJob,
												   TimeUtil.getDate(jobPlan.getStartTime()),
												   TimeUtil.getDate(jobPlan.getEndTime()),
												   jobPlan.getInterval(),
												   jobPlan.getParams());
		}
	}

}
