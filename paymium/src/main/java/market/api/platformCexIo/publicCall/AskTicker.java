package market.api.platformCexIo.publicCall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.Ticker;
import market.api.exception.ServiceException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AskTicker  {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTicker.class.getName());

	private static final String URLWEB = Constants.TICKER_URL_CEX;

	String getUrlWeb() {
		return URLWEB;
	}

	private JSONObject getJsonObject() throws ServiceException {
		JSONObject jsonObject = null;
		try {
			jsonObject = Conversor.getJsonObject(HttpsHelper.getPublicReaderCexIo(this.getUrlWeb()));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception", e);
			throw new ServiceException(e);
		}

		return jsonObject;
	}

	/**
	 * Gets json ticker in the CEX IO platform
	 * @return
	 * @throws ServiceException
	 */
	String getJson() throws ServiceException {
		return Util.adaptPrimaryKeyToMongoDb(this.getJsonObject().toString(), Ticker.TIMESTAMP);
	}
}
