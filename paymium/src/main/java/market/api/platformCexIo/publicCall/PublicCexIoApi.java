package market.api.platformCexIo.publicCall;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.exception.ServiceException;
import market.api.platformCexIo.IPublicCexIoApi;

public class PublicCexIoApi implements IPublicCexIoApi{


	private final AskTicker askTicker;

    private PublicCexIoApi() {
    	this.askTicker = new AskTicker();
    }

	/**
     * Initializes singleton.
     *
     * {@link SingletonHolder} is loaded on the first execution of {@link Singleton#getInstance()} or the first access to
     * {@link SingletonHolder#INSTANCE}, not before.
     */
    private static class SingletonHolder {
            private static final PublicCexIoApi INSTANCE = new PublicCexIoApi();
    }

    public static IPublicCexIoApi getInstance() {
            return SingletonHolder.INSTANCE;
    }

	public AskTicker getAskTicker() {
		return this.askTicker;
	}

	@Override
	public String getTicker() throws ServiceException {
		final long startTime = TimeUtil.startCountTime();

		final String retTicker = this.getAskTicker().getJson();

		TimeUtil.finishCountTime(startTime, String.format("Get ticket from platform [%s]", Constants.PLATFORM_CEX_IO));

		return retTicker;
	}

}
