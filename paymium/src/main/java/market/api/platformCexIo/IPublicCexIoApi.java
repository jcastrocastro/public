package market.api.platformCexIo;

import market.api.exception.ServiceException;

public interface IPublicCexIoApi {


	/**
	 * Gets the ticker
	 *
	 * @return String Json with the required info
	 * @throws ServiceException
	 */
	String getTicker() throws ServiceException;
}
