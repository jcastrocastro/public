package market.api.platformPaymium.publicall;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.Trade;
import market.api.enumtype.Currency;
import market.api.exception.ServiceException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LatestOrderMade {

	private final static Logger LOGGER = LoggerFactory.getLogger(LatestOrderMade.class.getName());

	private static final String URL_WEB = Constants.LATEST_TRADES_WEB;

	public String getUrlWeb() {
		return URL_WEB;
	}


	private Trade fullTrade(final JSONObject jsonObject) {
		final Trade trade = new Trade();

		trade.setUuid(jsonObject.getString(Trade.UUID));
		trade.setTraded_btc(jsonObject.getDouble(Trade.TRADED_BTC));
		trade.setTraded_currency(jsonObject.getDouble(Trade.TRADED_CURRENCY));
		trade.setCreated_at(jsonObject.getString(Trade.CREATED_AT));
		trade.setCurrency(Currency.getCurrency(jsonObject.getString(Trade.CURRENCY)));
		trade.setPrice(jsonObject.getDouble(Trade.PRICE));
		trade.setCreated_at_int(jsonObject.getInt(Trade.CREATED_AT_INT));

		return trade;
	}


	List<Trade> get() throws ServiceException {
		final List<Trade> retList = new ArrayList<>();

		LOGGER.info("Start retrieving latests trades");

		final JSONArray jsonArray = this.getJsonArray();

		if (!Util.isEmpty(jsonArray)){
			for (int cnt=0; cnt<jsonArray.length(); cnt++){
				retList.add(this.fullTrade((JSONObject)jsonArray.get(cnt)));
			}
		} else {
			LOGGER.error ("Number of trades is empty");
		}

		LOGGER.info("Finish retrieving latests trades");

		return retList;
	}


	private JSONArray getJsonArray() throws ServiceException {
		JSONArray jsonArray;
		try {
			jsonArray = Conversor.getJsonArray(HttpsHelper.getPublicReader(this.getUrlWeb(), Constants.UTF_8));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception", e);
			throw new ServiceException(e);
		}

		return jsonArray;
	}

	private String getJsonString() throws ServiceException {
		String line = Constants.EMPTY_STRING;

		try {
			final BufferedReader reader = HttpsHelper.getPublicReader(this.getUrlWeb(), Constants.UTF_8);

			if ((line = reader.readLine()) != null) {
				if (Util.isEmpty(line)){
					LOGGER.error("Empty line array json objects");
				}
			}

			reader.close();

		} catch (final IOException iexc){
			LOGGER.error("IO exception", iexc);
			throw new ServiceException(iexc);
		}

		return line;
	}

	String getJson() throws ServiceException {
		return this.getJsonString();
	}

}
