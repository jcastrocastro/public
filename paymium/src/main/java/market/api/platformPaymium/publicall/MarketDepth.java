package market.api.platformPaymium.publicall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.AskBidDepth;
import market.api.entity.Depth;
import market.api.exception.ServiceException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MarketDepth {

	private final static Logger LOGGER = LoggerFactory.getLogger(MarketDepth.class.getName());

	private static final String URL_WEB = Constants.MARKET_DEPTH_WEB;


	private String getUrlWeb() {
		return URL_WEB;
	}

	AskBidDepth get() throws ServiceException {
		final AskBidDepth askBidDepth = new AskBidDepth();

		LOGGER.info("Start retrieving market depth");

		Depth marketDepth;
		try {
			marketDepth = Conversor.getMarketDepth(HttpsHelper.getPublicReader(this.getUrlWeb(), Constants.UTF_8));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception", e);
			throw new ServiceException(e);
		}

		if (marketDepth!=null && !marketDepth.isEmpty()){
			for (int cnt=0; cnt<marketDepth.getBids().length(); cnt++){
				askBidDepth.getBids().add(Util.fullMarketDepth((JSONObject)marketDepth.getBids().get(cnt)));
			}
			LOGGER.info("Length of market depth bids [{}]", marketDepth.getBids().length());

			for (int cnt=0; cnt<marketDepth.getAsks().length(); cnt++){
				askBidDepth.getAsks().add(Util.fullMarketDepth((JSONObject)marketDepth.getAsks().get(cnt)));
			}
			LOGGER.info("Length of market depth asks [{}]", marketDepth.getAsks().length());
		} else {
			LOGGER.error ("MarketDepth is empty");
		}

		LOGGER.info("Finish retrieving market depth");

		return askBidDepth;
	}

	/**
	 *
	 * @return
	 * @throws ServiceException Json exception or unsupported encoding or IO exception
	 */
	String getJson() throws ServiceException {
		String retString = null;

		try {
			retString =  Util.getString(HttpsHelper.getPublicReader(this.getUrlWeb(), Constants.UTF_8));

		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception", e);
			throw new ServiceException(e);
		}

		return retString;
	}

}
