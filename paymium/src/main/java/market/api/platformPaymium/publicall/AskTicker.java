package market.api.platformPaymium.publicall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.Ticker;
import market.api.enumtype.Currency;
import market.api.exception.ServiceException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AskTicker  {

	private final static Logger LOGGER = LoggerFactory.getLogger(AskTicker.class.getName());

	private static final String URLWEB = Constants.TICKER_URL_WEB;

	public String getUrlWeb() {
		return URLWEB;
	}


	Ticker get() throws ServiceException {
		LOGGER.info("Start retrieving Ticker");

		final JSONObject jsonObject = this.getJsonObject();

		Ticker ticker = null;
		if (!Util.isEmpty(jsonObject)){
			ticker = this.fullTicker(jsonObject);
		} else {
			LOGGER.error ("The ticker is empty");
		}

		LOGGER.info("Finish retrieving Ticker");

		return ticker;
	}

	private JSONObject getJsonObject() throws ServiceException {
		JSONObject jsonObject = null;
		try {
			jsonObject = Conversor.getJsonObject(HttpsHelper.getPublicReader(this.getUrlWeb(), Constants.UTF_8));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception", e);
			throw new ServiceException(e);
		}

		return jsonObject;
	}

	/**
	 * Full one ticket
	 * @param listJsonObjects
	 * @return
	 */
	private Ticker fullTicker(final JSONObject jsonObject) {
		final Ticker ticker = new Ticker();

		ticker.setHigh(jsonObject.getDouble(Ticker.HIGH));
		ticker.setLow(jsonObject.getDouble(Ticker.LOW));
		ticker.setVolume(jsonObject.getDouble(Ticker.VOLUME));
		ticker.setBid(jsonObject.getDouble(Ticker.BID));
		ticker.setAsk(jsonObject.getDouble(Ticker.ASK));
		ticker.setMidpoint(jsonObject.getDouble(Ticker.MIDPOINT));
		ticker.setVwap(jsonObject.getDouble(Ticker.VWAP));
		ticker.setAt(jsonObject.getLong(Ticker.AT));
		ticker.setPrice(jsonObject.getDouble(Ticker.PRICE));
		ticker.setOpen(jsonObject.getDouble(Ticker.OPEN));
		ticker.setVariation(jsonObject.getDouble(Ticker.VARIATION));
		ticker.setCurrency(Currency.getCurrency(jsonObject.getString(Ticker.CURRENCY)));
		ticker.setTradeId(jsonObject.getString(Ticker.TRADEID));
		ticker.setSize(jsonObject.getDouble(Ticker.SIZE));

		return ticker;
	}

	String getJson() throws ServiceException {
		return Util.adaptPrimaryKeyToMongoDb(this.getJsonObject().toString(), Ticker.TRADEID);
	}

}
