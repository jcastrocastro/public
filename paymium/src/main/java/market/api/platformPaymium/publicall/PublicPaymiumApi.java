package market.api.platformPaymium.publicall;

import market.api.common.TimeUtil;
import market.api.entity.AskBidString;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPublicPaymiumApi;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PublicPaymiumApi implements IPublicPaymiumApi{

	private final static Logger LOGGER = LoggerFactory.getLogger(PublicPaymiumApi.class.getName());

	private final AskTicker askTicker;
	private final LatestOrderMade latestTrades;
	private final MarketDepth marketDepth;

	/**
     * Initializes singleton.
     *
     * {@link SingletonHolder} is loaded on the first execution of {@link Singleton#getInstance()} or the first access to
     * {@link SingletonHolder#INSTANCE}, not before.
     */
    private static class SingletonHolder {
            private static final PublicPaymiumApi INSTANCE = new PublicPaymiumApi();
    }

    public static IPublicPaymiumApi getInstance() {
            return SingletonHolder.INSTANCE;
    }

	// Private constructor. Prevents instantiation from other classes.
    private PublicPaymiumApi() {
    	this.askTicker = new AskTicker();
    	this.latestTrades = new LatestOrderMade();
    	this.marketDepth = new MarketDepth();
    }

    public AskTicker getAskTicker() {
		return this.askTicker;
	}


	public LatestOrderMade getLatestTrades() {
		return this.latestTrades;
	}


	public MarketDepth getMarketDepth() {
		return this.marketDepth;
	}

	@Override
	public String getTicker() throws ServiceException{
		return this.askTicker.getJson();
	}

	@Override
	public AskBidString getMarketDepthJson(final int numAsks, final int numBids) throws ServiceException {
		LOGGER.info("Get Market Depth number ask [{}] and bids [{}]", numAsks, numBids);

		AskBidString askBidString = null;
		try {
			final String retJson = this.getMarketDepth().getJson();

			askBidString = new AskBidString(this.getAsk(retJson, numAsks), this.getBid(retJson, numBids));
		} catch (final ServiceException e) {
			LOGGER.error("Error getting marketDepth numAsks [{}], numBids [{}]", numAsks, numBids);
			throw e;
		}

		return askBidString;
	}


	@Override
	public String getBid(final String retJson, final int numBids) {
		final StringBuilder stringBuilder = new StringBuilder(retJson.split(",\"asks\":")[0]).reverse();
		final String tempString = stringBuilder.deleteCharAt(0).toString();
		stringBuilder.delete(0, stringBuilder.length());

		return stringBuilder.append(tempString.split("}")[numBids]).reverse().deleteCharAt(0).append("}").toString();
	}

	@Override
	public String getAsk(final String retJson, final int numAsks) {
		final StringBuilder stringBuilder = new StringBuilder(retJson.split(",\"asks\":")[1]);
		final String tempString = stringBuilder.deleteCharAt(0).toString();
		stringBuilder.delete(0, stringBuilder.length());

		return stringBuilder.append(tempString.split("}")[numAsks-1]).deleteCharAt(0).append("}").toString();
	}


	@Override
	public AskBidString getLastNthMarketDepthJson(final int maxAsks, final int maxBids) throws ServiceException {
		final long startTime = TimeUtil.startCountTime();

		LOGGER.info("Get Market Depth number max ask [{}] and max bids [{}]", maxAsks, maxBids);

		String retJson = null;
		try {
			retJson = this.getMarketDepth().getJson();
		} catch (final ServiceException e) {
			LOGGER.error("Error getting marketDepth numAsks [{}], numBids [{}]. {}", new Object[]{maxAsks, maxBids, e});
			throw e;
		}

		final AskBidString askBidString = new AskBidString(this.getLastAsk(retJson, maxAsks), this.getLastBid(retJson, maxBids));

		TimeUtil.finishCountTime(startTime, "Getting getLastNMarketDepthJson.");

		return askBidString;
	}


	@Override
	public String getLastAsk(final String retJson, int maxAsks) {
		final String splitAsk = retJson.split(",\"asks\":")[1];

		final int numberOcurrences = StringUtils.countMatches(splitAsk, "}") - 1 ;
		if (numberOcurrences<maxAsks){
			maxAsks = numberOcurrences;
			LOGGER.trace("Number maximum of asks [{}]", maxAsks);
		}

		return new StringBuilder(splitAsk.substring(0, StringUtils.ordinalIndexOf(splitAsk, "}", maxAsks))).append("}]").toString();
	}

	@Override
	public String getLastBid(final String retJson, int maxBids) {
		final String splitAsk = retJson.split(",\"asks\":")[0];
		final StringBuilder stringBuilder = new StringBuilder(splitAsk).reverse();

		final int numberOcurrence = StringUtils.countMatches(splitAsk, "}") - 1 ;
		if (numberOcurrence<maxBids){
			maxBids = numberOcurrence;
			LOGGER.trace("Number maximum of bids [{}]", maxBids);
		}

		final int ordinalIdex = StringUtils.ordinalIndexOf(stringBuilder.toString(), "}", maxBids+1);

		return new StringBuilder(stringBuilder.substring(0, ordinalIdex)).append("}").reverse().replace(0, 2, "[").toString();

	}

	@Override
	public String getOrderMade(final int maxTrades) throws ServiceException {
		final long startTime = TimeUtil.startCountTime();

		LOGGER.trace("Gets max order made [{}]", maxTrades);

		StringBuilder stringBuilder = null;
		try {
			stringBuilder = new StringBuilder(this.getLatestTrades().getJson()).reverse();
		} catch (final ServiceException e) {
			LOGGER.error("Error getting latest trades", e);
			throw e;
		}

		final int finalIndex = StringUtils.ordinalIndexOf(stringBuilder.toString(), "}", maxTrades+1);

		final StringBuilder retStringBuilder = new StringBuilder(stringBuilder.substring(0, finalIndex-1)).append("[").reverse();

		TimeUtil.finishCountTime(startTime, "get Order Made (retrieve executed trades) to the platform");

		return retStringBuilder.toString();
	}
}
