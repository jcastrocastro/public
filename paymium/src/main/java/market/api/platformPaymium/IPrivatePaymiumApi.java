package market.api.platformPaymium;

import market.api.exception.ServiceException;

import org.bson.Document;
import org.json.JSONArray;

public interface IPrivatePaymiumApi {

	/**
	 * Gets all active orders (ACTIVE) and not (FILLED)
	 * @return
	 * @throws ServiceException Connection error to the platform
	 */
	String getAllActiveOrders() throws ServiceException;

	/**
	 * Gets all active orders (ACTIVE) and not (FILLED).
	 * <p>
	 * Makes sure to get only the state active orders
	 * @return
	 * @throws ServiceException Connection error to the platform
	 */
	JSONArray getAllActiveOrders2() throws ServiceException;

	/**
	 * Gets user info from platform
	 * @return
	 * @throws ServiceException
	 */
	String getUserInfo() throws ServiceException;

	/**
	 * Gets all recent orders from platform. Include recent alive and filled trades.
	 * <p>
	 * The meaning of 'recent' is controlled by the platform
	 * @return
	 * @throws ServiceException
	 */
	String getAllRecentOrders() throws ServiceException;

	/**
	 * Makes a real buy order to platform
	 * @param price Price to make order
	 * @param amount Amout to make order
	 * @return
	 * @throws ServiceException
	 */
	String makeLimitedBuyOrder(double price, double amount)
			throws ServiceException;

	/**
	 * Makes limited sell order
	 *
	 * @param priceMakeSellOrder Price make sell order
	 * @param amountSellOrder amount sell order
	 * @return
	 * @throws ServiceException
	 */
	String makeLimitedSellOrder(double priceMakeSellOrder, double amountSellOrder) throws ServiceException;

	/**
	 * Cancels the order given
	 * @param docToCancel
	 * @throws ServiceException
	 * 						Empty uuid, Unsopported encoding exception, Json exception, the uuid is not active, IO exception. Authorization issues
	 */
	void cancelOrder(Document docToCancel)
										throws ServiceException;


}
