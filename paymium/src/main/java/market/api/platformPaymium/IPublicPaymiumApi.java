package market.api.platformPaymium;

import market.api.entity.AskBidString;
import market.api.exception.ServiceException;

public interface IPublicPaymiumApi {

	/**
	 * Gets the all market depth in json. Bids and asks
	 *
	 * @param maxAsks Number maximum of asks
	 * @param maxbids Number maximum of bids
	 * @return AskBidString
	 * @throws ServiceException
	 */
	AskBidString getMarketDepthJson(int numAsks, int numBids) throws ServiceException;

	/**
	 * Gets the last nth market depth in json. Bids and asks
	 *
	 * @param maxAsks Number maximum of asks
	 * @param maxbids Number maximum of bids
	 * @return AskBidString
	 * @throws ServiceException Error connection to the platform
	 */
	AskBidString getLastNthMarketDepthJson(int maxAsks, int maxbids) throws ServiceException;

	/**
	 * Gets the nth order already made of executaded trades.
	 *
	 * @param nthExecutedTrade Nth executed trade
	 * @return String Json with the required info
	 * @throws ServiceException
	 */
	String getOrderMade(int nthExecutedTrade) throws ServiceException;

	/**
	 * Gets ticker in json string
	 *
	 * @return
	 * @throws ServiceException
	 */
	String getTicker() throws ServiceException;

	/**
	 * Gets ask nth
	 *
	 * @param retJson jsonString
	 * @param numAsks Number of asks to ask for
	 * @return
	 */
	String getAsk(final String jsonString, final int numAsks);

	/**
	 * Gets bid nth
	 *
	 * @param retJson jsonString
	 * @param numBids Number of bids to ask for
	 * @return
	 */
	String getBid(String json, int numBids);

	/**
	 * Gets last ask
	 *
	 * @param json
	 * @param numAsk
	 * @return
	 */
	String getLastAsk(String json, int numAsk);

	/**
	 * Gets last bid
	 *
	 * @param json
	 * @param numBid
	 * @return
	 */
	String getLastBid(String json, int numBid);

}
