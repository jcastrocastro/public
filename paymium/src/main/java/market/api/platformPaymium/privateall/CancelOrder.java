package market.api.platformPaymium.privateall;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.enumtype.HttpMethod;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.encoder.ApiSecurityEncodeHex;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CancelOrder extends PrivateAccess {

	private final static Logger LOGGER = LoggerFactory.getLogger(CancelOrder.class.getName());

	private static final String POST_FIX = "/cancel";


	String getUrlWeb() {
		return Constants.USER_ORDER_URL;
	}

	/**
	 *
	 * @param uuid Uuid to cancel in the platform
	 * @throws ServiceException Empty uuid, Unsopported encoding exception, Json exception, the uuid is not active, IO exception. Authorization issues
	 */
	void delete(final String uuid) throws ServiceException {
		if (Util.isEmpty(uuid)){
			LOGGER.error("Uuid is empty");
			throw new ServiceException("Uuid can not be empty");
		}

		LOGGER.info("Start cancelling order with uuid [{}]", uuid);

		final String bodyMessage = new StringBuilder("/").append(uuid).append(POST_FIX).toString();

		final Properties prop = this.getConnectionProp(this.getUrlWeb(), bodyMessage);

		// Get the codification info
		prop.setProperty(Constants.API_SIGNATURE, ApiSecurityEncodeHex.encodeHash(prop.getProperty(Constants.API_NONCE),
																				  bodyMessage,
			  	   		 														  prop.getProperty(Constants.API_SECRET_KEY),
			  	   		 														  this.getUrlWeb()));

		BufferedReader output = null;
		try {

			output = HttpsHelper.getPrivateReader(new StringBuilder(Constants.USER_ORDER_URL).append("/").append(uuid).append(POST_FIX).toString(),
										   			Constants.UTF_8,
										   			prop,
										   			HttpMethod.DELETE);
			// Printing info delete
			String str;
			while ((str=output.readLine())!=null) {
				LOGGER.info(str);
		    }

		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final FileNotFoundException fnfe) {
			LOGGER.error("I guess the uuid [{}] is not active. {}", uuid, fnfe.getMessage());
			throw new ServiceException(fnfe);
		} catch (final IOException e) {
			LOGGER.error("IO exception. Authorization issues. {}", e.getMessage());
			throw new ServiceException(e);
		}

		LOGGER.info("Finish cancelling order of uuid [{}]", uuid);

	}

}
