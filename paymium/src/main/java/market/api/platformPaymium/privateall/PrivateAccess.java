package market.api.platformPaymium.privateall;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.entity.AccountOperation;
import market.api.entity.Order;
import market.api.entity.Token;
import market.api.enumtype.Currency;
import market.api.enumtype.State;
import market.api.exception.ServiceException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrivateAccess {

	private final static Logger LOGGER = LoggerFactory.getLogger(PrivateAccess.class.getName());

	// Only uses one token
	private static final Token token = ConnectionDao.getNewTokenDao().getToken();

	public static Token getToken() {
		return token;
	}

	protected Properties getConnectionProp(final String webUrl, final String bodyHttpMsg){
		final Properties prop = new Properties();
		prop.setProperty(Constants.API_KEY, getToken().getApiKey());
		prop.setProperty(Constants.API_SECRET_KEY, getToken().getSecretKey());
		prop.setProperty(Constants.BODY_HTTP_MSG, bodyHttpMsg);
		prop.setProperty(Constants.API_NONCE, String.valueOf(TimeUtil.getTimeNow()));
		prop.setProperty(Constants.API_WEB_USER, new StringBuilder(webUrl).append(bodyHttpMsg).toString());

		return prop;
	}

	/**
	 * Full Order
	 * @param jsonObject
	 * @return
	 * @throws JobExecutionException  Error the jsonString key value is not a number
	 */
	protected Order fullOrder(final JSONObject jsonObject) throws JobExecutionException {
		final Order userActivity = new Order();

		userActivity.setUuid(jsonObject.getString(Order.UUID));
		try {
			userActivity.setAmount(Util.getDouble(jsonObject.toString(), Order.AMOUNT));
		} catch (final ServiceException exc) {
			throw new JobExecutionException(exc);
		}
		userActivity.setState(State.getState(jsonObject.getString(Order.STATE)));
		userActivity.setBtcFee(jsonObject.getDouble(Order.BTC_FEE));
		userActivity.setCurrencyFee(jsonObject.getDouble(Order.CURRENCY_FEE));
		userActivity.setUpdateAt(jsonObject.getString(Order.UPDATED_AT));
		userActivity.setCreatedAt(jsonObject.getString(Order.CREATED_AT));
		userActivity.setCurrency(Currency.getCurrency(jsonObject.getString(Order.CURRENCY)));
		userActivity.setType(jsonObject.getString(Order.TYPE));
		userActivity.setAccountOperations(this.fullAccountOperations(jsonObject.getJSONArray(Order.ACCOUNT_OPERATIONS)));

		return userActivity;
	}


	protected List<AccountOperation> fullAccountOperations(final JSONArray jsonArray) {
		final List<AccountOperation> accountOperationList = new ArrayList<>();

		if (!Util.isEmpty(jsonArray)){
			for (int cnt=0; cnt<jsonArray.length(); cnt++){
				accountOperationList.add(this.fullAccountOperation((JSONObject)jsonArray.get(cnt)));
			}
		} else {
			LOGGER.error ("Number of account operations is empty");
		}

		return accountOperationList;
	}

	private AccountOperation fullAccountOperation(final JSONObject jsonObject) {
		final AccountOperation accountOperation = new AccountOperation();

		accountOperation.setUuid(jsonObject.getString(AccountOperation.UUID));
		accountOperation.setName(jsonObject.getString(AccountOperation.NAME));
		accountOperation.setAmount(jsonObject.getDouble(AccountOperation.AMOUNT));
		accountOperation.setCurrency(Currency.getCurrency(jsonObject.getString(AccountOperation.CURRENCY)));
		accountOperation.setCreatedAt(jsonObject.getString(AccountOperation.CREATED_AT));
		accountOperation.setCreatedAtInt(jsonObject.getInt(AccountOperation.CREATED_AT_INT));
		accountOperation.setTradingAccount(jsonObject.getBoolean(AccountOperation.IS_TRADING_ACCOUNT));

		return accountOperation;
	}
}
