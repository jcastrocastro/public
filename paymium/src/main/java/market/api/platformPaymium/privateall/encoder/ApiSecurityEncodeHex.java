package market.api.platformPaymium.privateall.encoder;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import market.api.common.Constants;
import market.api.exception.ServiceException;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApiSecurityEncodeHex {

	private final static Logger LOGGER = LoggerFactory.getLogger(ApiSecurityEncodeHex.class.getName());
	
	/**
	 * Encodes hash
	 * <pre>
	 * Concatenatiof of:
	 * 	nonce + uri + bodyMessage
	 * </pre>
	 * @param nonce
	 * @param bodyMessage
	 * @param apiSecretKey
	 * @param uri
	 * @return
	 * @throws ServiceException
	 */
	public static String encodeHash(String nonce, String bodyMessage, String apiSecretKey, String uri) throws ServiceException {
		
		byte[] dataBytes = null;
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");

			SecretKeySpec secret_key = new SecretKeySpec(apiSecretKey.getBytes(Constants.CHARSET), "HmacSHA256");
			sha256_HMAC.init(secret_key);

			final String messageToSend = new StringBuilder().append(nonce).append(uri).append(bodyMessage).toString();
			
			LOGGER.info("Message to codify [{}], charset [{}] ", messageToSend, Constants.CHARSET);
			
//			LOGGER.info("SecretKey [{}] ", apiSecretKey);
			
			dataBytes = sha256_HMAC.doFinal(messageToSend.getBytes(Constants.CHARSET));
			
		} catch (NoSuchAlgorithmException e) {
			LOGGER.error("Error not such algoritm", e);
			throw new ServiceException(e);
		} catch (InvalidKeyException e) {
			LOGGER.error("Error not such algoritm", e);
			throw new ServiceException(e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Error not such algoritm", e);
			throw new ServiceException(e);
		}		
		
		return Hex.encodeHexString(dataBytes);
	}


}
