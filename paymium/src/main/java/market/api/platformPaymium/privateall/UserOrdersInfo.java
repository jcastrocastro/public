package market.api.platformPaymium.privateall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.Order;
import market.api.enumtype.HttpMethod;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.encoder.ApiSecurityEncodeHex;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author raul
 *
 */
public class UserOrdersInfo extends PrivateAccess{

	private final static Logger LOGGER = LoggerFactory.getLogger(UserOrdersInfo.class.getName());

	private static final String urlWeb = Constants.USER_ORDER_URL;

	private final String bodyUrl;

	public UserOrdersInfo() {
		super();
		this.bodyUrl = Constants.EMPTY_STRING;
	}

	/**
	 * Get all active activity of the user
	 * @param bodyUrl
	 */
	public UserOrdersInfo(final String bodyUrl) {
		super();
		this.bodyUrl = bodyUrl;
	}

	public String getBodyUrl() {
		return this.bodyUrl;
	}


	public String getUrlWeb() {
		return urlWeb;
	}


	List<Order> get() throws ServiceException {
		final List<Order> retOrderList = new ArrayList<>();

		LOGGER.info("Start getting user activity info");

		final Properties prop = this.getProp();

		JSONArray jsonArray;
		try {
			jsonArray = Conversor.getJsonArray(HttpsHelper.getPrivateReader(prop.getProperty(Constants.API_WEB_USER), Constants.UTF_8, prop, HttpMethod.GET));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception. Sure can't connect for authorization issues. {}", new Object[]{e.getMessage()});
			throw new ServiceException(e);
		}

		if (!Util.isEmpty(jsonArray)){
			for (int cnt=0; cnt<jsonArray.length(); cnt++){
				try {
					retOrderList.add(this.fullOrder((JSONObject)jsonArray.get(cnt)));
				} catch (JobExecutionException | JSONException sexc) {
					throw new ServiceException(sexc);
				}
			}
		} else {
			LOGGER.error ("Number of trades is empty");
		}

		LOGGER.info("Finish getting user activity info");

		return retOrderList;
	}

	private Properties getProp() throws ServiceException {
		 final Properties retProp = this.getConnectionProp(this.getUrlWeb(), this.getBodyUrl());

		// Get the codification info
		retProp.setProperty(Constants.API_SIGNATURE, ApiSecurityEncodeHex.encodeHash(retProp.getProperty(Constants.API_NONCE),
																				  	this.getBodyUrl(),
																				  	retProp.getProperty(Constants.API_SECRET_KEY),
			  	   		 														  	this.getUrlWeb()));
		return retProp;
	}

	public String getJson() throws ServiceException {
		String retJson = null;

		final long startTime = TimeUtil.startCountTime();

		final Properties prop = this.getProp();

		try {
			retJson = Util.getString(HttpsHelper.getPrivateReader(prop.getProperty(Constants.API_WEB_USER), Constants.UTF_8, prop, HttpMethod.GET));
		} catch (final IOException e) {
			LOGGER.error("IO exception. Sure can't connect for authorization issues. {}", new Object[]{e.getMessage()});
			throw new ServiceException(e);
		}

		TimeUtil.finishCountTime(startTime, "Getting order info from platform.");

		return retJson;
	}

}
