package market.api.platformPaymium.privateall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import market.api.application.Application;
import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.TimeUtil;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.User;
import market.api.enumtype.HttpMethod;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.encoder.ApiSecurityEncodeHex;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserInfo extends PrivateAccess {

	private final static Logger LOGGER = LoggerFactory.getLogger(UserInfo.class.getName());

	private static final String urlWeb = Constants.USER_INFO_URL;


	public String getUrlWeb() {
		return urlWeb;
	}


	User get() throws ServiceException {
		LOGGER.info("Start getting user info");

		final Properties prop = this.getProp();

		// Get info user
		JSONObject jsonObject = null;
		try {
			jsonObject = Conversor.getJsonObject(HttpsHelper.getPrivateReader(this.getUrlWeb(), Constants.UTF_8, prop, HttpMethod.GET));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception. Sure can't connect for authorization issues. {}", e);
			throw new ServiceException(e);
		}

		User user = null;
		if (!Util.isEmpty(jsonObject)){
			user = this.fullUser(jsonObject);
		} else {
			LOGGER.error ("The User info is empty");
		}

		LOGGER.info("Finish getting user info");

		return user;
	}

	private Properties getProp() throws ServiceException {
		final Properties prop = this.getConnectionProp(this.getUrlWeb(), Constants.EMPTY_STRING);

		// Get the codification info
		prop.setProperty(Constants.API_SIGNATURE, ApiSecurityEncodeHex.encodeHash(prop.getProperty(Constants.API_NONCE),
			  	   		 														  prop.getProperty(Constants.BODY_HTTP_MSG),
			  	   		 														  prop.getProperty(Constants.API_SECRET_KEY),
			  	   		 														  this.getUrlWeb()));
		return prop;
	}


	private User fullUser(final JSONObject jsonObject) {
		final User retUser = new User();

		retUser.setLocked_eur(jsonObject.getDouble(User.LOCKED_EUR));
		retUser.setLocked_btc(jsonObject.getDouble(User.LOCKED_BTC));
		retUser.setBalanceBtc(jsonObject.getDouble(User.BALANCE_BTC));
		retUser.setBalanceEur(jsonObject.getDouble(User.BALANCE_EUR));
		retUser.setLocale(jsonObject.getString(User.LOCALE));
		retUser.setName(jsonObject.getString(User.NAME));

		return retUser;
	}

	/**
	 * Gets user info from platform
	 * @return
	 * @throws ServiceException
	 */
	public String getJson() throws ServiceException {
		String retJson = null;

		LOGGER.trace("Start retrieving user info from platform");

		final long startTime = TimeUtil.startCountTime();
		try {
			retJson = Util.getString(HttpsHelper.getPrivateReader(this.getUrlWeb(), Constants.UTF_8, this.getProp(), HttpMethod.GET));
			Application.resetConnectionRetry();
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsupported encoding exception");
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception");
			throw new ServiceException(e);
		}

		// Finish/print count time
		TimeUtil.finishCountTime(startTime, "Getting user info from platform.");

		LOGGER.trace("Retrieved user info {}", retJson);

		return retJson;
	}
}
