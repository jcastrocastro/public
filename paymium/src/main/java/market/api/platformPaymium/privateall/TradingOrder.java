package market.api.platformPaymium.privateall;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.TradeOrder;
import market.api.enumtype.Currency;
import market.api.enumtype.Direction;
import market.api.enumtype.State;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.encoder.ApiSecurityEncodeHex;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TradingOrder extends PrivateAccess {

	private final static Logger LOGGER = LoggerFactory.getLogger(TradeOrder.class.getName());

	private static final String URLWEB = Constants.USER_ORDER_URL;

	public String getUrlWeb() {
		return URLWEB;
	}


	TradeOrder post(final String bodyPostJson) throws ServiceException {

		LOGGER.info("Start getting {} info", TradeOrder.class.getSimpleName());

		final Properties prop = this.getProp(bodyPostJson);

		JSONObject jsonObject = null;
		try {
			prop.setProperty(Constants.BODY_HTTP_MSG, bodyPostJson);
			jsonObject = Conversor.getJsonObject(HttpsHelper.postPrivateReader(this.getUrlWeb(), Constants.UTF_8, prop));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException e) {
			LOGGER.error("IO exception. Sure can't connect for authorization issues or you are using the incorrect token. ", e);
			throw new ServiceException(e);
		}

		TradeOrder order = null;
		if (!Util.isEmpty(jsonObject)){
			order = this.fullTradeOrder(jsonObject);
		} else {
			LOGGER.error ("The Order info jsonObject is empty");
		}

		LOGGER.info("Finish getting {} info", TradeOrder.class.getSimpleName());

		return order;
	}


	private Properties getProp(final String bodyPostJson) throws ServiceException {
		final Properties prop = this.getConnectionProp(this.getUrlWeb(), Constants.EMPTY_STRING);

		// Get the codification info
		prop.setProperty(Constants.API_SIGNATURE, ApiSecurityEncodeHex.encodeHash(prop.getProperty(Constants.API_NONCE),
																				  bodyPostJson,
			  	   		 														  prop.getProperty(Constants.API_SECRET_KEY),
			  	   		 														  this.getUrlWeb()));
		return prop;
	}


	private TradeOrder fullTradeOrder(final JSONObject jsonObject) {
		final TradeOrder retOrder = new TradeOrder();

		retOrder.setUuid(jsonObject.getString(TradeOrder.UUID));
		retOrder.setAmount(jsonObject.getDouble(TradeOrder.AMOUNT));
		retOrder.setState(State.getState(jsonObject.getString(TradeOrder.STATE)));
		retOrder.setBtcFee(jsonObject.getDouble(TradeOrder.BTC_FEE));
		retOrder.setCurrencyFee(jsonObject.getDouble(TradeOrder.CURRENCY_FEE));
		retOrder.setUpdateAt(jsonObject.getString(TradeOrder.UPDATED_AT));
		retOrder.setCreatedAt(jsonObject.getString(TradeOrder.CREATED_AT));
		retOrder.setCurrency(Currency.getCurrency(jsonObject.getString(TradeOrder.CURRENCY)));
		retOrder.setType(jsonObject.getString(TradeOrder.TYPE));
		retOrder.setTradedBtc(jsonObject.getDouble(TradeOrder.TRADED_BTC));
		retOrder.setTraded_currency(jsonObject.getDouble(TradeOrder.TRADED_CURRENCY));
		retOrder.setDirection(Direction.getDirection(jsonObject.getString(TradeOrder.DIRECTION)));
		retOrder.setPrice(jsonObject.getDouble(TradeOrder.PRICE));
		retOrder.setAccount_operations(this.fullAccountOperations(jsonObject.getJSONArray(TradeOrder.ACCOUNT_OPERATIONS)));

		return retOrder;
	}

	String postJson(final String bodyPostJson) throws ServiceException {

		LOGGER.info("Start getting {} info", TradingOrder.class.getSimpleName());

		final Properties prop = this.getProp(bodyPostJson);

		prop.setProperty(Constants.BODY_HTTP_MSG, bodyPostJson);

		String jsonString = null;
		try {
			jsonString = Util.getString(HttpsHelper.postPrivateReader(this.getUrlWeb(), Constants.UTF_8, prop));
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Error unsupported encoding exception", e);
			throw new ServiceException(e);
		} catch (final IOException ioexc) {
			LOGGER.error("Error IO exception", ioexc);
			throw new ServiceException(ioexc);
		}

		LOGGER.info("Finish getting {} info", TradingOrder.class.getSimpleName());

		return jsonString;
	}

}
