package market.api.platformPaymium.privateall;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import market.api.common.Constants;
import market.api.common.Conversor;
import market.api.common.Util;
import market.api.connection.https.HttpsHelper;
import market.api.entity.Order;
import market.api.enumtype.HttpMethod;
import market.api.exception.ServiceException;
import market.api.platformPaymium.privateall.encoder.ApiSecurityEncodeHex;

import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OrderDetail extends PrivateAccess {

	private final static Logger LOGGER = LoggerFactory.getLogger(OrderDetail.class.getName());


	public String getUrlWeb() {
		return Constants.USER_ORDER_URL;
	}

	Order get(final String uuid) throws ServiceException {
		Order retOrder = null;

		if (Util.isEmpty(uuid)){
			LOGGER.error("Uuid is empty");
			throw new ServiceException("Uuid can not be empty");
		}

		LOGGER.info("Start getting order with uuid [{}]", uuid);

		final String bodyMessage = new StringBuilder("/").append(uuid).toString();

		final Properties prop = this.getConnectionProp(this.getUrlWeb(), bodyMessage);

		// Get the codification info
		prop.setProperty(Constants.API_SIGNATURE, ApiSecurityEncodeHex.encodeHash(prop.getProperty(Constants.API_NONCE),
																				  bodyMessage,
			  	   		 														  prop.getProperty(Constants.API_SECRET_KEY),
			  	   		 														  this.getUrlWeb()));

		JSONObject jsonObject = null;
		try {

			jsonObject = Conversor.getJsonObject(HttpsHelper.getPrivateReader(new StringBuilder(Constants.USER_ORDER_URL).append("/").append(uuid).toString(),
										   			Constants.UTF_8,
										   			prop,
										   			HttpMethod.GET));
		} catch (final JSONException e) {
			LOGGER.error("Json exception", e);
			throw new ServiceException(e);
		} catch (final UnsupportedEncodingException e) {
			LOGGER.error("Unsopported encoding exception", e);
			throw new ServiceException(e);
		} catch (final FileNotFoundException fnfe) {
			LOGGER.error("I guess the uuid [{}] is not active. {}", uuid, fnfe.getMessage());
			throw new ServiceException(fnfe);
		} catch (final IOException e) {
			LOGGER.error("IO exception. Authorization issues. {}", e.getMessage());
			throw new ServiceException(e);
		}

		try {
			retOrder = this.fullOrder(jsonObject);
		} catch (final JobExecutionException sexc) {
			throw new ServiceException(sexc);
		}

		LOGGER.info("Finish getting order with uuid [{}]", uuid);

		return retOrder;
	}

}
