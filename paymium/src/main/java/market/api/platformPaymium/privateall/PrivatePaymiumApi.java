package market.api.platformPaymium.privateall;

import java.util.HashMap;
import java.util.Map;

import market.api.application.ApplicationConf;
import market.api.common.Constants;
import market.api.common.SimulationUtil;
import market.api.common.Util;
import market.api.entity.TradeOrder;
import market.api.enumtype.Currency;
import market.api.enumtype.Direction;
import market.api.enumtype.TypeOrder;
import market.api.exception.ServiceException;
import market.api.platformPaymium.IPrivatePaymiumApi;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.spi.resource.Singleton;

public class PrivatePaymiumApi implements IPrivatePaymiumApi{

	private final static Logger LOGGER = LoggerFactory.getLogger(PrivatePaymiumApi.class.getName());

	private final CancelOrder cancelOrder;
	private final TradingOrder tradingOrder;
	private final UserInfo userInfo;
	private final UserOrdersInfo allUserOrderInfo;    // Get last nth trade orders  // 20th ??? with two states ACTIVE or not ACTIVE (FILLED and so on)
	private final UserOrdersInfo activeTradeOrderInfo; // Get last nth active trade orders // 20th ???

	// Private constructor. Prevents instantiation from other classes.
	private PrivatePaymiumApi() {
		this.cancelOrder = new CancelOrder();
		this.tradingOrder = new TradingOrder();
		this.allUserOrderInfo = new UserOrdersInfo();
		this.activeTradeOrderInfo = new UserOrdersInfo("?active=true"); //("?active=true");
		this.userInfo = new UserInfo();
	}

	/**
	 * Initializes singleton.
	 *
	 * {@link SingletonHolder} is loaded on the first execution of {@link Singleton#getInstance()} or the first access to
	 * {@link SingletonHolder#INSTANCE}, not before.
	 */
	private static class SingletonHolder {
		private static final PrivatePaymiumApi INSTANCE = new PrivatePaymiumApi();
	}

	public static IPrivatePaymiumApi getInstance() {
		return SingletonHolder.INSTANCE;
	}

	@Override
	public String getAllActiveOrders() throws ServiceException{
		return this.activeTradeOrderInfo.getJson();
	}

	@Override
	public JSONArray getAllActiveOrders2() throws ServiceException{
		final JSONArray jsonAliveArray = new JSONArray(this.activeTradeOrderInfo.getJson());

		return Util.keepOnlyGivenType(jsonAliveArray, TypeOrder.LIMIT_ORDER);
	}

//	final JSONArray jsonAliveArray = new JSONArray(jsonActiveObjectString);

	@Override
	public String getAllRecentOrders() throws ServiceException {
		return this.allUserOrderInfo.getJson();
	}

	@Override
	public String getUserInfo() throws ServiceException{
		return this.userInfo.getJson();
	}

	@Override
	public String makeLimitedBuyOrder(final double price, final double amount)
																		throws ServiceException {

		final Map<String, Object> valueByKeyTradeOrder = new HashMap<>();
		valueByKeyTradeOrder.put(TradeOrder.TYPE, TypeOrder.LIMIT_ORDER.getName());
		valueByKeyTradeOrder.put(TradeOrder.CURRENCY, Currency.EUR.getName());
		valueByKeyTradeOrder.put(TradeOrder.DIRECTION, Direction.BUY.getName());
		valueByKeyTradeOrder.put(TradeOrder.PRICE, price);
		valueByKeyTradeOrder.put(TradeOrder.AMOUNT, amount);

		String jsonAnswer = null;
		if (ApplicationConf.getApplicationConf().isSimulate()) {
			LOGGER.info("Simulation of the buy order with price [{}] and amount [{}]", price, amount);
			jsonAnswer = SimulationUtil.setTradeFee(valueByKeyTradeOrder);
			LOGGER.debug("Simulated answer making limited buy order [{}]", jsonAnswer);
		} else {
			LOGGER.info("Real execution of the buy order with price [{}] and amount [{}]", price, amount);
			final JSONObject bodyPostJson = new JSONObject(JSONObject.valueToString(valueByKeyTradeOrder));
			jsonAnswer = this.tradingOrder.postJson(bodyPostJson.toString());
			LOGGER.info("Real answer making limited buy order [{}]", jsonAnswer);
		}

		return jsonAnswer;
	}

	@Override
	public String makeLimitedSellOrder(final double price, final double amount)
																		throws ServiceException {

		final Map<String, Object> valueByKeyTradeOrder = new HashMap<>();
		valueByKeyTradeOrder.put(TradeOrder.TYPE, TypeOrder.LIMIT_ORDER.getName());
		valueByKeyTradeOrder.put(TradeOrder.CURRENCY, Currency.EUR.getName());
		valueByKeyTradeOrder.put(TradeOrder.DIRECTION, Direction.SELL.getName());
		valueByKeyTradeOrder.put(TradeOrder.PRICE, price);
		valueByKeyTradeOrder.put(TradeOrder.AMOUNT, amount);

		String jsonAnswer = null;
		if (ApplicationConf.getApplicationConf().isSimulate()) {
			LOGGER.info("Simulation of the sell order with price [{}] and amount [{}]", price, amount);
			jsonAnswer = SimulationUtil.setTradeFee(valueByKeyTradeOrder);
			LOGGER.trace("Json answer simulation making limited sell order [{}]", jsonAnswer);
		} else {
			LOGGER.info("Real execution of the sell order with price [{}] and amount [{}]", price, amount);
			final JSONObject bodyPostJson = new JSONObject(JSONObject.valueToString(valueByKeyTradeOrder));
			jsonAnswer = this.tradingOrder.postJson(bodyPostJson.toString());
			LOGGER.info("Json answer making limited sell order [{}]", jsonAnswer);
		}

		return jsonAnswer;
	}

	@Override
	public void cancelOrder(final Document docToCancel)
										throws ServiceException {
		if (docToCancel!=null){
			this.cancelOrder.delete(docToCancel.getString(Constants.STANDART_PK_MONGODB));
		}
	}

}
