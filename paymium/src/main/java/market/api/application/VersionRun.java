package market.api.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.common.Constants;
import market.api.exception.ServiceException;

public class VersionRun {

	private final static Logger LOGGER = LoggerFactory.getLogger(VersionRun.class.getName());

	static{
		try {
			ApplicationConf.setApplicationVersion(Constants.PATH_CONFIGURATION_VERSION_FILE);
		} catch (final ServiceException e) {
			LOGGER.error("Error jaxb version configuration file");
		}
	}

	public static final String whichVersion() {
		return ApplicationConf.getApplicationVersion().getVersion();
	}

	public final static String whichPlatform() {
		return ApplicationConf.getApplicationVersion().getPlatform();
	}

	public static void main(final String[] args) {
		LOGGER.info("Platform: {}, Version: {}",ApplicationConf.getApplicationVersion().getPlatform(), ApplicationConf.getApplicationVersion().getVersion());
	}

}
