package market.api.application;

import java.util.concurrent.atomic.AtomicInteger;

import market.api.common.Constants;
import market.api.common.Util;
import market.api.connection.db.ConnectionDao;
import market.api.connection.db.ConnectionDbApi;
import market.api.entity.Token;
import market.api.exception.ServiceException;
import market.api.scheduler.SchedulerJobApi;
import market.email.SenderEmail;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Rate-limiting
 * API calls are rate-limited by IP to 86400 calls per day (one per second on average). Information about the status of the limit can be found in the X-RateLimit-Limit and X-RateLimit-Remaining HTTP headers.
 *
 * Example response with rate-limit headers
 *
 * HTTP/1.1 200
 *
 * Content-Type: application/json; charset=utf-8
 *
 * X-Ratelimit-Limit: 86400
 *
 * X-Ratelimit-Remaining: 4982
 *
 * Date: Wed, 30 Jan 2013 12:08:58 GMT
 *
 * @author raul
 *
 */
public class Application {

	private final static Logger LOGGER = LoggerFactory.getLogger(Application.class.getName());

	private static final SchedulerJobApi schedulerJobApi = new SchedulerJobApi();

	private static final SenderEmail senderEmail = new SenderEmail();
	/**
	 * Max retry application
	 */
	private static AtomicInteger maxRetryApplication = new AtomicInteger();

	public static AtomicInteger getMaxRetryApplication() {
		return maxRetryApplication;
	}

	private static void setMaxRetryApplication(final int retryConnection) {
		maxRetryApplication.set(retryConnection);
	}

	private static int decreaseMaxRetryApplication() {
		return getMaxRetryApplication().decrementAndGet();
	}

	public static SchedulerJobApi getSchedulerJobApi() {
		return schedulerJobApi;
	}

	/**
	 * Reads configuration file and connect to DB
	 *
	 * @param fileNameDb
	 * @throws ServiceException
	 */
	public static void setApplicationDb(final String fileNameDb) throws ServiceException {
		if (ApplicationConf.getApplicationConf()==null) {
			// Read file
			ApplicationConf.setApplicationConf(fileNameDb);
			// Set db properties and connect to it
			ConnectionDbApi.setConnectionProp(ApplicationConf.getApplicationConf());
			ConnectionDbApi.setConnection(ConnectionDbApi.getConnectionMongoDb());
		}
	}

	public static void start(){
		LOGGER.info("The application is starting ... Version [{}]", VersionRun.whichVersion());

		try {
			// Configure MongoDB and start connection to the server
			LOGGER.info("Connecting to database ... ");
			Application.setApplicationDb(Constants.PATH_CONFIGURATION_FILE_DB);

			// Set max retry application
			setMaxRetryApplication(ApplicationConf.getApplicationConf().getMaxRetryConnection());

			// Start Quartz scheduler planner
			Application.getSchedulerJobApi().startSchedulerPlanner();

			// Init token simulation or not
			// if simulation replace the token for a read only one
			initTokenForSimulation(ApplicationConf.getApplicationConf().isSimulate());

			//	Start EOD jobs
			getSchedulerJobApi().configureAllJobsInSeconds(Util.conversor(ApplicationConf.getApplicationConf().getInitApplicationjobPlans().getJobPlan1()));

			//	Start Engine jobs
			getSchedulerJobApi().configureAllJobsInSeconds(ApplicationConf.getApplicationConf().getEngine().getJobPlan());

			LOGGER.info("The application started ...");

			// Add hooks and sleep forever
			sleepApplication(ApplicationConf.getApplicationConf().getMaxRunningTime());

		} catch (final ServiceException e) {
			LOGGER.error("Error starting the application. Starting is aborted");
			Application.stopAndSendEmail();
		} catch (final Exception exc) {
			LOGGER.error("Unexpected exception while starting application", exc);
			Application.stopAndSendEmail();
		}
	}

	/**
	 * Application error but can still have few retries more
	 *
	 * @param docString Document to delete in the sellBuy collection
	 */
	public static void applicationError(final String docString){

		LOGGER.error("Connection error. The remaining number or retry connections is [{}]", decreaseMaxRetryApplication());

		try {
			if (!Util.isEmpty(docString)){
				ConnectionDao.getNewBuySellDao().delete(Document.parse(docString));
			}
		} catch(final Exception exc){
			LOGGER.error("Error delete doc string [{}]", docString, exc);
		} finally {
			if (getMaxRetryApplication().get() == 0){
				LOGGER.error("Max number of retries [{}] has already exceeded. The application will stop.", ApplicationConf.getApplicationConf().getMaxRetryConnection());
				Application.stopAndSendEmail();
			}
		}
	}

	public static void applicationError(){
		Application.applicationError(null);
	}

	public static void resetConnectionRetry() {
		LOGGER.debug("Reset connection retry to original value [{}]", ApplicationConf.getApplicationConf().getMaxRetryConnection());
		setMaxRetryApplication(ApplicationConf.getApplicationConf().getMaxRetryConnection());
	}

	private static void initTokenForSimulation(final boolean isSimulation) {
		if (isSimulation){
			LOGGER.info("This is YES simulation only for testing.");
			//	db.token.insert( {"_id":"1", "Hello":"xxxx", "Api-Key":"yyyy"} )
			final Document doc = new Document(Token.ID,"1").
											 append(Token.SECRET_KEY, "60858c5cb9cfb85301f719c0b2326ece8b99bac6c84a117ce0fc07e62c7d22a0").
											 append(Token.API_KEY, "43427dcf2af41bdd9c4ec91ef3fae0a3c2f3a0e3e6fae379ad9bf61154cbff76");
			ConnectionDao.getNewTokenDao().saveToken(doc.toJson());
		} else {
			LOGGER.info("This is NOT a simulation. It makes real orders !!!!!!");
		}
	}

	/**
	 * Stops the application and terminates the current JVM and deletes the given document by parameter
	 *
	 * @param documentString Delete the document in the buySell collection, if null then only stop the application
	 */
	private static void stop(final String documentString) {
		LOGGER.info("The application stopped ... Version [{}]", VersionRun.whichVersion());

		// Stop quarts and its threads forever
		try {
			LOGGER.debug("Stop scheduler planner ");
			getSchedulerJobApi().stopSchedulerPlaner();
			if (getSchedulerJobApi().isAliveSchedulerPlanner()){
				LOGGER.error("The scheduler planner is still alive？");
			}
		} catch (final ServiceException e) {
			LOGGER.error("Error while stop scheduler planner");
		} finally {
			if (!Util.isEmpty(documentString)){
				ConnectionDao.getNewBuySellDao().delete(Document.parse(documentString));
			}
		}

		// Stop connection db
		LOGGER.debug("Stop connection DB ");
		ConnectionDbApi.close();

		// Stop main thread (Stop JVM)
		System.exit(0);
	}

	/**
	 * Stops the application, sends email and terminates the current JVM
	 *
	 * @throws ServiceException
	 */
	public static void stopAndSendEmail() {
		Application.stopAndSendEmail(null);
	}

	/**
	 * Stops the application, delete the document in the collection buySell, sends email and terminates the current JVM
	 *
	 * @throws ServiceException
	 */
	public static void stopAndSendEmail(final String documentString) {
		// Send email
		try {
			final String serverResponse = senderEmail.sendSimpleMessage("Error paymium Application", "The application stopped");
			LOGGER.info("Server response sending email [{}]", serverResponse);
		} catch (final ServiceException sexc) {
			LOGGER.error("Wrong status sending email");
		} catch (final Exception exc){
			LOGGER.error("Unexpected error", exc);
		}

		// Stop and delete document
		Application.stop(documentString);
	}

	public static void stopNonSendingEmail() {
		Application.stop(null);
	}

	/**
	 * Adds hook and sleeps forever
	 * The hook works for kill-15 or Control-C
	 * <p>
	 * Doesn't work for kill -9
	 * @param maxTimeInMin Max time in minutes to sleep application
	 */
	private static void sleepApplication(int maxTimeInMin) {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				LOGGER.info("Shutdown hook running!");
				Application.stopNonSendingEmail();
			}
		});

		if (maxTimeInMin == 0){ // Set up one year of minutes
			maxTimeInMin = Constants.NUMBER_MINUTES_IN_ONE_YEAR;
		}

		LOGGER.info("Sleeping application for [{}] minutes or [{}] hours", maxTimeInMin, (double)maxTimeInMin/Constants.SEC_IN_ONE_MINUTE);
		try {
			Thread.sleep(maxTimeInMin * Constants.ONE_MINUTE_IN_MILLIS);
		} catch (final InterruptedException e) {
			LOGGER.error("Error sleeping main thread", e);
		}

		LOGGER.info("Reached the maximum time to sleep the application. It'is going to stop immediately");

		// Stop application
		Application.stopAndSendEmail();
	}

	/**
	 * Allow start and stop
	 * <p>
	 * restart is made by script outside java
	 * @param args
	 */
	public static void main(final String[] args) {

		if (Util.isEmpty(args)){
			LOGGER.info("Args [{}] can not be empty");
			return;
		}

		try {
			if (args[0].equalsIgnoreCase("Start")) {
				start();
			} else if (args[0].equalsIgnoreCase("Stop")) {
				stopAndSendEmail();
			}
		} catch (final Exception exc) {
			LOGGER.error("Unexpected exception", exc);
		}
	}



}
