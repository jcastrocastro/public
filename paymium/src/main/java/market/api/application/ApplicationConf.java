package market.api.application;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import market.api.application.confVersion.VersionApplication;
import market.api.application.configuration.Configuration;
import market.api.common.Constants;
import market.api.common.Util;
import market.api.exception.ServiceException;

public class ApplicationConf {

	private final static Logger LOGGER = LoggerFactory.getLogger(ApplicationConf.class.getName());

	private static Configuration applicationConf;

	private static VersionApplication applicationVersion;

	// Fees
	private static final double PAYMIUM_FEE = 0.0059;

    // Private constructor. Prevents instantiation from other classes.
    private ApplicationConf() {}


    public static Configuration getApplicationConf() {
		return applicationConf;
	}

    static VersionApplication getApplicationVersion() {
		return applicationVersion;
	}

	public static void setApplicationVersion(final String fileName) throws ServiceException {
		if (applicationVersion==null){
			final InputStream inputStream = getInputStream(fileName);

			try {
				final Unmarshaller jaxbUnmarshaller = JAXBContext.newInstance(VersionApplication.class).createUnmarshaller();
				applicationVersion = (VersionApplication) jaxbUnmarshaller.unmarshal(inputStream);
			} catch (final JAXBException e) {
				LOGGER.error("Error getting info of the configuration file. Path [{}]. {}", fileName, e);
				throw new ServiceException(e);
			}
		}
    }

	public static void setApplicationConf(final String fileName) throws ServiceException {
		if (applicationConf==null){
			final InputStream inputStream = getInputStream(fileName);

			try {
				final Unmarshaller jaxbUnmarshaller = JAXBContext.newInstance(Configuration.class).createUnmarshaller();
				applicationConf = (Configuration) jaxbUnmarshaller.unmarshal(inputStream);
			} catch (final JAXBException e) {
				LOGGER.error("Error getting info of the configuration file. Path [{}]. {}", fileName, e);
				throw new ServiceException(e);
			}
		} else {
			LOGGER.debug("The application configuration was already set up. Don't need to do again");
		}
    }

	private static InputStream getInputStream(final String fileName) throws ServiceException {
		final InputStream inputStream = ApplicationConf.class.getResourceAsStream(fileName);

		if (inputStream == null) {
			LOGGER.error("Can not find the file [{}]", fileName);
			throw new ServiceException("Can not find the configuration file !!!");
		}

		return inputStream;
	}


	public static boolean isPaymiumMainPlatform() {
		final String mainPlatform = ApplicationConf.getApplicationConf().getMainPlatform();
		if (Util.isEmpty(mainPlatform)){
			LOGGER.error("In the configuration xml main application can not be empty");
			return false;
		}

		return mainPlatform.equalsIgnoreCase(Constants.PLATFORM_PAYMIUM);
	}

	/**
	 * Gets platform cost fee
	 *
	 * @return Platform cost fee
	 *
	 * @throws ServiceException The platform fee is undefined
	 */
	public static double getPlatformCostFee()
										throws ServiceException {
		double retCostFee = 0.0;
		if (ApplicationConf.isPaymiumMainPlatform()){
			retCostFee = ApplicationConf.PAYMIUM_FEE;
		} else {
			throw new ServiceException("NON USED");
		}

		return retCostFee;
	}
}
