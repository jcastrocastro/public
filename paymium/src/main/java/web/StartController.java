package web;

import market.api.application.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class StartController {

	private final static Logger LOGGER = LoggerFactory.getLogger(StartController.class.getName());
	
    @RequestMapping("/start")
    public String start(@RequestParam(value="key", required=true) String key, Model model) {
    	LOGGER.info("Starting the application");
    	
        model.addAttribute("key", key);

        // Try to start the application
        Application.start();

        return "start";
    }

}
