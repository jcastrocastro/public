package web;

import market.api.application.Application;
import market.api.exception.ServiceException;
import market.email.SenderEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import market.api.application.VersionRun;

@Controller
public class StopController {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(StopController.class.getName());

	private final SenderEmail senderEmail = new SenderEmail();

    @RequestMapping("/stop")
    public String stop(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
//        model.addAttribute("name", name);
    	
    	LOGGER.info("Stopping the application. Version v{} and Platform [{}]", VersionRun.whichVersion(), VersionRun.whichPlatform());

        // Stop application
        Application.stopNonSendingEmail();

        return "stop";
    }

}
