# README #

### What is this repository for? ###

This repository is for showing how anyone can build a multi-thread trading client and implement any trading strategy to integrate easily inside of the platform.

I wrote this code around September 2015. I only implemented the back-end and I left the front-end for later.

Since I am not a big trader I haven't had direct access to the market to make direct transactions. I had to pay fees to be able to access to the market through external platforms. I chose two external platforms to make REST API transactions [paymium](www.paymium.com) and CexIO(www.cex.io). These two platforms are specialized in 24/7 BTC/EUR trading. It made the market very attractive to me since I can check or buy/sell trades once I finished work or during weekends. More traditional markets I can only operate with them during working hours.

It was hard to test the code correctly, however I did my best using unit/integration tests and cURL tool to check code correctness. The external trading platforms only provided me with an unique environment (production environment) which I could access through their APIs. I found several bugs in their platforms and they have awful client support teams, made me wait days for an email answer. It really drove me crazy wasting a lot of valuable time. :( So it means that I tested the platform with real trades and using my own money. Any mistake in my own code would have cost me money.

My platform acts like an automatic trader, so it doesn't need a human interaction once it starts to run. I run the platform during two months 24/7 and I made around 100 euros. I implemented a simple strategy to look for "free meals" checking for price differences between two external platforms. I was constantly searching for these differences to buy/sell BTC/EUR. Obviously after few months they tracked me down and realized about my strategy. One day one platform was down and came back alive with a new algorithm to set the price BTC/EUR making my simple strategy ineffective. I was very busy at that time and no more free time to keep up with this project and look for a new one strategy. Maybe I will try it in the near future, who knows :)

![Scheme](images/platform.jpg)

**Please, be aware** that I hardcoded a lot of constants in code. For example, email accounts, keys and so on. I consider it a bad practise since these kind of data will change from one environment to another. It creates a code dependency. It should have been saved inside of the database. Hopefully I will fix it in next release :)

**bitcoin3App** is just a simple console application to calculate fees when you make manual trading. We will focus on **\paymium** directory since it is more interesting from a software point of view.

### Package structure \paymium directory ###

I will briefly describe the structure of \paymium directory.

![Scheme](images/treeStructure.jpg)

* `\java\market\api\application` -

  The start point of the application. It makes DB connections, starts Quartz scheduler planner, initializes end of days jobs and engine jobs (in the engine jobs are implemented the strategies), and set up java hooks and number of retry connections.
  It also contains the java beans to access the xml configuration using java xml bind annotation.

* `\java\market\api\common` -

  Constants and common static methods.

* `\java\market\api\connection` -

  Http helper to reuse code when I access to external platforms API.
  MongoDb DAO implementation to read/write in it.

* `\java\market\api\entity` -

  Java entities or beans

* `\java\market\api\enumtype` -

  Enums

* `\java\market\api\exception` -

  Custom exceptions.

* `\java\market\api\platformCexIo` -

  My own API to access to CexIO platform thought REST over http calls. I wrote it an API fashion to allow to reduce coupling between packages which make it more reusable.

* `\java\market\api\platformPaymium` -

  My own API to access to Paymium platform thought REST over http calls.  I wrote it an API fashion to allow to reduce coupling between packages which make it more reusable.

* `\java\market\api\scheduler` -

  Quartz scheduler planner is the one in charge of orchestrating engines and jobs.

* `\java\market\api\scheduler\engine` -

  The business logic to make profits are coded by trading is here.

* `\java\market\api\scheduler\job` -

  Besides engines there are another tasks to prepare the system to be able to analize correctly the prices. I divided them in EOD (End of Day) jobs and intraday jobs. EOD are usualy executed after I sleep and intraday any time during the day.

* `\java\market\email` -

  Send emails to my email box if the system is down.

* `\java\market\graphics` -

  Here should be some kind of graphics to show information of active trades. However I didn't build it. No free time for it.

* `\java\web` -

  A simple way to launch the application using Spring boot.

* `\resources\configuration` -

  Here I set up the configuration for the engines. I made two different configurations 'configurationPaimiumToCexEngines.xml' and 'configurationSimpleEngines.xml'. Whatever you want to use, you need to copy it at \resouces level and rename it as configuration.xml to be used for the platform.

  In this directory you can see the trading client configurationfor engines and jobs. It has a maximum of threads at one point of time. The Quartz Scheduler Planner executes the engines periodically at an X interval of time (measured in seconds) adding some specific parameters needed for the engines.

* `\resources\schema` -

  Schemas used to generate the Java beans using xml bind annotation (JAXB).

* `\resources\scripts` -

  Useful DB javascript scripts

* `\resources\static` -

  Basic html index.

* `\resources\templates` -

  Basic html templates.


### How do I get set up? ###

* `Dependencies` -

  For a list of dependencies please check the pom.xml

* `Database configuration` -

  Need to set up MongoDB in the computer. [MongoDB](https://www.mongodb.com/)

* `Deployment and run instructions` -

  Can generate the war using maven. It will also try to pass tests and load any required library:

  ```
  mvn clean package
  ```

  Without tests:

  ```
  mvn clean package -Dmaven.test.skip=true
  ```

  Execute the platform

  ```
  java -jar paymium-1.5-exec.war
  ```

  Of course before run the platform you need to sign up an user in [paymium platform](www.paymium.com)
